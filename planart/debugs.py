# -*- coding: utf-8 -*-
"""
Created on Thu May 18 08:19:18 2023

@author: belie
"""

import pickle
import time


def get_time():
    return time.time()

def save_current_state(vocal_tract, prms, x, file="debug.pickle"):
    with open(file, "wb") as f:
        pickle.dump([vocal_tract, prms, x], f)