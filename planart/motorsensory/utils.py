# -*- coding: utf-8 -*-
"""
Created on Thu Feb 20 13:08:06 2025

@author: belie
"""

import h5py
from maedeep import parametric
import numpy as np
from os import path
import time


def check_config(config, key, default):
    if key in config.keys():
        return config[key]
    else:
        return default
    
def convert_f(x, scaler_ac, scaler_p):
    x_real = scaler_ac.inverse_transform(x)
    return scaler_p.transform(x_real)

def denan(x, fill_value=0):
    if np.isnan(x).any():
        x[np.isnan(x)] = fill_value
    return x

def extract_geometry(data_file):
    data = read_h5py_file(data_file)
    p = data['articulatory vectors'].T
    lc, uc = [data[key + ' contour'] for key in ['lower', 'upper']]
    lcx, lcy = [lc[:, n, :].squeeze().T for n in range(2)]
    ucx, ucy = [uc[:, n, :].squeeze().T for n in range(2)]
    return p, np.concatenate([lcx, lcy, ucx, ucy], axis=1)

def get_c(model=None, size_correction=None):
    data = parametric.check_model(model)    
    if size_correction is None:
        size_correction = data["semi-polar coordinates"]["size correction"]
    return size_correction * data["semi-polar coordinates"]["map coeff"]

def inverse_transform_scl(scl, x):
    if hasattr(scl, 'mean_'):
        m, s = scl.mean_, np.sqrt(scl.var_)
    elif hasattr(scl, 'data_range_'):
        m, s = scl.data_min_, scl.data_range_
    return x*s + m

def print_first_line(optimizer):
    print('\n')
    
    first_line = ('rep. || att. || local min cost ||' +
          ' new cost || ')
    if optimizer['effort']:
        first_line += 'effort weight || '
    if optimizer['somatosensory learning rate']:
        first_line += 'learning rate (somatosensory) || '
    if optimizer['auditory learning rate']:
        first_line += 'learning rate (auditory) || '
    if optimizer['auditory weight']:
        first_line += 'auditory weight || '

    first_line += 'nb. iter. (eval) || Total time (s) || Attempt time (s)'
    print('-'*len(first_line), flush=True)
    print(first_line, flush=True)
    print('-'*len(first_line), flush=True)
    return [len(c) for c in first_line.split('||')]

def print_temp_solution(x, k, config, new_cost, stub_count,
                        local_min_cost, col_len, nb_iter, start_time,
                        previous_time=0):
    """ Display the current state of the optimization process """

    if start_time is not None:
        new_time = time.time() - start_time
        delta_time = new_time - previous_time
    else:
        new_time = 0
        delta_time = 0
    lines = [str(k+1) + "/" + str(config['adaptation optimization']['repetitions']),
             str(stub_count),
             '%3.5f' %local_min_cost, '%3.5f' %new_cost]
    optimizer = config['adaptation optimization']['optimizer']
    idx = 0
    if optimizer['effort']:
        lines += ['%.5f' %(x[idx])]
        idx += 1
    if optimizer['somatosensory learning rate']:
        lines += ['%.5g' %(10**x[idx])]
        idx += 1
    if optimizer['auditory learning rate']:
        lines += ['%.5g' %(10**x[idx])]
        idx += 1
    if optimizer['auditory weight']:
        lines += ['%.5f' %(x[idx])]
        idx += 1
    lines += [str(nb_iter[0]) + ' (' + str(nb_iter[1]) + ')',
    '%2.f' %new_time, '%2.f' %delta_time]

    second_columns = [write_column(line, c) for line, c in zip(lines, col_len)]
    second_line = '||'.join(second_columns)
    print(second_line, flush=True)
    return new_time

def read_data_file(data_file, keys):
    return [h5py.File(data_file, 'r')[key][()].T for key in keys]

def read_formant_corpus(file):
    with h5py.File(file, "r") as hf:
        X = hf['X'][()]
        y = hf['y'][()]
        vowel_labels = [v.decode('ascii') for v in hf['labels'][()].tolist()]
    return X, y, vowel_labels

def read_h5py_file(file):
    return {key: h5py.File(file, "r")[key][()] for 
            key in h5py.File(file, "r").keys()}

def replace_extension(x, new_extension=''):
    return x.replace(path.splitext(x)[-1], new_extension)

def sensory_prediction(model_dict, x):
    x_sc = transform_scl(model_dict['scalers'][0], np.array(x).reshape(1,-1))
    f = model_dict['model'](x_sc).numpy()
    return inverse_transform_scl(model_dict['scalers'][1], f)

def sol2config(x, config):        
    n = 0
    if config['adaptation optimization']['optimizer']['effort']:
        config['articulatory optimization']['weights']['effort'] = x[n]
        n += 1
    if config['adaptation optimization']['optimizer']['somatosensory learning rate']:
        config['somatosensory model']['learning rate'] = 10**x[n]
        n += 1
    if config['adaptation optimization']['optimizer']['auditory learning rate']:
        config['acoustic model']['learning rate'] = 10**x[n]
        n += 1
    if config['adaptation optimization']['optimizer']['auditory weight']:
        config['articulatory optimization']['weights']['auditory'] = x[n]
        config['articulatory optimization']['weights']['somatosensory'] = 1-x[n]
        n += 1

    return config

def transform_scl(scl, x):
    if hasattr(scl, 'mean_'):
        m, s = scl.mean_, np.sqrt(scl.var_)
    elif hasattr(scl, 'data_range_'):
        m, s = scl.data_min_, scl.data_range_
    return denan((x-m)/s, fill_value=0)

def write_column(string, col_size):
    nb_space = int((col_size - len(string))/2)
    string = " "*nb_space + string + " "*nb_space
    while len(string) < col_size:
        string += " "
    return string
