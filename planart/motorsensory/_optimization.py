# -*- coding: utf-8 -*-
"""
Created on Tue May 30 10:52:13 2023

@author: belie
"""

import copy
import numpy as np
import os
from scipy.optimize import minimize
from .utils import (
                                    check_config,
                                    transform_scl,
                                    inverse_transform_scl
                                    )
from maedeep import parametric
import tensorflow as tf

@tf.function
def prediction(model, x):
    try:
        x = tf.convert_to_tensor(x, dtype=tf.float64)
    except:
        x = tf.convert_to_tensor(x, dtype=tf.float32)
    return model(x)

class Optimizer:
    name = None
    internal_models = None
    weights = None
    threshold = 0
    nb_attempts = 3
    verbosity = True
    parallel = 1
    repetitions = 1
    maxiter = 200
    solution = None
    cognitive_noise = 0
    objective_model = 'dual'


    # Constructor method
    def __init__(self, *args):
        nargin = len(args)
        for k in range(0, nargin, 2):
            key, value = args[k:k+2]
            setattr(self, key, value)

    def copy(self):
        return copy.deepcopy(self)

    def global_optimization(self, vowel, x0=None):
        """ Performs global optimization """

        if x0 is not None:
            global_min_cost = objective(x0, self, vowel)
            global_min_x = [x for x in x0]
        else:
            global_min_cost = np.inf
            global_min_x = None

        if self.parallel <= 1 or self.repetitions <= 1:
            for k in range(self.repetitions):
                local_min_cost, local_min_x = self.repetition(x0, vowel,
                                                         global_min_cost, k)

                global_checks = check_global_optimization(local_min_cost,
                                                          local_min_x,
                                              global_min_cost, global_min_x)
                global_min_cost, global_min_x = global_checks
        else:
            from joblib import Parallel, delayed
            if os.name == "nt":
                backend = "threading"
            else:
                backend = "loky"
            solutions = Parallel(n_jobs=self.parallel, backend=backend,
                        prefer="processes")(delayed(self.repetition)(x0,
                                                                     vowel,
                                                                     None,
                                                                     k) for
                                                k in range(self.repetitions))
            for solution in solutions:
                local_min_cost, local_min_x = solution
                global_checks = check_global_optimization(local_min_cost,
                                                          local_min_x,
                                                          global_min_cost,
                                                          global_min_x)
                global_min_cost, global_min_x = global_checks

        if self.verbosity:
            print("Optimization completed", flush=True)
            print("Global minimum: ", global_min_cost, flush=True)

        self.solution = global_min_x
        return global_min_x

    def local_optimization(self, x0, vowel):
        options = {'maxiter': self.maxiter * len(x0)}
        bounds = ((-3, 3), ) * 7
        args = (self, vowel)
        result = minimize(objective, x0, args=args, method="nelder-mead",
                              options=options, bounds=bounds)
        return result["x"], result["fun"]

    def repetition(self, x0_start, vowel, global_min_cost, k):

        """ Perform one otpimization run """
        if x0_start is not None:
            min_x = [xx for xx in x0_start]
        else:
            min_x = (6*np.random.rand(7)-3).tolist()

        rem_attempts = self.nb_attempts
        local_min_cost = objective(min_x, self, vowel)
        isContinue = True

        while isContinue:
            new_x, new_cost = self.local_optimization(min_x, vowel)

            (min_x, rem_attempts,
             local_min_cost, isContinue) = check_local_optimization(new_x,
                                            new_cost, local_min_cost,
                                         self.nb_attempts, min_x,
                                         self.threshold, rem_attempts)
            if self.nb_attempts == 0:
                isContinue = False

            if self.verbosity >= 1 and global_min_cost is not None:
                print("Current repetition: ", k+1, " /", self.repetitions,
                      flush=True)
                print("Global cost function ", global_min_cost, flush=True)
                print("Local cost function: ", local_min_cost, flush=True)
                print("New cost :", new_cost, flush=True)
                print("Number of remaining attempts :", rem_attempts,
                      flush=True)

        return local_min_cost, min_x

def change_x(x, n, eps):
    xn = [y for y in x]
    xn[n] += eps
    return xn

def check_global_optimization(local_min_cost, local_min_x,
                              global_min_cost, global_min_x):
    """ Check whether the local minimum in the local loop
    is temporary a global one """
    if local_min_cost < global_min_cost:
        global_min_x = local_min_x
        global_min_cost = local_min_cost

    return global_min_cost, global_min_x

def check_local_optimization(new_x, new_cost, local_min_cost,
                             nb_attempts, min_x, threshold, rem_attempts):
    """ Check whether the local minimum is temporary a global one """

    isContinue = True
    gain = np.abs((new_cost - local_min_cost) / local_min_cost)
    if new_cost < local_min_cost:
        local_min_cost = new_cost
        min_x = new_x

        if gain > threshold:
            rem_attempts = nb_attempts
        else:
            rem_attempts -= 1
        if rem_attempts <= 0:
            isContinue = False
    else:
        rem_attempts -= 1
        if rem_attempts <= 0:
            isContinue = False
    return min_x, rem_attempts, local_min_cost, isContinue

def concat_features(formants, geometry):
    return np.hstack((formants, geometry))

def get_effort_cost(x, optim):
    xs = [optim.initial_position, optim.final_position]
    E = 0
    for n in range(2):
        movement = x - xs[n]   
        E += (np.sum((movement * optim.masses)**2, axis=-1)) 
    return E

def get_acoustic_features(x, optim):
    model_dict = optim.internal_models['acoustic predictor model']
    x_sc = transform_scl(model_dict['scalers'][0], np.array(x).reshape(1,-1))
    f = prediction(model_dict['model'], x_sc).numpy()
    return inverse_transform_scl(model_dict['scalers'][1], f)

def get_parsing_acoustic(x, optim, vowel):
    formants = get_acoustic_features(x, optim)
    if np.isnan(formants).any():
        return 1
    else:        
        model = optim.internal_models['acoustic perception model']
        p = get_probabilities(formants, model["model"], model["scalers"][0])
        return 1 - p[model["labels"].index(vowel)]

def get_parsing_somato(x, optim, vowel):
    model_key = 'somatosensory perception model'
    labels = optim.internal_models[model_key]["labels"]
    somatosensory_feats = get_somatosensory_features(x, optim)
    p_model, p_scaler = [optim.internal_models[model_key][key] for
                         key in ['model', 'scalers']]
    p = get_probabilities(somatosensory_feats, p_model, p_scaler[0])
    return 1 - p[labels.index(vowel)]
 
def get_probabilities(features, model, scaler):
    try:
        return model.predict_proba(transform_scl(scaler, features)).squeeze()
    except:
        return prediction(model, transform_scl(scaler, features)).numpy().squeeze()

def get_somatosensory_features(x, optim):
    scalers = optim.internal_models['somatosensory predictor model']['scalers']
    x_norm = transform_scl(scalers[0], np.array(x).reshape(1, -1))
    return inverse_transform_scl(
            scalers[1],
            prediction(
                optim.internal_models['somatosensory predictor model']['model'],
                x_norm
                ).numpy()
            )

def init_optimizer(config):
    art_config = config['articulatory optimization']
    art_config['repetitions'] = check_config(art_config, 'repetitions', 1)
    art_config['initial position'] = check_config(art_config, 
                                                   'initial position',
                                                   np.array([0]*7))
    art_config['final position'] = check_config(art_config, 
                                                   'final position',
                                                   np.array([0]*7))
    art_config['parallel'] = check_config(art_config, 'parallel', 1)
    art_config['masses'] = check_config(art_config, 'masses', 1)
    art_config['maxiter'] = check_config(art_config, 'maxiter', 100)
    art_config['threshold'] = check_config(art_config, 'threshold', 1e-4)
    art_config['patience'] = check_config(art_config, 'patience', 0)
    config['cognitive noise'] = check_config(config, 'cognitive noise', 0)
    config['verbose'] = check_config(config, 'verbose', False)
    art_config['weights']['somatosensory'] = check_config(
                                                    art_config['weights'],
                                                    'somatosensory',
                                                    0
                                                    )
    art_config['weights']['auditory'] = 1 - art_config['weights']['somatosensory']    
    
    internal_models = {key: config[key]['model'] for
                       key in [
                           'acoustic predictor model', 
                           'somatosensory predictor model', 
                           'acoustic perception model',
                           'somatosensory perception model'
                           ]
                       }

    return Optimizer(
                   "internal_models", internal_models,
                   "weights", art_config['weights'],
                   "repetitions", art_config['repetitions'],
                   "parallel", art_config['parallel'],
                   'verbosity', art_config['verbose'],
                   'maxiter', art_config['maxiter'],
                   'threshold', art_config['threshold'],
                   'nb_attempts', art_config['patience'],
                   'cognitive_noise', config['cognitive noise'],
                   'initial_position', np.array(art_config['initial position']),
                   'final_position', np.array(art_config['final position']),
                   'masses', np.array(art_config['masses'])
                   )

def objective(x, *args):
    optim, vowel = args
    effort_weight = optim.weights["effort"] * (1 + optim.cognitive_noise*np.random.randn() / 100)
    E, P_auditory, P_somato = 0, 0, 0
    if optim.weights["effort"] != 0:
        E = get_effort_cost(x, optim)
    if optim.weights['auditory'] != 0:
        P_auditory = get_parsing_acoustic(x, optim, vowel) 
    if optim.weights['somatosensory'] != 0 :
        P_somato = get_parsing_somato(x, optim, vowel)
    return (
            effort_weight * E +
            optim.weights['auditory'] * P_auditory +
            optim.weights['somatosensory'] * P_somato
            )

def optimization(config, vowel, x0=None):
    optim = init_optimizer(config)
    return optim.global_optimization(vowel, x0=x0)