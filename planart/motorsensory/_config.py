# -*- coding: utf-8 -*-
"""
Created on Fri Jul 19 16:04:14 2024

@author: belie
"""

from os import path
from planart import ( 
    get_planart_path,
    import_model
    )

def config_optimization(config):
    config_start = dict(config)
    config_start['articulatory optimization'] = dict(config['articulatory optimization'])
    config_start['articulatory optimization']['maxiter'] = 200
    config_start['articulatory optimization']['patience'] = 3
    
    # config_start['articulatory optimization']['repetitions'] = 10
    # config_start['articulatory optimization']['verbose'] = True
    return config_start    

def create_default_config(method="xt3c", language="american-english", files=None):
    if files is None:
        files = get_default_files(method=method, language=language)
    if method == "xt3c":
        articulatory_optimization = {
                                    'maxiter': 1,
                                    'convergence threshold': 0,
                                    'patience': 0,
                                    'repetitions': 1,
                                    'weights': {
                                        "effort": 1,
                                        "somatosensory": 0.25,
                                        "auditory": 0.75
                                        },
                                    'parallel': 1,
                                    'verbose': False,
                                    'masses': [
                                        0.550, # JAW mass
                                        0.25, # TD mass
                                        0.25, # TD mass
                                        0.05, # TT mass
                                        0.03, # Lip mass
                                        0.03, # Lip mass
                                        0.05  # Larynx mass
                                        ]
                                     }
        adaptation_optimization = {
                                'maxiter': 10,
                                'convergence threshold': 1e-5,
                                'patience': 1,
                                'repetitions': 1,
                                'verbosity': False,
                                'eval': 0,
                                'bounds': {
                                    'learning rate': {
                                        'lower': -7,
                                        'upper': -3
                                        },
                                    'ratio': {
                                        'lower': 0,
                                        'upper': 5
                                        },
                                    'auditory': {
                                        'lower': 0,
                                        'upper': 1
                                        }
                                    },
                                'optimizer': {
                                    'effort': True,
                                    'somatosensory learning rate': False,
                                    'auditory learning rate': True,
                                    'auditory weight': True
                                    }
                                }

        return {
                'acoustic perception model': {
                    'file': files['ftp']
                    },
                'somatosensory perception model': {
                    'file': files['stp']
                    },    
                'acoustic predictor model': {
                    'file': files['acoustic predictor model'],
                    'learning rate': 1e-3,
                    'loss': 'mean_squared_error',
                    'optimizer': 'adagrad',
                    'epochs': 1
                    },
                'somatosensory predictor model': {
                    'file': files['somatosensory predictor model'],
                    "learning rate": 1e-3,
                    'loss': 'mean_squared_error',
                    'optimizer': 'adagrad',
                    "epochs": 1
                    },
                'verbose': False,
                'articulatory optimization': articulatory_optimization,
                'adaptation optimization': adaptation_optimization,
                'cognitive noise': 0,
                'motor noise': 0,
                'somatosensory representation': 'tract variable',
                'acoustic representation': 'formant',
                'full start': False,
                'verbose trials': False,
                'all update': True                
                }

    elif method == "simplediva":
        return {
                  'verbose': False,
                  "alteration": 1,
                  'parallel': 1,
                  'maxiter': 200,
                  'patience': 3,
                  'repetitions': 1,
                  'convergence threshold': 1e-4,
                  'bounds': {
                              'auditory': {
                                          'lower': 0,
                                          'upper': 1
                                          },
                              'somatosensory': {
                                          'lower': 0,
                                          'upper': 1
                                  }
                              }
              }
    elif method == "geppeto":
        return {
                  'verbose': False,
                  "step size": 0.1,
                  'burn in': 1,
                  'model files': files,
                  'acoustic model': {
                      'file': files['acoustic predictor model'],
                      'learning rate': 1e-4,
                      'epochs': 1
                      },
                  'target parameters': {
                      'auditory': 1,
                      'somatosensory': 1
                      },
                  'model type': "fusion",
                  'maxiter': 200
                  }

def get_default_files(method="xt3c", language='american-english'):
    if method.lower() == "xt3c":
        model_path = path.join(get_planart_path(), 'models')
        files = {
            'ftp': path.join(
                model_path, 
                'perception_models', 
                'acoustic_recognition',
                'acoustic_perception_model_' + language,
                'model.keras'
                ),
            'stp': path.join(
                model_path, 
                'perception_models', 
                'somatosensory_recognition',
                'somatosensory_perception_model_' + language,
                'model.keras'
                ),
            'acoustic predictor model': path.join(
                model_path, 
                'sensory_predictors',
                'acoustic_predictors',
                '5_layers_250_140_20_40_10',
                'model.keras'
                ),
            'somatosensory predictor model': path.join(
                model_path, 
                'sensory_predictors',
                'somatosensory_predictors',
                'articulatory_to_tract',
                'model.keras'
                ),
            'dictionary': path.join(
                model_path, 
                'dictionaries',
                language + '_dictionary.json'
                ),
            }

    elif method.lower() == "geppeto":
        model_path = path.join(get_planart_path(), 'planart_adaptation',
                               'GEPPETO', 'models')
        files = {
            'gmm acoustic': path.join(model_path,
                                      language + '_GMM_acoustics.h5'),
            'gmm articulatory': path.join(model_path,
                                      language + '_GMM_articulatory.h5'),
            'art2ac': path.join(model_path, 'initial_model.h5')
            }
    elif method.lower() == "simplediva":
        files = {}
    elif method.lower() == "training":
        model_path = path.join(get_planart_path(), 'data', 'simulations')
        if language == "english":
            formant_file = "vtr_formants_english.h5"
        elif language == "french":
            formant_file = "ifcasl_formants_french.h5"
        files = {
            'maeda data': path.join(model_path,
                                    'articulatory_acoustics_data.h5'), 
            'formants': path.join(model_path, formant_file) 
            }
    elif method.lower() == "optimality":
        model_path = path.join(get_planart_path(), 'models')
        files = {
            'ftp': path.join(
                model_path, 
                'perception_models', 
                'acoustic_recognition',
                'acoustic_perception_model_' + language,
                'model.keras'
                ),
            'stp': path.join(
                model_path, 
                'perception_models', 
                'somatosensory_recognition',
                'somatosensory_perception_model_' + language,
                'model.keras'
                ),
            'acoustic predictor model': path.join(
                model_path, 
                'sensory_predictors',
                'acoustic_predictors',
                '5_layers_250_140_20_40_10',
                'model.keras'
                ),
            'somatosensory predictor model': path.join(
                model_path, 
                'sensory_predictors',
                'somatosensory_predictors',
                'articulatory_to_tract',
                'model.keras'
                ),
            'optimality predictor': path.join(
                model_path, 
                'optimality_predictors', 
                '3_layers_40_80_40_' + language,
                'model.keras')            
            }
    for key in files.keys():
        assert path.isfile(files[key])
    return files

def update_config(config, key):
    model, scalers = import_model(config[key]['file'])
    config[key].update(
        {
         "model":
             {
             'model': model,
             'scalers': scalers,
             }
         }
        )

def upload_internal_models(config):
    
    [update_config(config, key) for 
     key in [
         'acoustic predictor model', 
         'somatosensory predictor model', 
         'acoustic perception model',
         'somatosensory perception model'
         ]
     ]