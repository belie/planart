# -*- coding: utf-8 -*-
"""
Created on Thu Feb 20 13:08:06 2025

@author: belie
"""

import numpy as np


def auditory_feedback_alteration(config, produced):

    if config['alteration type'] == 'shift':
        return formant_shift(produced, config['alteration'])
    elif config['alteration type'] == 'centralization':
        return centralization(produced, config['central'], config['alteration'])
    elif config['alteration type'] == 'none':
        return produced
    elif config['alteration type'] == 'scaling':
        return formant_scale(produced, config['alteration'])

def assign_alteration(config, df):
    if config['alteration type'] == 'scaling':
        config['alteration'] = np.ones((4, 1)) + np.array([df, 0, 0, 0]).reshape(-1, 1)
    elif config['alteration type'] == 'centralization':
        config['alteration'] = df * 100
    elif config['alteration type'] == 'shift':
        config['alteration'] = np.zeros((4, 1)) + np.array([df, 0, 0, 0]).reshape(-1, 1)
    return config

def centralization(produced, central, rate=20):
    df = central - produced
    new_f1 = produced[0] + rate / 100 * df[0]
    new_f2 = produced[1] + rate / 100 * df[1]
    return np.array([new_f1, new_f2, produced[2], produced[3]]).reshape(-1, 1)
   
def edit_perturbation(perturbations):
    if type(perturbations) == dict:    
        if 'auditory' in perturbations.keys():
            df_vec = perturbations['auditory']
        else:
            df_vec = [0]
        if 'somatosensory' in perturbations.keys():
            dt_vec = perturbations['somatosensory']
        else:
            dt_vec = [0]
    else:
        df_vec = perturbations
        dt_vec = [0]
        
    N = np.max((len(df_vec), len(dt_vec)))
    if np.sum(np.abs(df_vec)) == 0:
        df_vec = [0] * N
    if np.sum(np.abs(dt_vec)) == 0:
        dt_vec = [0] * N
    return df_vec, dt_vec, N

def formant_scale(produced, perturbation=np.ones((4, 1))):
    return produced.reshape(-1, 1) * perturbation

def formant_shift(produced, shift=np.zeros((4, 1))):
    return produced.reshape(-1, 1) + shift

def sensory_noise_perturbation(X, sensory_noise=0):
    if sensory_noise > 0:
        if len(X.shape) == 1:
            X = X.reshape(-1, 1)
        X = X * (1 + sensory_noise/100*np.random.randn(X.shape[0], X.shape[1])) * (X > 0)
    return X.squeeze()

def somatosensory_feedback_alteration(config, contours):

    contours_perturbed = contours.copy()
    lx, ly, ux, uy = contours[0].contours
    if config['jaw']:
        distance = (lx[-3] - 10)
        lx[-3:] += distance * (config['alteration'][0].squeeze()-1)
        ux[-3:] += distance * (config['alteration'][0].squeeze()-1)
    if config['protrusion']:        
        distance = (lx[-1] - lx[-2])
        lx[-1:] += distance * (config['alteration'][0].squeeze()-1)
        ux[-1:] += distance * (config['alteration'][0].squeeze()-1)
    
    contours_perturbed[0].contours = np.array([lx, ly, ux, uy])   
    return contours_perturbed