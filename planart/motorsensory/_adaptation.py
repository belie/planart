# -*- coding: utf-8 -*-
"""
Created on Fri Feb 21 11:47:14 2025

@author: belie
"""

from maedeep import parametric
import numpy as np
from planart import ( 
    compile_model,
    sensory_model_update
    )
from tqdm import tqdm
from ._perturbations import sensory_noise_perturbation
from ._config import config_optimization
from ._optimization import optimization
from .utils import sensory_prediction

def adaptation_auditory_perturbation_experiment(config, perturbations, 
                                                vowel, min_x=None):
    
    config_start = config_optimization(config)
    x_init = optimization(config_start, vowel, min_x)
    x0_sol = [x for x in x_init]
    x0 = sensory_noise_perturbation(
        np.array(x0_sol), sensory_noise=config['motor noise']
        )
    formants = parametric.articulatory_to_formant(x0).squeeze()
    while np.any(formants==0) or formants[0] > 1000:
        x_init = optimization(config_start, vowel, min_x)
        x0_sol = [x for x in x_init]
        x0 = sensory_noise_perturbation(
            np.array(x0_sol), sensory_noise=config['motor noise']
            )
        formants = parametric.articulatory_to_formant(x0).squeeze()
    produced_acoustics_matrix = np.zeros((len(perturbations), 4))
    predicted_acoustics_matrix = np.zeros((len(perturbations), 4))
    auditory_feedback_matrix = np.zeros((len(perturbations), 4))
    articulatory_matrix = np.zeros((len(perturbations), 7))
    
    [compile_model(config[key + ' predictor model']) for 
     key in ('acoustic', 'somatosensory')]
    
    for n in tqdm(range(len(perturbations)), disable=not config['verbose']):
        x0_sol = optimization(config, vowel, x0_sol)        
        x0 = sensory_noise_perturbation(
            np.array(x0_sol), sensory_noise=config['motor noise']
            )
        formants = parametric.articulatory_to_formant(x0).squeeze()
        auditory_feedback = formants * 1
        auditory_feedback[0] = formants[0] * (1 + perturbations[n])
        
        auditory_feedback = sensory_noise_perturbation(
            auditory_feedback, sensory_noise=config['auditory noise']
            )
        
        predicted_acoustics_matrix[n, :] = sensory_prediction(
            config['acoustic predictor model']['model'], 
            np.array(x0_sol).reshape(1, -1)
            )
        produced_acoustics_matrix[n, :] = formants
        auditory_feedback_matrix[n, :] = auditory_feedback
        articulatory_matrix[n, :] = x0
        
        sensory_model_update(
            config['acoustic predictor model']['model'], 
            np.array(x0_sol).reshape(1, -1), 
            auditory_feedback.reshape(1, -1)
            )
        
    return ( 
        produced_acoustics_matrix, 
        auditory_feedback_matrix, 
        articulatory_matrix,
        predicted_acoustics_matrix
        )
    
    

    
    

