from typing import Dict, Tuple  # noqa: F401
from .utils import gesture_tier, write_json

OUTPUTS = {
    'symbolic': (
        'symbolic_cues.json',
        'symbolic_weights.json'
    ),
    'optimization': (
        'solution.json',
        'final_score.json'
    ),
    'acoustics': (
        'speech_audio.wav',
    ),
} 

def export_solution(vocal_tract, output_name, stub_count=0):
    """ Export solution in JSON format """
    
    arts = vocal_tract.articulators
    nb_gests = len(arts[0].movements)
    json_output = {"static model": "maeda", "dynamic model": "tau", 
                   "reference phonemes": " ".join(vocal_tract.reference_phonemes),
                   "predicted phonemes": " ".join(vocal_tract.predicted_phonemes),
                   "sampling rate": arts[0].movements[0].sampling_frequency,
                   "number of attempts remaining": stub_count,
                   "cost_function": {"global": vocal_tract.cost_function,
                                     "effort": vocal_tract.effort_cost,
                                     "parsing": vocal_tract.parsing_cost,
                                     "PER": vocal_tract.per_cost,
                                     "prosody": vocal_tract.prosodic_cost,
                                     "speech rate": vocal_tract.speech_rate_cost,
                                     "duration": vocal_tract.duration_cost},
                   "duration": vocal_tract.duration,
                   "solution": [gesture_tier(arts, n) for n in range(nb_gests)]}   
            
    write_json(json_output, output_name)