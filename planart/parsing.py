#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug 25 15:19:44 2022

@author: benjamin
"""

import numpy as np

def closure_masking(vocal_tract, prms):
    """ Returns closure function """

    z = vocal_tract.tract_space.dynamics
    nb_frame = z.shape[1]
    ones =  np.ones((1, nb_frame))
    coll = z[:2,:] - vocal_tract.anatomy.boundaries[:2].reshape(-1,1) @ ones

    bound_force = np.abs(1/(1e9*(coll**3)))

    closure_mask = (bound_force > prms.closure_threshold).astype(int)
    dc = np.diff(closure_mask, axis=0).squeeze()
    closure_mask[1,:] = np.max((np.zeros_like(dc), dc), axis=0)
    pure_closure_mask = np.sign(np.sum(closure_mask, axis=0))

    return closure_mask, pure_closure_mask

def consonant_onsets(vocal_tract, closure_mask, prms):
    """ Consonant closure onsets and offsets """
    closure_points = np.diff(closure_mask, 1, axis=1)
    cons_IDs, cons_onsets_points = np.where(closure_points == 1)
    cons_IDs, cons_offsets_points = np.where(closure_points == -1)
    cons_onsets_times = np.array([vocal_tract.time_vector[i] for i in cons_onsets_points.astype(int)])
    cons_offsets_times = np.array([vocal_tract.time_vector[i] for i in cons_offsets_points.astype(int)])
    max_prom_value = 1 - np.array(prms.consonant_timing_factor)/(1000*(cons_offsets_times-cons_onsets_times))
    return [[cons_IDs[n], max_prom_value[n], cons_offsets_times[n],
             cons_onsets_times[n], cons_offsets_times[n]]
            for n in range(len(cons_IDs))]

def vowel_distance(vocal_tract, prms):
    """ Returns parsing distance for vowels """

    vocal_tract.tract_space.get_target(False, speech_ready=False)
    vocal_tract.speech_ready_space.get_target(False)
    z = vocal_tract.tract_space.dynamics
    z_0 = vocal_tract.tract_space.target
    z_00 = vocal_tract.speech_ready_space.target
    nb_frame = z.shape[1]
    ones =  np.ones((1, nb_frame))

    return 1 - np.abs((z[2:,:] - (z_0[-1:].reshape(-1, 1) @ ones ))/
                        ((z_0[-1:]-z_00[-1:]).reshape(-1, 1) @ ones))
