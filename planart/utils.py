#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May 25 15:48:04 2019

@author: benjamin
"""

import json
from pathlib import Path
import numpy as np
import h5py
from ._inputs import (sort_all_constituents)
import scipy.interpolate as scintp
import os
from ._proba import phoneme_probability


def art_to_tract_mapping(art_space, tract_space, attr, order=0):
    """ Articulatory to tract mapping """

    vocal_tract = art_space.parent
    value = getattr(art_space, attr)
    if order == 0:
        setattr(tract_space, attr, vocal_tract.anatomy.general_matrix @ value)
    else:
        value2 = (vocal_tract.anatomy.general_matrix @
                                     value @
                                     vocal_tract.anatomy.pseudo_inverse_matrix)
        setattr(tract_space, attr, value2)

def articulator_tier(articulator, n):
    return {"name": articulator.name,
            "onset": {"time": articulator.movements[n].time_points[0],
                      "position": articulator.movements[n].position[0]},
            "offset": {"time": articulator.movements[n].time_points[1],
                      "position": articulator.movements[n].position[1]},
            "kappa": articulator.movements[n].kappa,
            "weights": articulator.movements[n].weights,
            "index": articulator.movements[n].index}

def boundaries(bnds, a_m, zm2):
    """ Returns the boundary damping term """
    return np.diag(1/(1e9*((bnds - a_m @ zm2)**3)))

def change_parameters(file, keys, values, level):
    """ change parameters in the globalparameter file """
    with open(file, 'r') as f:
        prms = json.load(f)
    for key, value in zip(keys, values):
        if type(prms[level][key] == float) and type(value != float):
            value = float(value)
        if type(prms[level][key] == int) and type(value != int):
            value = int(value)
        prms[level][key] = value
    with open(file, 'w') as f:
        json.dump(prms, f, indent=4)

def check_config(config, key, default):
    if key in config.keys():
        return config[key]
    else:
        return default

def check_file_exist(file):
    if os.path.isfile(file):
        return 0
    else:
        print(file + " does not exist", flush=True)
        return 1
    
def compute_acceleration(x, sr):
    return np.gradient(np.gradient(x, 1/sr), 1/sr)

def crit_damp(space, general_stiffness=1, diagonal=False):
    """ Returns the critical damping matrix """
    if diagonal:
        return diag(np.abs(2 * np.sqrt(space.mass * space.stiffness * general_stiffness)))
    else:
        return np.abs(2 * np.sqrt(space.mass @ space.stiffness * general_stiffness))

def diag(matrix):
    """ Foced diagonalisation of matrixx """
    return np.diag(np.diag(matrix))

def empty_list(nb_list):
    return [[] for _ in range(nb_list)]

def find_model_labels(proba_file, label_file):
    """ returns the labels for the sleected probabilistic model """
    dicts = read_json(label_file)["models"]
    root_name = os.path.splitext(os.path.split(proba_file)[1])[0]
    model_dict = [d for d in dicts if d["name"] == root_name]
    if len(model_dict) > 0:
        return [c["label"] for c in model_dict[0]["classes"]]
    else:
        raise ValueError("No labels for this model")

def gesture_effective_timing(vocal_tract):
    """ returns the name and effective timings of gestures in sorted order"""
    name_vec = []
    duration_vec = []
    start_vec = []
    for gest in vocal_tract.gestures:
        name = gest.name
        for start, duration in zip(gest.effective_start,
                                   gest.effective_duration):
            if duration >= 0:
                name_vec.append(name)
                duration_vec.append(duration)
                start_vec.append(start)

    idx_sort = np.argsort(start_vec)
    name_sort = [name_vec[i] for i in idx_sort]

    start_sort = np.sort(start_vec)
    duration_sort = [duration_vec[i] for i in idx_sort]

    return update_effective_timings(name_sort, start_sort, duration_sort)

def gesture_tier(arts, n):
    return {"name": arts[0].movements[n].name,
                "articulators": [articulator_tier(art, n) for art in arts]}

def gesture_timing(vocal_tract):
    """ returns the name and timings of gestures in sorted order"""
    name_vec = []
    start_vec = []
    duration_vec = []
    for gest in vocal_tract.gestures:
        name = gest.name
        (starts, ends) = (gest.start, gest.end)
        for start, end in zip(starts, ends):
            duration = end - start
            if duration > 0:
                name_vec.append(name)
                start_vec.append(start)
                duration_vec.append(duration)

    idx_sort = np.argsort(start_vec)
    name_sort = [name_vec[i] for i in idx_sort]
    start_sort = np.sort(start_vec)
    duration_sort = [duration_vec[i] for i in idx_sort]

    return name_sort, start_sort, duration_sort

def get_solution(vocal_tract):
    """ returns the stiffness and score solutions """
    return vocal_tract.general_stiffness, vocal_tract.get_time_points()

def get_surface_duration(vocal_tract, prms, th=-1):

    tvec = vocal_tract.time_vector
    idx = [g.index for g in vocal_tract.articulators[0].movements[:-1]]
    idx_neutral = prms.probabilistic_model.labels.index(prms.probabilistic_model.neutral_phoneme)
    inst = np.zeros((len(idx),2))

    if vocal_tract.probability is None:
        proba = phoneme_probability(vocal_tract.articulatory_space.dynamics,
                                prms.probabilistic_model)
    else:
        proba = vocal_tract.probability
    proba_neutral = proba[:, idx_neutral]
    
    for n, idx_c in enumerate(idx):
        start, end = get_time_points(vocal_tract, n)
        
        inst[n, :] = start, end
        if n == len(idx) - 1:
            end = vocal_tract.time_vector[-1]
        idx_s, idx_e = [get_time_index(t, vocal_tract.time_vector) for
                        t in [start, end]]
        pp = proba[idx_s:idx_e+1, :]
        win = np.hanning(pp.shape[0]).reshape(-1, 1)**2
        ppw = pp*win
        all_curr_p = np.sum(ppw, axis=0)        
        if np.argmax(all_curr_p) != idx_c:            
            idx[n] = np.argmax(all_curr_p)
            
    for n, idx_c in enumerate(idx):
        start, end = get_time_points(vocal_tract, n)
        inst[n, :] = start, end
        if n == len(idx) - 1:
            end = vocal_tract.time_vector[-1]
        idx_s, idx_e = [get_time_index(t, vocal_tract.time_vector) for
                        t in [start, end]]
        curr_p = [proba[:, idx_c]]
        if n > 0:
            idx_m1 = idx[n-1]
            curr_p.append(proba[:, idx_m1])
              
        if n < len(idx)-1:
            idx_p1 = idx[n+1]
            curr_p.append(proba[:, idx_p1])

        curr_p = np.array(curr_p)
        curr_p_range = curr_p[:, idx_s:idx_e+1]
        p_predict = np.argmax(curr_p, axis=0)
        p_predict_range = p_predict[idx_s:idx_e+1]
        nb_pred = np.sum(p_predict_range == 0)
        if n > 0 and p_predict_range[0] == 0 and curr_p_range[0, 0] > th:
            curr_n = 0
            curr_i = min(idx_s - curr_n, len(tvec)-1)
            inst[n, 0] = tvec[curr_i]

            while curr_i >= 0 and p_predict[curr_i] == 0 and curr_p[0, curr_i] > th:
                nb_pred += 1
                inst[n, 0] = tvec[curr_i]
                curr_n += 1
                curr_i = idx_s - curr_n
        else:
            idx_true = np.argwhere((p_predict_range==0) & 
                                   (curr_p_range[0, :] > th) & 
                                   (curr_p_range[0, :] >= proba_neutral[idx_s:idx_e+1]))
            if len(idx_true) > 0:
                inst[n, 0] = tvec[idx_true[0][0] + idx_s]

        if inst[n, 0] < start:
            inst[n, 0] = start
        if (n < len(idx) - 1 and p_predict_range[-1] == 0 and
            curr_p_range[0, -1] > th):
            curr_n = 0
            curr_i = idx_e + curr_n
            inst[n, 1] = tvec[curr_i]
            while (curr_i < len(vocal_tract.time_vector)-1 and 
                   p_predict[min(len(p_predict)-1, curr_i)] == 0 and 
                   curr_p[0, min(curr_p.shape[1]-1, curr_i)] > th):
                nb_pred += 1
                curr_n += 1
                curr_i = min(idx_e + curr_n, len(tvec)-1)
                inst[n, 1] = tvec[curr_i]
        else:
            idx_true = np.argwhere((p_predict_range==0) & 
                                   (curr_p_range[0, :] > th) & 
                                   (curr_p_range[0, :] >= proba_neutral[idx_s:idx_e+1]))
            if len(idx_true) > 0:
                inst[n, 1] = tvec[idx_s + idx_true[-1][0]]
        if inst[n, 1] > end:
            inst[n, 1] = end
    return rearrange_timing(inst)

def get_time_index(t, time_vector):
    idx = np.argwhere(time_vector >= t).astype(int)
    if len(idx) == 0:
        return len(time_vector) - 1
    elif len(idx) == 1:
        return idx[0][0]
    else:
        return idx.squeeze()[0]

def get_time_point_max(vocal_tract, n):
    return max([a.movements[n].time_points[1] for 
                a in vocal_tract.articulators])

def get_time_point_min(vocal_tract, n):
    return min([a.movements[n].time_points[0] for 
                a in vocal_tract.articulators])

def get_time_points(vocal_tract, n):
    return (get_time_point_min(vocal_tract, n), 
            get_time_point_max(vocal_tract, n+1))

def get_time_vector(t_max, sr):
    """ returns the time vector """
    return list(np.arange(0, t_max + 1/sr, 1/sr))

def get_timings(tvec, z):
    """ returns the timing and values of a tract variable targets """
    dz = np.diff(z)
    idx = [x+1 for x in range(len(dz)) if dz[x] != 0]
    times = [tvec[i] for i in idx]
    targets = [z[i-1] for i in idx]
    times.append(tvec[-1])
    targets.append(z[-1])

    return times, targets

def get_vocal_effort(vocal_tract, sr):
    """ Computes the vocal effort based on the subglottal pressure """
    return (np.sum(vocal_tract.subglottal_pressure.dynamics) / 2000) ** 2 / sr

def integrate(x, dt, method='linear'):
    if method == 'linear':
        return np.sum(x) * dt
    elif method == 'trapz':
        return np.trapz(x, dx=dt)

def interpolate(x_in, y_in, x_out, kind="linear", fill_value="extrapolate"):
    """ Interpolation """
    f1 = scintp.interp1d(x_in, y_in, kind=kind,
                         fill_value=fill_value)
    return f1(x_out)

def linspace(a, b, n):
    """ Faster implementation of numpy.linspace """
    return np.arange(a, b + (b-a)/(n-1), (b-a)/(n-1))[:n]

def max_list(x):
    """ returns the max value in nested lists """
    return max([item for sublist in x for item in sublist])

def movement_padding(vocal_tract):
    len_mov = [len(a.dynamics) for a in vocal_tract.articulators]
    if len(np.unique(len_mov)) > 1:
        for n in range(len(len_mov)):
            while len(vocal_tract.articulators[n].dynamics) < max(len_mov):
                vocal_tract.articulators[n].dynamics = np.append(vocal_tract.articulators[n].dynamics, 0)

def normalize_duration(duration):
    """ Returns a normalized version of durations """
    return [d/np.max(duration) if np.max(duration) != 0 else 0 for d in duration]

def np2blocs(npts):
    """ returns indices of activation for each gesture """
    pts_bloc = []
    for curr_n in npts:
        blocs = []
        bloc = []
        for i, n in enumerate(curr_n):
            if n > 0:
                bloc.append(i)
            else:
                if len(bloc) > 0:
                    blocs.append(bloc)
                bloc = []
        if len(bloc) > 0:
            blocs.append(bloc)
        pts_bloc.append(blocs)
    return pts_bloc

def psinv(A, W, inversion=True):
    """ Weighted Penrose-Moore pseudoinversion """
    x = np.array(A)
    if inversion:
        W_inv = np.linalg.inv(W)
    else:
        W_inv = W
    return W_inv @ x.T @ np.linalg.pinv(x @ W_inv @ x.T)

def read_json(file):
    """ read JSON file """
    with open(file, "r") as file_id:
        data = json.load(file_id)
    return data

def read_score(file, gestures):
    """ read gesture score files """
    score = read_json(file)
    names = []
    start = []
    duration = []
    for phon in score["phoneme"]:
        names.append(phon["name"])
        start.append(phon["start"])
        duration.append(phon["duration"])

    nb_gestures = len(gestures)
    nb_activ = len(names)
    start_vec = empty_list(nb_gestures)
    duration_vec = empty_list(nb_gestures)
    end_vec = empty_list(nb_gestures)
    for k_gest in range(nb_gestures):
        phon = gestures[k_gest]["name"]
        idx = [x for x in range(nb_activ) if names[x] == phon]
        if len(idx) > 0:
            for x in idx:
                start_vec[k_gest].append(start[x])
                duration_vec[k_gest].append(duration[x])
                end_vec[k_gest].append(duration[x] + start[x])

        else:
            start_vec[k_gest].append(-1)
            duration_vec[k_gest].append(-1)
            end_vec[k_gest].append(-1)

    return (start_vec, duration_vec, end_vec)

def read_weights(constituents, gestures):
    """ read weight files """

    constituent_sorted, types = sort_all_constituents(constituents)
    phonemes = constituent_sorted[-1]

    names = []
    effort = []
    duration = []
    precision = []
    for phon in phonemes:
        names.append(phon["label"])
        effort.append(phon["weights"]["effort"])
        duration.append(phon["weights"]["parsing duration"])
        precision.append(phon["weights"]["parsing precision"])

    nb_gestures = len(gestures)
    nb_activ = len(names)
    effort_vec = empty_list(nb_gestures)
    duration_vec = empty_list(nb_gestures)
    precision_vec = empty_list(nb_gestures)
    for k_gest in range(nb_gestures):
        phon = gestures[k_gest]["name"]
        idx = [x for x in range(nb_activ) if names[x] == phon]
        if len(idx) > 0:
            for x in idx:
                effort_vec[k_gest].append(effort[x])
                duration_vec[k_gest].append(duration[x])
                precision_vec[k_gest].append(precision[x])

        else:
            effort_vec[k_gest].append(0)
            duration_vec[k_gest].append(0)
            precision_vec[k_gest].append(0)

    return effort_vec, duration_vec, precision_vec

def rearrange_timing(surface_timing):
    """ Rearrange surface timing to remove gaps """
    new_timing = surface_timing
    for n in range(new_timing.shape[0]-1):
        if surface_timing[n, 1] < surface_timing[n+1, 0]:
            new_time = np.mean([surface_timing[n, 1], surface_timing[n+1, 0]])
            surface_timing[n, 1] = new_time
            surface_timing[n+1, 0] = new_time
        if surface_timing[n, 1] > surface_timing[n+1, 0]:
            surface_timing[n+1, 0] = surface_timing[n, 1]
    return new_timing

def set_configs(global_parameters, key, config_parameters):
    """ Sets hyperparameters from the parmaeters file """
    for k in global_parameters[key].keys():
        setattr(config_parameters, k, global_parameters[key][k])

def tp2np(vocal_tract, params):
    """ converts time points into indices """
    s_r = params.sampling_rate
    t_pp = vocal_tract.get_time_points()
    t_max = max_list(t_pp)
    t_vec = get_time_vector(t_max, s_r)
    nb_gestures = len(t_pp)
    n_pts = np.zeros((nb_gestures, len(t_vec)))
    for k_gest, t_tmp in enumerate(t_pp):
        for k_pts in range(int(len(t_tmp)/2)):
            (start, end) = (np.array(t_tmp[2*k_pts:2*(k_pts+1)]) * s_r).astype(int)
            end = min(end, len(t_vec))
            if t_tmp[2*k_pts+1] == t_max:
                end = len(t_vec)
            if end > start:
                n_pts[k_gest, start:end] = np.ones_like(n_pts[k_gest,
                                                              start:end])

    return n_pts, t_vec

def trajectory_tau(vocal_tract, prms, delta_t):
    """ compute tract variables trajectories using general tau theory """
    z_0 = vocal_tract.tract_target_function
    tvec = vocal_tract.time_vector
    nb_var, nb_frame = z_0.shape
    zvec = np.zeros_like(z_0)

    for k_var in range(nb_var):

        times, targets = get_timings(tvec, z_0[k_var,:].squeeze())
        curr_zvec = vocal_tract.tract_initial_state[k_var] * np.ones_like(tvec)
        if len(times) > 0:
            zk = vocal_tract.tract_initial_state[k_var, 0] - targets[0]
            zkm1 = vocal_tract.tract_initial_state[k_var, 0] - targets[0]
            targ_m1 = targets[0]
            idx_t = 1
            for t in tvec[1:]:

                K = vocal_tract.tract_stiffness_function[k_var, idx_t] / 5
                idx = [i for i in range(len(times)) if times[i] >= t][0]
                time = times[idx]
                target = targets[idx]

                if target != targ_m1:
                    zk = curr_zvec[idx_t-1] - target
                    zkm1 = curr_zvec[idx_t-1] - target
                    targ_m1 = target*1
                else:
                    TT = K / 2*(t-time**2 / t)
                    num = -TT / delta_t * zkm1
                    den = 1-TT / delta_t
                    zk = num / den
                    zkm1 = zk
                curr_zvec[idx_t] = zk + target
                idx_t += 1
        zvec[k_var, :] = curr_zvec.reshape(1, -1)

    return np.linalg.lstsq(prms.anatomy_matrix, zvec)[0], zvec

def update_effective_timings(names, start, duration):
    """ Returns an updated version of effective gestural timings """
    for k,  (n, s, d) in enumerate(zip(names, start, duration)):
        if n in ["a", "i"]:
            acc_start = s
            acc_end = s + d
            if k < len(names) - 1:
                if names[k+1] not in ["a", "i"]:
                    acc_end = start[k+1]
            if k > 0:
                if names[k-1] not in ["a", "i"]:
                    acc_start = start[k-1] + duration[k - 1]
            if k == 0:
                acc_start = 0
            duration[k] = acc_end - acc_start
            start[k] = acc_start
    return names, start, duration

def update_learning_ratio(sol, sol_m1, jac, jac_m1):
    """ computes new learning ratio """
    delta_x = (sol.reshape(-1,1) - sol_m1.reshape(-1,1)).T
    delta_j = jac.reshape(-1,1) - jac_m1.reshape(-1,1)
    return (np.abs(delta_x @ delta_j) /np.linalg.norm(delta_j)**2).squeeze()

def update_zkb(vocal_tract, k_frame, z_0, bnds, a_m, zm1, zm2):
    curr_z = z_0[:,k_frame].reshape(-1,1)
    curr_k = (vocal_tract.tract_stiffness_matrix @
             np.diag(vocal_tract.tract_stiffness_function[:, k_frame]))
    curr_b = crit_damp(vocal_tract, curr_k)
    curr_b += boundaries(bnds, a_m, zm2) * np.diag(np.sign(a_m @ zm1))

    return curr_z, curr_k, curr_b

def workspace() -> Path:
    """ Returns the Path object for computation workspace."""
    return Path.cwd()

def write_hdf5(output_name, *args):
    """ Writes HDF5 files """
    nargin = len(args)
    with h5py.File(output_name, "w") as hf:
        for k in range(0, nargin, 2):
            key, value = args[k:k+2]
            hf.create_dataset(key, data=value)

def write_json(data, output_name):
    """ Write JSON files """
    with open(output_name, "w") as file_id:
        json.dump(data, file_id, indent=4)
