#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 20 11:08:48 2021

@author: benjamin
"""

from ._plottools import (plot_vt_trajectories)
from .utils import (read_score,
                    read_gestures)

def plot_gestures(score_file, gestures_file):
    """ plots the planned articulator and tract functions """

    score = read_score(score_file)
    gestures = read_gestures(gestures_file)
    plot_vt_trajectories(score, gestures)