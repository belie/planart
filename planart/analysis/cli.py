#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  1 09:00:03 2021

@author: benjamin
"""

import click

from ._analysis import (
    plot_gestures
)


@click.version_option()
@click.group()
def cli_analysis_cmd():
    pass


@cli_analysis_cmd.command(
    name='plot-gestures',
    help='plot the planned articulatory gestures',
)

@click.argument(
    'score', type=click.Path(exists=False),
)
@click.argument(
    'gestures', type=click.Path(exists=False),
)
def plot_trajectories(score, gestures):
    plot_gestures(score, gestures)
