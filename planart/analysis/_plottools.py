#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 20 11:08:48 2021

@author: benjamin
"""

import matplotlib.pyplot as plt
import matplotlib
matplotlib.rcParams.update({'text.usetex': False,
                            'text.latex.preamble': '\\usepackage{tipa}'})
FONT_SIZE = 20

def close():
    plt.close("all")

def labels(x_label, y_label, font_size=FONT_SIZE, fontweight="bold"):
    plt.xlabel(x_label, fontsize=FONT_SIZE, fontweight='bold')
    plt.ylabel(y_label, fontsize=FONT_SIZE, fontweight='bold')
    set_ticks()

def plot_segmentation(score):
    lbls, starts, ends = score
    ylims = plt.gca().get_ylim()
    for lbl, start, end in zip(lbls, starts, ends):
        if start > 0:
            plt.vlines(start, ylims[0], ylims[1], colors = (0, 0.498, 0),
                       linestyles = "--")
        if end > 0:
            plt.vlines(end, ylims[0], ylims[1], colors = (0, 0.498, 0),
                       linestyles = "--")
            plt.text(0.5 * (start + end), ylims[1], 
                     special_characters(lbl),
                color=[ 0, 0.498, 0 ], fontsize=20, fontweight='bold',
                horizontalalignment='center', verticalalignment='top')

def plot_vt_trajectories(score, gesture):

    art_funcs = ['Jaw', 'Tongue body', 'Tongue tip', 'Upper lip', 'Lower lip']
    tract_funcs = ['Tongue body constriction', 'Tongue tip constriction',
                   'Lip aperture']
    
    art_dynamics = gesture[0]
    tract_dynamics = gesture[1]
    art_dynamics[0,:] *= -2
    art_dynamics[3,:] *= 1
    art_dynamics[4,:] += art_dynamics[0,:]
    art_dynamics[1:3,:] = tract_dynamics[:2,:]
    plt.figure()
       
    subplot_traj(score, gesture, 1, art_dynamics,
                       "Articulator functions", art_funcs)
    subplot_traj(score, gesture, 2, tract_dynamics,
                       "Tract functions", tract_funcs)
    plt.show()

def set_ticks(fs=FONT_SIZE):
    plt.xticks(fontsize=fs)
    plt.yticks(fontsize=fs)

def special_characters(x):
    y = x
    y = y.replace("x", "\\textipa{@}")
    y = y.replace("#", "\#")
    y = y.replace("_", "\_")
    #y = y.replace("~", "\~")
    y = y.replace("Z", "\\textipa{Z}")
    y = y.replace("S", "\\textipa{S}")
    y = y.replace("O", "\\textipa{O}")
    y = y.replace("^", "\\textipa{2}")
    y = y.replace("I", "\\textipa{I}")
       
    if "~" in y:
       y = y.replace("~", "")
       y = "\\textipa{\~" + y + "}"
    return y

def subplot_traj(score, gesture, k, attr, y_label, legs):
    ax = plt.subplot(2, 2, k)      
    lines = plt.plot(gesture[2],
                     attr.T, '--')
    labels("Time (s)", y_label)
    ax.set_title(y_label, fontsize=FONT_SIZE, fontweight='bold')
    write_legends(ax, lines, legs)
    plot_segmentation(score)
    
def write_legends(ax, lines, legs):
    for line, leg in zip(lines, legs):
        line.set_label(leg)
    ax.legend(loc="best")
