#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec 20 11:08:48 2021

@author: benjamin
"""

import h5py
import json

def read_gestures(file_name):
    """ Read gestures stored in HDF5 """
    with h5py.File(file_name, 'r') as hf:
        articulators = hf['articulators'][()]
        tract = hf['tract'][()]
        time_vector = hf["time"][()]

    return articulators, tract, time_vector

def read_score(file_name):
    """ Reads final score """
    with open(file_name, "r") as file_id:
        score = json.load(file_id)

    names = []
    start = []
    end = []
    for phon in score["phoneme"]:
        names.append(phon["name"])
        start.append(phon["start"])
        end.append(phon["start"] + phon["duration"])

    return names, start, end
