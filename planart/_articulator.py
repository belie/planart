#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 30 10:03:50 2021

@author: benjamin
"""

import copy
from .tautools import (
                        alternative_tau,
                        generate_tau_acceleration                        )
import numpy as np
import itertools
from .utils import ( 
                    compute_acceleration,
                    integrate,
                    read_json
                    )

class Articulator:
    name = None
    mass = None
    stiffness = None
    speech_ready_state = None
    parent = None
    movements = None
    dynamics = None
    acceleration = None
    effort_cost = None
    time_vector = None

    # Constructor method
    def __init__(self, *args):
        nargin = len(args)
        for k in range(0, nargin, 2):
            key, value = args[k:k+2]
            setattr(self, key, value)

    def copy(self):
        return copy.deepcopy(self)

    def effort(self, oversampling=1, model="tau", 
               acceleration_model="full_gradient"):
        """ Computes the articulatory effort of a seauence of movement units """
        self.effort_cost = np.mean([m.effort(oversampling=oversampling,
                                             model=model,
                                             acceleration_model=acceleration_model) for
                                    m in self.movements])
        return self.effort_cost

    def generate_trajectory(self, oversampling=1, model="tau", deriv=0):
        """ Generates the articulatory trajectory of a seauence of movement units """
        dynamics = [m.generate_trajectory(oversampling=oversampling, 
                                          model=model) for
                    m in self.movements]
        
        positions, oversampled_positions = [[d[i] for d in dynamics] for 
                                             i in [0, 1]] 
        # self.dynamics = np.array([positions[0][0]] +
        #                  list(itertools.chain.from_iterable([d[1:] for
        #                                                      d in positions]))).squeeze()
        self.dynamics = np.array(list(itertools.chain.from_iterable([d for
                                                             d in positions]))).squeeze()
        self.time_vector = np.array(list(itertools.chain.from_iterable([m.time_vector for
                                                             m in self.movements]))).squeeze()
        tu, idx = np.unique(self.time_vector, return_index=True)
        if len(tu) < len(self.time_vector):
            self.time_vector = tu
            self.dynamics = [self.dynamics[i] for i in idx]
        sr = self.movements[0].sampling_frequency * oversampling
        if oversampling > 1:
            new_dynamics = np.array([oversampled_positions[0][0]] +
                             list(itertools.chain.from_iterable([d[1:] for
                                     d in oversampled_positions]))).squeeze()  
            self.acceleration = compute_acceleration(new_dynamics, sr)
            nb_pts = [len(p) for p in oversampled_positions]
        else:
            self.acceleration = compute_acceleration(self.dynamics, sr)
            nb_pts = [len(p) for p in positions]
        start = 0
        end = 1
        for n, m in enumerate(self.movements):
            start = end - 1
            end = start + nb_pts[n]
            m.acceleration = self.acceleration[start:end]
            
class Movement:
    time_points = None
    position = None
    kappa = None
    dynamics = None
    acceleration = None
    sampling_frequency = None
    parent = None
    weights = None
    index = None
    name = None
    time_vector = None

    # Constructor method
    def __init__(self, *args):
        nargin = len(args)
        for k in range(0, nargin, 2):
            key, value = args[k:k+2]
            setattr(self, key, value)

    def copy(self):
        return copy.deepcopy(self)

    def effort(self, oversampling=1, model="tau", 
               acceleration_model="full_gradient"):
        """ Computes the articulatory effort of a movement unit """
        dt = 1/self.sampling_frequency/oversampling
        C = self.parent.mass ** 2
        if (self.acceleration is not None and
            acceleration_model == "full_gradient"):
            e = C * integrate((self.acceleration/6)**2, dt)
        elif acceleration_model == "movement_gradient":
            acc = np.gradient(np.gradient(self.dynamics[1].squeeze()/6, dt), dt)
            e = C * integrate(acc**2, dt)
        elif acceleration_model == "analytic":
            e = C * integrate((self.dynamics[1].squeeze()/6)**2, dt) 
        elif acceleration_model == 'stiffness':
            C = self.parent.stiffness ** 2
            e = C * integrate((self.dynamics[1].squeeze())**2, dt) 
        elif acceleration_model == 'mass-stiffness':
            force = (self.parent.mass * self.acceleration/6 + 
                 self.parent.stiffness * self.dynamics[1].squeeze())
            e = integrate(force**2, dt)
            
        return e * self.weights["effort"]

    def generate_trajectory(self, oversampling=1, model="tau",
                            acceleration_model="full_gradient"):
        """ Generates the articulatory trajectory of a movement unit """
        if model == "tau":
            T = self.time_points[1] - self.time_points[0]
            # (self.dynamics, 
            #  self.time_vector) = generate_tau_position(self.kappa,
            #                                            self.position[0],
            #              self.position[1], T, sr=self.sampling_frequency)
            (self.dynamics, 
             self.time_vector) = alternative_tau(self.kappa,
                                                       self.position[0],
                         self.position[1], self.time_points, 
                         sr=self.sampling_frequency)
                                                       
            if oversampling > 1:
                # new_dynamics = generate_tau_position(self.kappa, 
                #                 self.position[0], self.position[1], T, 
                #                 sr=self.sampling_frequency*oversampling)[0]    
                new_dynamics = alternative_tau(self.kappa,
                                                       self.position[0],
                         self.position[1], self.time_points, 
                         sr=self.sampling_frequency*oversampling)[0] 
                self.dynamics = [self.dynamics, new_dynamics]
            else:
                self.dynamics = [self.dynamics]*2
            if acceleration_model == "analytic":
                self.dynamics[1] = generate_tau_acceleration(self.kappa, 
                             self.position[0], self.position[1], T, 
                             sr=self.sampling_frequency*oversampling)[0]
        return self.dynamics

def init_tau_articulators(targets, labels, prms, starts, ends, weights=None,
                          masses=None, stiffness=None, 
                          end_mov=True, initial_position=0):
    """ Creates articulator objects from initial score """
    
    keys = targets[0]["articulatory parameters"].keys()
    arts = []
    if initial_position == "random":
        initial_position = np.random.uniform(-3, 3, len(keys)).tolist()
    elif not isinstance(initial_position, list):
        initial_position = [initial_position]*len(keys)
    
    for k, key in enumerate(keys):
        if masses is None:
            mass = 1e-3
        else:
            mass = masses[key]
        if stiffness is None:
            K = 1
        else:
            K = stiffness[key]
        AA = Articulator("movements", [],
                            "name", key,
                            "mass", mass,
                            "stiffness", K)
        for n, l in enumerate(labels):
            curr_target = targets[n] 
            end_point = curr_target["articulatory parameters"][key]
            if n == 0:
                start_point = initial_position[k]
                time_points = (0, ends[n])
            else:
                start_point = AA.movements[-1].position[1]
                time_points = (starts[n], ends[n])
            if weights is None:
                curr_weights = {"effort": prms.effort_weight,
                           "parsing duration": prms.parsing_weight,
                           "parsing precision": prms.parsing_weight,
                           "prosody": prms.prosodic_weight,
                           "duration": prms.duration_weight}
            else:
                curr_weights = weights[n]
            if prms.initial_kappa == "random":
                ki = np.random.uniform(0.1, 0.9)
            else:
                ki = float(prms.initial_kappa)
            M = Movement("kappa", ki,
                            "time_points", time_points,
                            "position", (start_point, end_point),
                              "sampling_frequency", prms.sampling_rate,
                              "weights", curr_weights,
                              "parent", AA,
                              "name", l,
                              "index", prms.probabilistic_model.labels.index(l))
            AA.movements.append(M)
        if end_mov:
            M = Movement("kappa", 0.4,
                            "time_points", (ends[-2], ends[-1]),
                            "position", (AA.movements[-1].position[1], 0),
                            "sampling_frequency", prms.sampling_rate,
                            "weights", {"effort": 0,
                                       "parsing duration": 0,
                                       "parsing precision": 0,
                                       "prosody": 0,
                                       "duration": 0},
                            "name", "#",
                            "parent", AA)
            AA.movements.append(M)
        arts.append(AA)
    return arts

def read_solution(solution_file, sro=None):
    """ Reads solution file and returns articulators """
    json_solution = read_json(solution_file)
    solution = json_solution["solution"]
    if sro == None:
        sampling_rate = json_solution["sampling rate"]
    else:
        sampling_rate = sro
    names = [a["name"] for a in solution[0]["articulators"]]
    return [read_articulator(solution, name, sampling_rate) for name in names]

def read_articulator(solutions, name, sr):
    """ Create an articulator object"""
    art = Articulator("movements", [read_movement(solution, name, sr) for
                           solution in solutions],
                        "name", name,
                        "mass", 1e-3)
    [setattr(m, "parent", art) for m in art.movements]
    return art

def find_articulator(solutions, name):
    """ Finds index of articulators in json solutions """
    idx = [i for i in range(len(solutions)) if solutions[i]["name"] == name]
    if len(idx) > 0:
        return solutions[idx[0]]
    else:
        raise ValueError(name, ": bad articulator name")

def read_movement(solution, name, sr):
    """ Read a movement for a specified articulator """

    actual_art = find_articulator(solution["articulators"], name)
    return Movement("kappa", actual_art["kappa"],
                    "time_points", (actual_art["onset"]["time"],
                                    actual_art["offset"]["time"]),
                    "position", (actual_art["onset"]["position"],
                                    actual_art["offset"]["position"]),
                      "sampling_frequency", sr,
                      "weights", actual_art["weights"],
                      "name", solution["name"],
                      "index", actual_art["index"])

class OptimParameter:
    parent = None
    kappa = None
    position = None
    duration = None
    pressure = None

    # Constructor method
    def __init__(self, *args):
        nargin = len(args)
        for k in range(0, nargin, 2):
            key, value = args[k:k+2]
            setattr(self, key, value)

    def number_of_parameters(self, nb_articulators):
        return np.sum([local_number_of_parameters(p, nb_articulators) for
                       p in [self.kappa, check_position(self.position),
                             self.duration]])

def check_position(x):
    if x == "global":
        return "local"
    else:
        return x

def create_subglottal_pressure(vocal_tract, solution, prms, kappa=0.4):
    Psub = Articulator("name", "subglottal pressure", "parent", vocal_tract)    
    offset = vocal_tract.time_vector[-1]
    end_points = [0] +  np.cumsum(solution).tolist() + [offset]
    positions = [0] + [prms.subglottal_pressure] * len(solution) + [0]
    Psub.movements = [Movement("time_points", (end_points[n], end_points[n+1]),
                               "kappa", kappa,
                                "position", (positions[n], positions[n+1]),
                                "sampling_frequency", prms.sampling_rate) for 
                      n in range(len(solution) + 1)]
    Psub.generate_trajectory()
    vocal_tract.subglottal_pressure = Psub

def local_number_of_parameters(parameter, nb_articulators):
    """ Returns the number of parameters to optimize for a given movement """
    if parameter == "fixed":
        return 0
    elif parameter == "global":
        return 1
    elif parameter == "local":
        return nb_articulators
    


