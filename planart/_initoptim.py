# -*- coding: utf-8 -*-
"""
Created on Thu Feb  2 09:57:59 2023

@author: belie
"""

# from .gmmtools import get_proba, get_vowel_index
from ._proba import phoneme_probability
import os
from scipy.optimize import minimize
import numpy as np

def check_global_optimization(local_min_cost, local_min_x, 
                              global_min_cost, global_min_x):
    """ Check whether the local minimum in the local loop
    is temporary a global one """
    if local_min_cost < global_min_cost:        
        global_min_x = local_min_x
        global_min_cost = local_min_cost
        
    return global_min_cost, global_min_x

def check_local_optimization(new_x, probabilistic_model, local_min_cost, 
                             stub_const, min_x, prms, stub_count, idx_phoneme):
    """ Check whether the local minimum is temporary a global one """
    
    new_cost = get_init_cost_function(new_x, probabilistic_model, idx_phoneme)
    isContinue = True
    gain = np.abs((new_cost - local_min_cost) / local_min_cost)
    if new_cost < local_min_cost:
        local_min_cost = new_cost
        min_x = new_x
        
        if gain > prms.convergence_threshold:
            stub_count = stub_const
        else:
            stub_count -= 1
            if stub_count == 0:
                isContinue = False  
    else:
        stub_count -= 1
        if stub_count == 0:
            isContinue = False
    return min_x, stub_count, local_min_cost, isContinue

def get_init_cost_function(x, probabilistic_model, idx):
    return 1-phoneme_probability(np.array(x).reshape(-1, 1), 
                               probabilistic_model)[0].squeeze()[idx]

def optimal_initialization(probabilistic_model, idx_phoneme, prms, x0=None, 
                           verbosity=False):
    """ Performs global optimization """
    
    if x0 is not None:
        global_min_cost = get_init_cost_function(x0, probabilistic_model,
                                            idx_phoneme)
        global_min_x = [x for x in x0]
    else:
        global_min_cost = np.inf
        global_min_x = None
    
    stub_const = prms.attempts
    
    if prms.parallel <= 1 or prms.repetitions <= 1:
        for k in range(prms.repetitions):
            local_min_cost, local_min_x = repetition(k, prms, x0, 
                                                     stub_const, 
                                                     global_min_cost, 
                                                     probabilistic_model, 
                                                     idx_phoneme, 
                                                     verbosity=verbosity)
           
            global_checks = check_global_optimization(local_min_cost, local_min_x, 
                                          global_min_cost, global_min_x)
            global_min_cost, global_min_x = global_checks 
    else:
        from joblib import Parallel, delayed
        if os.name == "nt":
            backend = "threading"
        else:
            backend = "loky"
        solutions = Parallel(n_jobs=prms.parallel, backend=backend,
                         prefer="processes")(delayed(repetition)(k, prms, x0, 
                                                                  stub_const, 
                                                                  None, 
                                                                  probabilistic_model, 
                                                                  idx_phoneme,
                                                                  verbosity=verbosity) for
                                                 k in range(prms.repetitions))
        for solution in solutions:       
            local_min_cost, local_min_x = solution                      
            global_checks = check_global_optimization(local_min_cost, local_min_x, 
                                          global_min_cost, global_min_x)
            global_min_cost, global_min_x = global_checks 
        
    if verbosity:
        print("Optimization completed", flush=True)
        print("Global minimum: ", global_min_cost, flush=True)
        
    return global_min_x

def local_optimization(min_x, probabilistic_model, idx):
    """ Run an optimization procedure to find local minimum """
    rand_nudge = 1 + (0.05 - 0.10*np.random.rand(len(min_x)))
    x = (rand_nudge * min_x).tolist()        
    args = (probabilistic_model, idx)
    return optimization(x, args) 

def objective(x, *args):
    """ Objective function """
    probabilistic_model, idx = args
    return get_init_cost_function(x, probabilistic_model, idx)

def optimization(x0, args, maxiter=200, method='nelder-mead'):
    """ Optimization procedure """

    bounds = ((-3, 3), )*7
    options={'maxiter': maxiter * len(x0)}    
    return minimize(objective, x0, args=args, method=method,
                          options=options, bounds=bounds)["x"]
 
# def phoneme_probability(dynamics, probabilistic_model):
#     """ Generates the phoneme probability from the articulatory trajectory """
    
#     inputs = probabilistic_model.scaler.transform(dynamics.T)
#     return (get_proba(inputs, model=probabilistic_model.model),
#             get_vowel_index(inputs, model=probabilistic_model.model)) 
   
def repetition(k, prms, x0_start, stub_const, global_min_cost, 
               probabilistic_model, idx_phoneme, verbosity):
    """ Perform one otpimization run """
    if x0_start is not None:
        min_x = [xx for xx in x0_start]
    else:
        min_x = (6*np.random.rand(7)-3).tolist()
    stub_count = stub_const
    local_min_cost = get_init_cost_function(min_x, probabilistic_model, idx_phoneme)
    isContinue = True
           
    while isContinue:
        optim_x = local_optimization(min_x, probabilistic_model, idx_phoneme)
        
        local_checks = check_local_optimization(optim_x, probabilistic_model,
                                                local_min_cost, 
                                     stub_const, min_x, prms,
                                     stub_count, idx_phoneme)
        min_x, stub_count, local_min_cost, isContinue = local_checks
       
        if verbosity >= 1 and global_min_cost is not None:
            print("Current repetition: ", k+1, " /", prms.repetitions, flush=True)
            print("Global cost function ", global_min_cost, flush=True)
            print("Local cost function: ", local_min_cost, flush=True)        
            print("New cost :", get_init_cost_function(optim_x, probabilistic_model,
                                                  idx_phoneme),
                  flush=True)
            print("Number of remaining attempts :",stub_count, flush=True)
            
    return local_min_cost, min_x