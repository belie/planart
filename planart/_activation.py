#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 12 18:48:21 2022

@author: benjamin
"""

import copy
import numpy as np
from .utils import crit_damp, psinv
from ._subclasses import (Anatomy, Space)

class Activation:
    """ Activation stores activation quantities """

    # pylint: disable=too-many-instance-attributes
    # pylint: disable=pointless-string-statement

    parent = None
    task_space = None
    articulatory_space = None
    anatomy = None
    gestures = None

    # Constructor method
    def __init__(self, *args):
        nargin = len(args)
        for k in range(0, nargin, 2):
            key, value = args[k:k+2]
            setattr(self, key, value)

    def copy(self):
        """ Returns a copy of the instance """
        return copy.deepcopy(self)

    def get_activations(self):
        """ Computes other activation quantities """
        self.task_space.mass = (self.anatomy.general_matrix @
                                  self.parent.articulatory_space.mass @
                                  psinv(self.anatomy.general_matrix,
                                        self.parent.anatomy.inverse_matrices[0],
                                        inversion=False))
        self.task_space.damping = crit_damp(self.task_space, diagonal=True)
        self.articulatory_space.stiffness = (psinv(self.anatomy.general_matrix,
                                       self.parent.anatomy.inverse_matrices[1],
                                       inversion=False) @
                                     self.task_space.stiffness @
                                     self.anatomy.general_matrix)

        self.articulatory_space.damping = (psinv(self.anatomy.general_matrix,
                                     self.parent.anatomy.inverse_matrices[1],
                                     inversion=False) @
                                     self.task_space.damping @
                                     self.anatomy.general_matrix)

    def gest2activation(self):
        """ Extract activation matrix from active gestures """
        self.gestures = [g for g in self.parent.gestures if g.activation]
        if len(self.gestures) > 0:
            self.anatomy = Anatomy("parent", self)
            self.anatomy.make_anatomy_matrix(False)
            self.articulatory_space = Space("parent", self)
            self.articulatory_space.get_target(False)

            self.articulatory_space.target = (psinv(self.anatomy.general_matrix,
                                        self.parent.anatomy.inverse_matrices[1],
                                        inversion=False) @
                                    self.articulatory_space.target).reshape(-1, 1)
            self.task_space = Space("parent", self)
            self.task_space.get_stiffness(False)
            self.task_space.apply_general_stiffness(self.parent.general_stiffness)

    def list2array(self, keys):
        """ Converts lists into numpy arrays """
        for key in keys:
            setattr(self, key, np.array(getattr(self, key)))
