# -*- coding: utf-8 -*-
"""
Created on Wed May 22 15:11:46 2024

@author: belie
"""

from keras import (
                    Input,
                    layers,
                    Model,
                    models,
                    optimizers
                    )
from os import path
import pickle
import tensorflow as tf
from .utils import (
                    check_config,
                    )


def compile_model(config):
    
    if config["optimizer"] == "adagrad":
        optimizer = optimizers.Adagrad(learning_rate=config["learning rate"])
    elif config["optimizer"] == "adam":
        optimizer = optimizers.Adam(learning_rate=config["learning rate"])
    else:
        raise ValueError(""" Optimizer type """ + config["optimizer"] +
                         """" is not recognized.
                         Should be either adam or adagrad""")
    config["model"]["model"].compile(optimizer=optimizer, loss=config["loss"])

def create_model(config, do_compile=True):

    input_activation = check_config(config, 'input activation', 'tanh')
    activation_function = check_config(config, 'activation', 'tanh')
    hidden_layer_size = check_config(config, 'hidden layer size', (100, ))
    nb_feat_input = check_config(config, 'input dimension', 7)
    nb_feat_output = config['output dimension']
    output_activation = check_config(config, 'output activation', 'linear')
    loss_function = check_config(config, 'loss function', 'mean_squared_error')
    metrics = check_config(config, 'metrics', 'mean_squared_error')

    input_art = Input(shape=(nb_feat_input,))
    nb_layers = len(hidden_layer_size)

    hidden_layers = [layers.Dense(hidden_layer_size[0],
                                activation=input_activation)(input_art)]
    for n in range(1, nb_layers):
        hidden_layers.append(layers.Dense(hidden_layer_size[n],
                                activation=activation_function)(hidden_layers[n-1]))
    decoded = layers.Dense(nb_feat_output,
                            activation=output_activation)(hidden_layers[-1])
    encoder = Model(input_art, decoded)
    if do_compile:
        encoder.compile(optimizer='adam',
                        loss=loss_function, metrics=[metrics])
    return encoder



def fit_keras_model(model, training_dataset, nb_epochs, batch_size,
              validation_dataset, verbose, config):
    model.fit(training_dataset[0], training_dataset[1],
                    epochs=nb_epochs,
                    batch_size=batch_size,
                    shuffle=True,
                    validation_data=validation_dataset,
                    verbose=verbose,
                    callbacks=config['callback'])
    return model

def import_model(model_file, with_scalers=True):
    model = models.load_model(model_file, compile=False)
    model_prefix = path.splitext(model_file)[0]
    if with_scalers:
        scalers = pickle.load(open(model_prefix + '.scl', 'rb'))
        return model, scalers
    else:
        return model

def init_model(dataset, config):
    validation_dataset = (dataset['input']['validation'],
                          dataset['output']['validation'])
    nb_epochs = check_config(config, 'epochs', 50)
    batch_size = check_config(config, 'batch size', 32)
    verbose = check_config(config, 'verbose', True)
    config['output dimension'] = dataset['output']['validation'].shape[1]
    model = create_model(config)
    if verbose:
        model.summary()
    return model, validation_dataset, nb_epochs, batch_size, verbose

def make_callbacks(fpath, monitor='val_loss', save_best_only=False, patience=0):
    callback = [tf.keras.callbacks.ModelCheckpoint(filepath=fpath,
                                               monitor=monitor,
                                                save_best_only=save_best_only),
            tf.keras.callbacks.TerminateOnNaN()]
    if patience > 0:
        callback += [tf.keras.callbacks.EarlyStopping(monitor='val_loss',
                                                      patience=patience)]
    return callback

def sensory_model_update(sensory_model, motor_commands, sensory_feedback):
    
    x_train_scaled = sensory_model["scalers"][0].transform(motor_commands)
    y_train_scaled = sensory_model["scalers"][1].transform(sensory_feedback)
    
    sensory_model['model'].train_on_batch(
        x_train_scaled, 
        y_train_scaled
        )    

def train_keras_mlp(config, training_dataset, validation_dataset):
    config['output dimension'] = training_dataset[1].shape[1]
    model = create_model(config)
    nb_epochs = check_config(config, 'epochs', 50)
    batch_size = check_config(config, 'batch size', 32)
    verbose = check_config(config, 'verbose', True)
    if verbose:
        model.summary()
    return fit_keras_model(model, training_dataset, nb_epochs, batch_size,
                  validation_dataset, verbose, config)