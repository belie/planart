# -*- coding: utf-8 -*-
"""
Created on Fri Mar 31 16:22:37 2023

@author: belie
"""

import numpy as np

def compute_per(reference, prediction, penalty=None):
    """ Computes the Phoneme-Error-Rate as the Levenshtein ddistance between
    the reference sting of phonemes and the predicted one.
    It uses the Wagner-Fischer algorithm and returns a dictionary containing
    the PER, as well as the correct predictions, the substitions, deletion
    and insertions """

    l_ref = len(reference)
    l_pred = len(prediction)
    if penalty is None:
        penalty = {"deletion": 1,
                   "substition": 1,
                   "insertion": 1}

    dist_mtx = np.zeros((l_pred+1, l_ref+1), dtype=int)
    backtrace = np.zeros((l_pred+1, l_ref+1), dtype=int)

    dist_mtx[:, 0] = range(l_pred+1)
    dist_mtx[0, :] = range(l_ref+1)

    backtrace[0, :] = 3
    backtrace[:, 0] = 2
    ids = [3, 2, 1]

    for j in range(1, l_ref+1):
        for i in range(1, l_pred+1):
            sub_cost = int(reference[j-1] != prediction[i-1])

            operation_vec = [dist_mtx[i, j-1] + 1,
                             dist_mtx[i-1, j] + 1,
                             dist_mtx[i-1, j-1] + sub_cost]
            dist_mtx[i, j] = min(operation_vec)
            operation = np.argmin(operation_vec)
            backtrace[i, j] = ids[int(operation)]*sub_cost

    i = l_pred
    j = l_ref
    corrects = {'number': 0, 'instances': []}
    deletions = {'number': 0, 'instances': []}
    substitutions = {'number': 0, 'instances': []}
    insertions = {'number': 0, 'instances': []}
    while i > 0 or j > 0:
        if backtrace[i, j] == 0:
            corrects['number'] += 1
            i -= 1
            j -= 1
            corrects['instances'].append(reference[j])
        elif backtrace[i, j] == 1:
            substitutions['number'] += 1
            i -= 1
            j -= 1
            substitutions['instances'].append(":".join([reference[j],
                                                      prediction[i]]))
        elif backtrace[i, j] == 2:
            insertions['number'] += 1
            i -= 1
            insertions['instances'].append(prediction[i])
        elif backtrace[i, j] == 3:
            deletions['number'] += 1
            j -= 1
            deletions['instances'].append(reference[j])

    return {'per': dist_mtx[-1, -1] / l_ref, 'correct': inverse_instance(corrects),
                          'deletion': inverse_instance(deletions),
                          'insertion': inverse_instance(insertions),
                          'substition': inverse_instance(substitutions)}

def get_per(vocal_tract, prediction, neutral_phoneme):
    reference = [p for p in vocal_tract.reference_phonemes]
    return compute_per(reference, prediction)['per']# * len(reference) / l
    

def inverse_instance(x):
    """ inverse order of instances during PER computation """
    x['instances'] = x['instances'][::-1]
    return x

def phoneme_prediction(vocal_tract, probabilistic_model, len_win=1):
    X = probabilistic_model.scaler.transform(vocal_tract.articulatory_space.dynamics.T)
    predicted_indices = probabilistic_model.model.predict(X).astype(int)
    raw_prediction = [probabilistic_model.labels[p] for p in predicted_indices]
    vocal_tract.predicted_phonemes = phoneme_sequence(raw_prediction,
                                                      len_win=len_win)
    return vocal_tract.predicted_phonemes
            
    
def phoneme_sequence(phonemes, len_win=1):
    """ Returns the phoneme sequence from 
    the frame-by-frame predicted phonemes """
    sequence = [phonemes[0]]
    n = -1
    for p in phonemes[1:]:
        n += 1
        if p != sequence[-1] and n > len_win:
            sequence.append(p)
            n = -1        
    return sequence