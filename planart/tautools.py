#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 18 14:14:10 2022

@author: benjamin
"""

import numpy as np
from ._proba import phoneme_probability
from ._intensity import intensity_score
from .utils import (
                    get_time_point_min,
                    get_time_points,
                    integrate,
                    linspace
                    )

stop_consonants = ["p", "t", "k", "b", "d", "g"]
fricatives = ["f", "s", "S", "v", "z", "Z"]

def alternative_tau(k, initial_position, final_position, time_points, sr):
    T = time_points[1] - time_points[0]
    ti = np.ceil(time_points[0]*sr)/sr - time_points[0]
    tf = np.floor(time_points[1]*sr)/sr - time_points[0]
    N = int((tf-ti)*sr) + 1
    t = np.linspace(ti, tf, N)
    t_ratio = (1-(np.square(t).astype('f')/np.square(T).astype('f')))
    return ((initial_position - final_position)*t_ratio**(1/k) + final_position,
            t + time_points[0])

def cost_parsing_precision(vocal_tract, prms, labels, weights):
    """ Returns the parsing cost given the formants and the target phonemes """

    dynamics = vocal_tract.articulatory_space.dynamics
    if vocal_tract.probability is None:
        proba = phoneme_probability(dynamics, prms.probabilistic_model)
    else:
        proba = vocal_tract.probability
    if vocal_tract.intensity is None and prms.intensity_model is not None:
        if prms.pressure_opt:
            vocal_tract.subglottal_pressure.generate_trajectory()
            psub = np.array(vocal_tract.subglottal_pressure.dynamics).reshape(-1, 1)
            if len(psub) > dynamics.shape[1]:
                psub = psub[:dynamics.shape[1]]
            if len(psub) < dynamics.shape[1]:
                psub = np.pad(psub.squeeze(), 
                              (0,dynamics.shape[1] - len(psub))).reshape(-1,1)
        else:
            psub = prms.subglottal_pressure  
        f0 = prms.fundamental_frequency
        if isinstance(psub, float) or isinstance(psub, int):
            psub = np.array([psub] * dynamics.shape[1]).reshape(-1, 1)
        if isinstance(f0, float) or isinstance(f0, int):
            f0 = np.array([f0] * dynamics.shape[1]).reshape(-1, 1)        
        p = np.hstack((dynamics.T, f0, psub))
        intensity = intensity_score(p, prms.intensity_model, 
                                    prms.background_noise)
    else:
        intensity = vocal_tract.intensity
    if intensity is None:
        intensity = np.ones(dynamics.shape[1])
        
    pmat = []
    nb_mov = len(vocal_tract.articulators[0].movements)
    for n, i in enumerate(labels):
        l = prms.probabilistic_model.labels[i]
        if n < nb_mov - 1:
            idx_start, idx_end = [int(T*prms.sampling_rate) for
                                  T in get_time_points(vocal_tract, n)]
        else:
            idx_start = int(get_time_point_min(vocal_tract,
                                               n)*prms.sampling_rate)
            idx_end = proba.shape[0]-1
        ph_proba = proba[idx_start:idx_end+1, i].squeeze()
        if prms.intelligibility_threshold_type == "absolute":
            threshold = prms.intelligibility_threshold
        elif prms.intelligibility_threshold_type == "relative":
            threshold = np.max(ph_proba)*prms.intelligibility_threshold
        else:
            raise ValueError(""" intelligibility threshold type not recognized.
                             Should be either asolute or relative """)
        ph_proba = [p for p in ph_proba if p >= threshold]
        if prms.intelligibility_model == "integral":            
            prob = 1 - np.max(ph_proba) * integrate(ph_proba, 
                                                    1/prms.sampling_rate)
        elif prms.intelligibility_model == "max":
            if l in stop_consonants or l in fricatives:
                c = prms.consonant_timing_factor / weights[n]["parsing duration"]
                curr_intensity = 1
            else:
                c = prms.vowel_timing_factor / weights[n]["parsing duration"]
                curr_intensity = np.max(intensity[idx_start:idx_end+1])
            dur = np.count_nonzero(ph_proba >= threshold)
            dg = 2 / np.pi * np.arctan(c  * dur / prms.sampling_rate)
            prob = 1 - np.max(ph_proba) * dg * curr_intensity
            
        else:
            raise ValueError(""" intelligibility model not recognized.
                             Should be either integral or max """)
        pmat.append(prob)

    """ Give more weight to stop consonants (here doubling the weights):
        prevents lenition to happen too often """        
    parsing_precision = [p * w["parsing precision"] * 
                         (1 + (prms.consonant_parsing_weight-1) * 
                          (prms.probabilistic_model.labels[i] in stop_consonants)) for 
                         p, w, i in zip(pmat, weights, labels)]

    return np.sum(parsing_precision)

def extract_duration_solution(vocal_tract, idx_movement):
    """ Returns the solutions of duration """
    if vocal_tract.optimization.duration == "fixed":
        return []
    else:
        solutions = [np.diff(a.movements[idx_movement].time_points)[0] for
                     a in vocal_tract.articulators]
        if (vocal_tract.optimization.duration == "global" or
            idx_movement == len(vocal_tract.articulators[0].movements)-1):
            return [solutions[0]]
        elif vocal_tract.optimization.duration == "local":
            return solutions

def extract_kappa_solution(vocal_tract, idx_movement):
    """ Returns the solutions of kappa """
    if vocal_tract.optimization.kappa == "fixed":
        return []
    else:
        solutions = [a.movements[idx_movement].kappa for
                     a in vocal_tract.articulators]
        if vocal_tract.optimization.kappa == "global":
            return [solutions[0]]
        elif vocal_tract.optimization.kappa == "local":
            return solutions

def extract_parameters(vocal_tract, end_mov=True):
    """ Returns the number of articulators and gestures """
    nb_art = len(vocal_tract.articulators)
    nb_gests = len(vocal_tract.articulators[0].movements) - int(end_mov)
    nb_param = vocal_tract.optimization.number_of_parameters(nb_art)
    return nb_art, nb_gests, nb_param

def extract_position_solution(vocal_tract, idx_movement):
    """ Returns the solutions of end point positions """
    if vocal_tract.optimization.position == "fixed":
        return []
    else:
        return [a.movements[idx_movement].position[-1] for
                     a in vocal_tract.articulators]
    
def extract_pressure_solution(vocal_tract):
    """ Returns the solutions of end point positions """
    positions = [m.position[-1] for 
                 m in vocal_tract.subglottal_pressure.movements[:-1]]
    durations = [np.diff(m.time_points)[0] for
                 m in vocal_tract.subglottal_pressure.movements[:-1]]
    xtmp = []
    for p, d in zip(positions, durations):
        xtmp.append(p)
        xtmp.append(d)
    return xtmp

def find_vowel_boundaries(which_vwl, idx_proba, start=0):
    nb_frame = len(which_vwl)
    idx = [i+start for i in range(nb_frame-start) if which_vwl[i+start] == idx_proba]
    if len(idx) == 0:
        return None, None
    else:
        start = idx[0]
        idx = [i+start for i in range(nb_frame-start) if which_vwl[i+start] != idx_proba]
        if len(idx) == 0:
            return start, nb_frame
        else:
            return start, idx[0]

def generate_tau(k, initial_position, final_position, T, sr=1, oversampling=1,
                 position=True, velocity=False, acceleration=False,
                 np_lin=False):
    """ Returns the position velocity and acceleration of a Tau-guided movement """

    N = int(T*sr) + 1
    if np_lin:
        t = np.linspace(0, T, N)
    else:
        t = linspace(0, T, N)
    

    X0 = (initial_position - final_position)
    t_ratio = (1-(np.square(t).astype('f')/np.square(T).astype('f')))
    # t_ratio[t_ratio<=0] = 0 ## Prevent some NaN values due to strange behavior of Numpy arrays
    if position:
        x = X0*t_ratio**(1/k) + final_position
    else:
        x = None
    if velocity:
        v = abs(2*X0*t/(k*T**2)*(t_ratio)**(1/k-1))
    else:
        v = None
    if acceleration:
        if oversampling > 1:
            N = int(T*sr*oversampling) + 1
            t = linspace(0, T, N)
        t_ratio = -t**2/T**2 + 1
        t_ratio[t_ratio <= np.finfo(float).eps] = np.finfo(float).eps

        a = (4*X0*t**2*(t_ratio)**(1/k - 2)*(1/k - 1)/(k*T**4) -
             2*X0*(t_ratio)**(1/k - 1)/(k*T**2))
    else:
        if oversampling > 1:
            N = int(T*sr*oversampling) + 1
            t = linspace(0, T, N)
            t_ratio = (1-(np.square(t).astype('f')/np.square(T).astype('f')))
        if oversampling == 1 and position:
            a = x
        else:
            a = X0*t_ratio**(1/k) + final_position
    return (x, v, a), t

def generate_tau_position(k, initial_position, final_position,
                          T, sr=1, np_lin=False):
    """ Returns the position velocity and acceleration of a Tau-guided movement """

    initial_gap = (initial_position - final_position)
    t_ratio, t = make_time_vector(T, sr, np_lin, False)
    return initial_gap*t_ratio**(1/k) + final_position, t

def generate_tau_velocity(k, initial_gap, T, sr=1, np_lin=False):
    """ Returns the position velocity and acceleration of a Tau-guided movement """

    t_ratio, t = make_time_vector(T, sr, np_lin, False)
    return abs(2*initial_gap*t/(k*T**2)*(t_ratio)**(1/k-1)), t

def generate_tau_acceleration(k, initial_gap, T, sr=1, np_lin=False):
    """ Returns the position velocity and acceleration of a Tau-guided movement """

    t_ratio, t = make_time_vector(T, sr, np_lin, True)
    return ((4*initial_gap*t**2*(t_ratio)**(1/k - 2)*(1/k - 1)/(k*T**4) -
         2*initial_gap*(t_ratio)**(1/k - 1)/(k*T**2))), t

def make_time_vector(T, sr, np_lin, change_last_point):
    N = int(T*sr) + 1
    if np_lin:
        t = np.linspace(0, T, N)
    else:
        t = linspace(0, T, N)
    t_ratio = (1-(np.square(t).astype('f')/np.square(T).astype('f')))
    if change_last_point:
        t_ratio[t_ratio <= np.finfo(float).eps] = np.finfo(float).eps
    return t_ratio, t

def update_duration(vocal_tract, x, end_mov=True):
    """ Updates duration values in vt """

    if vocal_tract.optimization.duration != "fixed":
        nb_art, nb_gests, nb_param = extract_parameters(vocal_tract,
                                                        end_mov=end_mov)
        for n in range(nb_gests):
            if vocal_tract.optimization.duration == "local":
                durations = x[(n+1)*nb_param-nb_art:(n+1)*nb_param]
            elif vocal_tract.optimization.duration == "global":
                durations = [x[(n+1)*nb_param-1]]*nb_art
            if n == 0:
                start_points = [0]*nb_art

            end_points = [s + d for s, d in zip(start_points, durations)]
            for m in range(nb_art):
                vocal_tract.articulators[m].movements[n].time_points = (start_points[m],
                                                                        end_points[m])
            start_points = [e for e in end_points]
        if end_mov:
            n = nb_gests
            end_point = max(start_points) + 0.05
            for m in range(nb_art):
                vocal_tract.articulators[m].movements[n].time_points = (start_points[m],
                                                                        end_point)

def update_kappa(vocal_tract, x, end_mov=True):
    """ Updates kappa values in vt """

    if vocal_tract.optimization.kappa != "fixed":
        nb_art, nb_gests, nb_param = extract_parameters(vocal_tract,
                                                        end_mov=end_mov)
        for n in range(nb_gests):
            if vocal_tract.optimization.kappa == "local":
                kappas = x[n*nb_param:n*nb_param+nb_art]
            else:
                kappas = [x[n*nb_param]]*nb_art
            for m in range(nb_art):
                vocal_tract.articulators[m].movements[n].kappa = kappas[m]

def update_position(vocal_tract, x, end_mov=True):
    """ Updates position values in vt """

    if vocal_tract.optimization.position != "fixed":
        nb_art, nb_gests, nb_param = extract_parameters(vocal_tract,
                                                        end_mov=end_mov)
        
        for n in range(nb_gests):
            if vocal_tract.optimization.kappa == "local":
                end_pos = x[n*nb_param+nb_art:n*nb_param+2*nb_art]
            elif vocal_tract.optimization.kappa == "fixed":
                end_pos = x[n*nb_param:n*nb_param+nb_art]
            elif vocal_tract.optimization.kappa == "global":
                end_pos = x[n*nb_param+1:n*nb_param+nb_art+1]
            if n == 0:
                start_pos = [0]*nb_art
            for m in range(nb_art):
                vocal_tract.articulators[m].movements[n].position = (start_pos[m],
                                                                    end_pos[m])
            start_pos = [e for e in end_pos]
        if end_mov:
            n = nb_gests
            for m in range(nb_art):
                vocal_tract.articulators[m].movements[-1].position = (start_pos[m],
                                                                      0)
                
def update_pressure(vocal_tract, x, end_mov=True):
    """ Updates subglottal pressure values in vt """

    nb_art, nb_gests, nb_param = extract_parameters(vocal_tract,
                                                    end_mov=end_mov)
    nb_sup_param = nb_param * nb_gests
    
    for n in range(vocal_tract.number_of_syllables):
        end_pos, duration = x[(nb_sup_param + 2*n):(nb_sup_param + 2*(n+1))]
        vocal_tract.subglottal_pressure.movements[n].position        
        if n == 0:
            start_pos, start_point = 0, 0
        end_point = start_point + duration
        vocal_tract.subglottal_pressure.movements[n].position = (start_pos,
                                                                end_pos)
        vocal_tract.subglottal_pressure.movements[n].time_points = (start_point,
                                                                end_point)
        start_pos, start_point = end_pos, end_point
    if end_mov:
        n = vocal_tract.number_of_syllables
        vocal_tract.subglottal_pressure.movements[-1].position = (start_pos,
                                                                  0)
        

def update_vt_tau(vocal_tract, x, end_mov=True):
    """ Updates the vocal tract object given solution x """

    update_kappa(vocal_tract, x, end_mov)
    update_position(vocal_tract, x, end_mov)
    update_duration(vocal_tract, x, end_mov)
    if vocal_tract.optimization.pressure:
        update_pressure(vocal_tract, x, end_mov)
    vocal_tract.solution = x

def vt_to_solution_tau(vocal_tract, end_mov=True):
    """ Returns the solution vector from vocal tract object """

    nb_art, nb_gests, nb_param = extract_parameters(vocal_tract,
                                                    end_mov=end_mov)
    x = []
    for ng in range(nb_gests):
            xtmp = (extract_kappa_solution(vocal_tract, ng) +
                    extract_position_solution(vocal_tract, ng) +
                    extract_duration_solution(vocal_tract, ng))
            x += xtmp
    if vocal_tract.optimization.pressure:
        x += extract_pressure_solution(vocal_tract)
        start_point = vocal_tract.subglottal_pressure.movements[-1].time_points[0]
        end_point = vocal_tract.articulators[0].movements[-1].time_points[-1]
        vocal_tract.subglottal_pressure.movements[-1].time_points = (start_point,
                                                                  end_point)
        
    vocal_tract.solution = x

    return x

