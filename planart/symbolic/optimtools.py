#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Sep  2 10:46:28 2022

@author: benjamin
"""

import numpy as np
from scipy.optimize import minimize


def cost_function(syllables, words, weights):
    """ Computes the overall cost function as in Eq. (1) """
    P = parsing_cost(parsing_syllable_cost(syllables),
                     parsing_word_cost(words))
    D = duration_cost(syllables)
    E = effort_cost(syllables)
    costs = (E, P, D)
    return np.sum([c*w for c, w in zip(costs, weights)])

def duration_cost(syllables):
    """ Computes the duration cost as in Eq. (5) """
    durations, weights = get_attributes(syllables,
                                        ["duration", "duration_weight"])
    return np.sum([d*w for d, w in zip(durations, weights)])

def effort_cost(syllables):
    """ Computes the effort as in Eq. (2) """
    durations, weights = get_attributes(syllables,
                                        ["duration", "effort_weight"])
    return np.sum([w*np.sqrt(d) for w, d in zip(weights, durations)])

def get_attributes(constituents, keys):
    return [[c[key] for c in constituents] for key in keys]

def objective(x, *args):
    """ Objective function """

    syllables, words, weights = args
    syllables = x_to_sol(x, syllables)
    words = update_word_duration(words, x)
    return cost_function(syllables, words, weights)

def optimization(syllable_parsing_weights, syllable_effort_weights,
                 word_parsing_weights, word_parsing_strengths, 
                 global_weights, word_indices, init_duration=0.25, 
                 randomize=False):
    """ Prosodic structure optimization """
      
    nb_syll = len(syllable_parsing_weights)
    nb_words = len(word_parsing_weights)
    if not randomize and init_duration is not None:
        x0 = [init_duration]*nb_syll
    elif init_duration is not None:
        x0 = init_duration + 0.02*np.random.randn(nb_syll)
    else:
        x0 = 0.25 + 0.02*np.random.randn(nb_syll)
    bounds = ((((0, None),)*len(x0)))  
    syllables = [{"duration": init_duration, 
                  "parsing_weight": syllable_parsing_weights[i],
                  "duration_weight": 1,
                  "effort_weight": syllable_effort_weights[i]} 
                 for i in range(nb_syll)]

    words = [{"duration": init_duration,
              "parsing_weight": word_parsing_weights[i],
              "parsing_strength": word_parsing_strengths[i],
              "indices": word_indices[i]}
             for i in range(nb_words)] # Dummy variable
    words = update_word_duration(words, x0)    
    args = (syllables, words, global_weights)
    return minimize(objective, x0, args=args, 
                       method="nelder-mead", bounds=bounds)["x"]

def parsing_cost(parsing_syllable, parsing_word):
    """ Computes the overall perception """
    return parsing_syllable + parsing_word

def parsing_syllable_cost(syllables):
    """ Computes the syllable parsing as in Eq. (3) """
    durations, weights = get_attributes(syllables,
                                        ["duration", "parsing_weight"])
    return np.sum([np.exp(-w*d) for w, d in zip(weights, durations)])

def parsing_word_cost(words):
    """ Computes the word parsing as in Eq. (4) """
    durations, weights, strengths = get_attributes(words,
                                        ["duration", "parsing_weight",
                                         "parsing_strength"])
    return np.sum([a*np.exp(-w*d) for a, w, d in zip(strengths, weights, durations)])

def update_word_duration(words, x):
    """ Updates the word duration """
    for w  in words:        
        w["duration"] = np.sum([x[i] for i in w["indices"]])
    return words

def x_to_sol(x, syllables):
    """ Update temporary solution in syllables structure """
    for n in range(len(x)):
        syllables[n]["duration"] = x[n]
    return syllables

