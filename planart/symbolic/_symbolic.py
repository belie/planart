#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 24 09:09:11 2022

@author: benjamin
"""

def compute_cues(phoneme):
    """ Returns the landmarks """
    return {"label" : phoneme,
            "type" : define_phoneme_type(phoneme),
            "landmarks" : define_landmark(define_phoneme_type(phoneme))}


def compute_weights(constituent, inputs, beta=1, change_effort=False):
    """ Computes the weights for the cost function assigned to one constituent """
    prominence = constituent["prominence"]
    weights = init_weights()
    if constituent["type"] == "sentence":
        weights["duration"] = get_duration_weight(inputs)
    if constituent["type"] in ["word", "syllable", "phoneme"]:
        weights["parsing duration"] = get_parsing_duration_cost(prominence,
                                                                beta=beta)
    if constituent["type"] == "phoneme":
        prominence = get_phoneme_prominence(constituent, inputs["constituents"])
        if change_effort:
            weights["effort"] = get_effort_cost(prominence, beta=beta)
        else:
            weights["effort"] = 1
        weights["parsing precision"] = get_parsing_precision_cost(prominence,
                                                                  beta=beta)
        weights["parsing duration"] = get_parsing_duration_cost(prominence,
                                                                  beta=beta)

    return weights

def create_output_cues_dict(constituents):
    """ Creates the output dictionary containing the acoustic cues """

    phonemes = extract_phonemes(constituents)
    return {"constituents" : [compute_cues(lbl) for lbl in phonemes]}

def create_output_weight_dict(json_dict, beta=1, change_effort=False):
    """ Creates the output dictionary containing the weights of the cost function """

    output_weights = dict()
    output_weights["inputs"] = json_dict["inputs"]
    constituents = output_weights["inputs"]["constituents"]
    output_weights["inputs"]["constituents"] = []
    for c in constituents:
        curr_c = c
        curr_c["weights"] = compute_weights(c, json_dict["inputs"], beta=beta, 
                                            change_effort=change_effort)
        output_weights["inputs"]["constituents"].append(curr_c)

    return output_weights

def define_landmark(phoneme_type):
    """ Returns the landmark field """
    if phoneme_type in ["vowel", "approximant", "glide"]:
        return ["proba_max"]
    if phoneme_type == "diphtong":
        return ["proba_max"]*2
    if phoneme_type == "triphtong":
        return ["proba_max"]*3
    if "stop" in phoneme_type:
        return ["closure", "release", "max_proba"]
    if "fricative" in phoneme_type:
        return ["proba_max"]

def define_phoneme_type(phoneme):
    """ Returns the pĥoneme type """
    if phoneme in ["a", "i", "o", "u", "e", "@", "3", "aa", "ae", "ah", "ao", 
                   "ax", "axr", "eh", "er", "ih", "ix", "iy", "uh", "uw", "ux"]:
        return "vowel"
    if phoneme in ["aI", "oU", "aU", "eI"]:
        return "diphthong"
    if phoneme in ["f", "T", "s", "S", "h"]:
        return "voiceless_fricative"
    if phoneme in ["v", "D", "z", "Z"]:
        return "voiced_fricative"
    if phoneme in ["p", "t", "k"]:
        return "voiceless_stop"
    if phoneme in ["b", "d", "g"]:
        return "voiced_stop"
    if phoneme in ["m", "n", "N"]:
        return "nasal_stop"
    if phoneme in ["j", "w"]:
        return "glide"
    if phoneme in ["r", "l"]:
        return "approximant"

def extract_phonemes(constituents):
    """ Returns the list of phonemes """

    phonemes = [x for x in constituents if x["type"] == "phoneme"]
    return [x["label"] for x in phonemes]

def find_constituent(constituent_id, constituents):
    """ Returns the constituent from its ID """
    constituent = [c for c in constituents if c["constituent_id"] == constituent_id]
    if len(constituent) == 0:
        raise ValueError("No constituent found for ID:", constituent_id)
    if len(constituent) > 1:
        raise ValueError("Several constituents share the same ID:", constituent_id)
    return constituent[0]

def get_duration_weight(inputs):
    """ Returns the total duration weight """
    return (inputs["relative_speech_rate"] *
            inputs["speech_style"]["relative speech rate importance"])

def get_effort_cost(prominence, beta=1):
    """ Returns the effort cost """
    return 1 /(prominence ** beta)
 
def get_parsing_duration_cost(prominence, beta=1):
    """ Returns the parsing duration cost """
    return (prominence ** beta)

def get_parsing_precision_cost(prominence,  beta=1):
    """ Returns the parsing precision cost """
    return (prominence ** beta)

def get_phoneme_prominence(phoneme, constituents):
    """ Returns the phoneme prominence weighted by
    its syllable and word prominences """
   
    parent_syllable = find_constituent(phoneme["parent"], constituents)
    parent_word = find_constituent(parent_syllable["parent"], constituents)
    return (phoneme["prominence"] * 
            parent_syllable["prominence"] * 
            parent_word["prominence"])    

def prosodic_weight(inputs):
    """ Returns the prososic structure weight """
    return inputs["speech_style"]["prosodic structure importance"]

def init_weights():
    """ Create a dictionary of weights """
    return {"duration" : 0,
               "effort" : 0,
               "parsing precision" : 0,
               "parsing duration" : 0,
               "prosody": 0}
