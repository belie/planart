#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 30 07:53:09 2021

@author: benjamin
"""

import abc
from configparser import ConfigParser, SectionProxy
from pathlib import Path
from typing import (  # noqa: F401
    List,
    Tuple,
    Union,
)

import h5py
import os
from . import (_io)

def optim_script_path(basepath: Path) -> Path:
    script_path = Path(basepath)
    if not script_path.exists():
        raise OSError('{} not found'.format(script_path))
    return script_path

def script_path(global_config: ConfigParser, module_name: str) -> Path:
    method = global_config[module_name]['method']
    basepath = Path(global_config['scripts'][method])
    return optim_script_path(basepath)

class _Runner(metaclass=abc.ABCMeta):
    """Abstract base class for python executable runner."""

    @abc.abstractproperty
    def module_name(self) -> str:
        """Name of python executable."""

    @abc.abstractmethod
    def inputs(self) -> List[Path]:
        """Sequence input arguments for the python executable."""

    def __init__(self, global_config: ConfigParser,
                 settings: SectionProxy) -> None:
        self.config = global_config
        self.settings = settings

    def _input_for(self, option: str) -> Path:
        fpath = self.settings[option]
        return Path(fpath).absolute()

    def run_exe(self) -> Tuple[str, ...]:
        script = script_path(self.config, self.module_name)
        cmd_line = script.as_posix() + " "
        args = []
        for fpath in self.inputs():
            if not isinstance(fpath, str):
                if not fpath.exists():
                    raise OSError('input file {} not found'.format(fpath))
                args.append(fpath.as_posix())
            else:
                args.append(fpath)
        
        cmd_line += " ".join(args)
        print("Preparing to launch ", script.as_posix(), flush=True)
        cmd_line = "python " + cmd_line
        os.system(cmd_line)
        
        return print("Job's done")

    def run_module(self) -> h5py.File:
        output_files = self.run_exe()
        h5files = [fname for fname in output_files if fname.endswith('.h5')]
        assert len(h5files) == 1, 'expecting exactly one .h5 output file'
        return h5py.File(h5files[0])

class SymbolicRunner(_Runner):
    module_name = 'symbolic'
    def inputs(self) -> List[Path]:
        return [
            self._input_for(option)
            for option in (
                'utterance',
                'parameters',
            )
        ]

class DynamicRunner(_Runner):
    module_name = 'dynamic'
    def inputs(self) -> List[Path]:
        return [
            self._input_for(option)
            for option in (
                'parameters',
                'phonemes'
            )
        ]

class OptimizationRunner(_Runner):
    module_name = 'optimization'
    def inputs(self) -> List[Path]:
        return ([self.settings["method"]] + [
            self._input_for(option)
            for option in (                
                'parameters',
                'dictionary',
                'probabilistic_model',
                'intensity_model',
            )] + 
            [Path(_io.OUTPUTS['symbolic'][i]).absolute() for i in range(2)]
        )
    
class AcousticsRunner(_Runner):
    module_name = 'acoustics'
    def inputs(self) -> List[Path]:
        return ([
            self._input_for(option)
            for option in (                
                'parameters',
            )] + 
            [Path(_io.OUTPUTS['optimization'][0]).absolute()]
        )

class MSRunner(_Runner):
    module_name = 'motor-sensory'
    def inputs(self) -> List[Path]:
        return ([
            self._input_for(option)
            for option in (                
                'parameters',
            )] + 
            [Path(_io.OUTPUTS['optimization'][0]).absolute()]
        )