#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 20 16:38:06 2022

@author: benjamin
"""

import numpy as np

def extract_types(constituents):
    """ Returns names of all different types of constituents """
    types = np.unique([c["type"] for c in constituents])
    constituent_types = []
    for const_type in types:
        type_list = [c for c in constituents if c["type"] == const_type]
        constituent_types.append(type_list)

    return constituent_types

def find_children(x, constituents):
    """ Returns the children of a constituent """
    x_id = x["constituent_id"]
    const_parents = list_parents(constituents)
    idx = [i for i in range(len(constituents)) if const_parents[i] == x_id]
    if len(idx) > 0:
        return [constituents[i] for i in idx], idx
    else:
        return None, None

def find_first(siblings):
    """ Returns the index of the first constituent among siblings """
    return [i for i in range(len(siblings)) if
            "previous_sibling" not in siblings[i].keys()][0]

def find_next_sibling(constituent, siblings):
    """ Returns the index of the next constituent among siblings """
    const_id = constituent["constituent_id"]
    for idx, s1 in enumerate(siblings):
        if "previous_sibling" in s1.keys():
            if s1["previous_sibling"] == const_id:
                return idx

def find_parent(x, constituents):
    """ Returns the parent of a constituent """
    if "parent" in x.keys():
        const_ids = [c["constituent_id"] for c in constituents]
        parent_id = x["parent"]
        idx = [i for i in range(len(constituents)) if const_ids[i] == parent_id]
        if len(idx) > 0:
            return constituents[idx[0]], idx[0]
        else:
            return None, None
    else:
        return  None, None

def get_sibling_position(siblings, position):
    """ Returns whether the sibling is next r previous """
    return [s["sibling_id"] for s in siblings if s["position"] == position]

def find_constituent(x, constituents):
    """ Find indices of constituent x among constituents """
    idx = [i for i in range(len(constituents)) if
           constituents[i]["constituent_id"] == x["constituent_id"]]
    if len(idx) > 0:
        return idx[0]
    else:
        raise ValueError("There is no such constituent")

def find_siblings(x, constituents):
    """ Returns the siblings of a constituent """
    parent, idx = find_parent(x, constituents)
    if parent is not None:
        const_parents = list_parents(constituents)
        parent_id = parent["constituent_id"]
        idx = [i for i in range(len(constituents)) if const_parents[i] == parent_id]
        if len(idx) > 0:
            return [constituents[i] for i in idx], idx
        else:
            return None, None
    else:
        return None, None

def list_parents(constituents):
    """ List the parents of all constituents """
    parents = []
    for constituent in constituents:
        if "parent" in constituent.keys():
            parents.append(constituent["parent"])
        else:
            parents.append(None)
    return parents

def sort_all_constituents(constituents):
    """ Sort constituents by tiers and among tiers, from the root node
    (constituent without parent) to the end (constituents with no children) """

    constituent_types = extract_types(constituents)
    constituent_sorted = sort_type(constituent_types)    

    level_constituent = constituent_sorted[0]
    level_labels = " ".join([r["label"] for r in level_constituent])
    
    all_sorted = [level_constituent]
    for n in range(1, len(constituent_sorted)):
        level_constituent, level_labels = sort_constituents(level_constituent, constituents)
        all_sorted.append(level_constituent)

    types = [c[0]["type"] for c in all_sorted]
    return all_sorted, types

def sort_constituents(parent_type, constituent_list):
    """ Sort constituents by tiers, from the root node (constituent without parent)
    to the end (constituents with no children) """

    constituents = []
    constituents_id = []
    constituents_label = []

    for parent in parent_type:
        children, _ = find_children(parent, constituent_list)
        if len(children) > 1:
            for child in children:
                if child["constituent_id"] not in constituents_id:
                    idx_first = [i for i in range(len(children)) if "previous_sibling" not in children[i].keys()][0]
                    siblings = [children[idx_first]]
                    while len(siblings) < len(children):
                        idx = find_next_sibling(siblings[-1], children)
                        if idx is not None:
                            siblings.append(children[idx])

                    [constituents.append(w) for w in siblings]
                    [constituents_label.append(w["label"]) for w in siblings]
                    [constituents_id.append(w["constituent_id"]) for w in siblings]
        else:
            constituents.append(children[0])
            constituents_label.append(children[0]["label"])
            constituents_id.append(children[0]["constituent_id"])

    return constituents, " ".join(constituents_label)

def sort_type(constituent_types):
    """ Sort constituent among a tiers """

    nb_types = len(constituent_types)
    for c in constituent_types:
        keys = [x.keys() for x in c]
        if all(["parent" not in key for key in keys]):
            constituent_sorted = [c]
    [constituent_types.remove(c) for c in constituent_sorted]
    while len(constituent_sorted) < nb_types:
        ids = [c["constituent_id"] for c in constituent_sorted[-1]]
        for c1 in constituent_types:
            parents = [c2["parent"] for c2 in c1]
            if all([p in ids for p in parents]):
                constituent_sorted.append(c1)
                constituent_types.remove(c1)

    return constituent_sorted
