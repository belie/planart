#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov  1 09:00:03 2021

@author: benjamin
"""

from collections import OrderedDict

import click

from ._config import (
    GlobalConfig,
    initialize_global_config,
    read_config,
    initialize_settings,
    default_constituents,
    create_utterance_file,
)
from . import (
    _runner
)

from .utils import change_parameters

@click.version_option()
@click.group()
def cli_cmd():
    pass

@cli_cmd.command(
    name='init-config',
    help='initialize global configuration',
)
@click.option(
    '--force', '-f', is_flag=True, default=False,
    help='force overridding of an already existing configuration file',
)
def init_config(force):
    config_fpath = GlobalConfig.file_path()
    if config_fpath.exists() and not force:
        raise click.ClickException(
            'file {} already exists, use --force to override'.format(
                config_fpath)
        )
    initialize_global_config(config_fpath,
                             GlobalConfig.json_path(),
                             GlobalConfig.etd_path(),
                             GlobalConfig.maeda_path(),
                             GlobalConfig.planart_acoustic_models_path())
    
    click.echo(
        "configuration initiliazed at {}; "
        "now edit this file to complete the process".format(
            config_fpath)
    )


@cli_cmd.command(
    'new',
    help='initialize a project from a "settings" file',
)

@click.argument(
    'settings', type=click.Path(exists=False),
)
def init_settings(settings):
    initialize_settings(settings)

_modules = OrderedDict()

def _register(name):
    def decorator(func):
        _modules[name] = func
        return func
    return decorator

@_register("symbolic")
def cmd_symbolic(config, settings):
    _runner.SymbolicRunner(config, settings["symbolic"]).run_exe()

@_register("dynamic")
def cmd_dynamics(config, settings):    
    method = config['dynamic']['method']
    if method == 'optimization':
       print("The dynamic module will run the " + method +
             " method")
       _runner.OptimizationRunner(config, settings["optimization"]).run_exe()
    else:
        raise ValueError('Method ' + method + 
                         ' is not supported by the dynamic module')

@_register("optimization")
def cmd_optimization(config, settings):
    _runner.OptimizationRunner(config, settings["optimization"]).run_exe()

@_register("motor-sensory")
def cmd_motorsensory(config, settings):
    method = config['motor-sensory']['method']    
    if method == 'acoustics':
        print("Motor-sensory implementation module will run the " + method +
              " method")
        _runner.AcousticsRunner(config, settings["acoustics"]).run_exe()
    else:
        raise ValueError('Method ' + method + 
                         ' is not supported by the motor-sensory module')

@_register("acoustics")
def cmd_acoustics(config, settings):
    _runner.AcousticsRunner(config, settings["acoustics"]).run_exe()

@cli_cmd.command(
    name='run',
    help='run one or all module(s)',
)
@click.argument(
    'settings', type=click.Path(exists=True),
)
@click.option(
    '--module', '-m',
    type=click.Choice(list(_modules)),
)
@click.option(
    '--from-module', '-f',
    type=click.Choice(list(_modules)),
    help='run simulation from a given module',
)
def run(settings, module, from_module):
    config = GlobalConfig()
    config.load()
    settings = read_config(settings)
    if module is not None:
        if from_module is not None:
            raise click.BadParameter(
                "--module and --from-module option are mutually exclusive"
            )
        _modules[module](config, settings)
    else:
        for modl, cmd in _modules.items():      
            if from_module is not None:
                if modl != from_module:
                    continue
                from_module = None         
            if modl not in ['optimization', 'acoustics']:      
                click.echo('\n* running module {}'.format(modl))                
                cmd(config, settings)


@cli_cmd.command(
    name='init-utterance',
    help='create an utterance file',
)
@click.argument(
    'filename', type=click.Path(exists=False),
    nargs=1,
)
@click.option(
    '--sentence', '-utt',
    help='initialize sentence',
    type=str,
    multiple=True,
)
@click.option(
    '--words', '-w',
    help='initialize words',
    type=str,
    multiple=True,
)
@click.option(
    '--syllables', '-syl',
    help='initialize syllables',
    type=str,
    multiple=True,
)
@click.option(
    '--phonemes', '-p',
    help='initialize phonemes',
    type=str,
    multiple=True,    
)
@click.option(
    '--speech-rate', '-sr',
    help='relative speech rate',
    type=float,
    default=1,
)
@click.option(
    '--speech-style', '-sty',
    help='speech style',
    type=str,
)
@click.option(
    '--default', '-d', is_flag=True, default=False,
    help='creates an example utterance for test',
)
@click.option(
    '--vowel', '-v',
    help='for simulating just one vowel',
    type=str,
)
def init_utterance(filename, sentence, words, syllables, phonemes,
                   speech_rate, speech_style, default, vowel):
    
    if not default and (sentence is None or words is None or 
                    syllables is None or phonemes is None):
        raise click.ClickException('missing required option')
    
    if default and vowel is None:
        sentence, words, syllables, phonemes = default_constituents()
    if vowel is not None:
        sentence, words, syllables, phonemes = default_constituents(vowel)
        
    create_utterance_file(filename, sentence, words, syllables, phonemes,
                       speech_rate, speech_style)
    
@cli_cmd.command(
    name='param-modif',
    help='modify parameters in the globalparameters.json file',
)
@click.argument(
    'filename', type=click.Path(exists=False),
    nargs=1,
)
@click.option(
    '--parameter', '-p',
    help='initialize sentence',
    type=str,
    multiple=True,
)
def param_modif(filename, parameter):
    param_file = read_config(filename)['optimization']['parameters']
    for p in parameter:
        level, settings = (p.split(":")[0], 
                           p.split("(")[-1].replace(')', '').split(','))
        keys = [s.split("=")[0] for s in settings]
        values = [s.split("=")[1] for s in settings]
        change_parameters(param_file, keys, values, level)
        
    