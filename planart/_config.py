import configparser
from pathlib import Path
from typing import Any
import json
from .models._etd import write_etd_dictionary
from .models._maeda import write_maeda_dictionary
import os
import shutil
from importlib.resources import files
from importlib.metadata import version

def get_version():
    return version('planart')

def choose_home():
    from os import environ
    if os.name == "nt":
        return Path(environ['USERPROFILE'])
    else:
        return Path(environ['HOME'])

def get_planart_path():
    return files("planart").parent

class GlobalConfig(configparser.ConfigParser):

    def __init__(self, *args: Any, **kwargs: Any) -> None:
        kwargs['strict'] = True
        super().__init__(*args, **kwargs)

    @classmethod
    def file_path(cls) -> Path:
        planart_home = os.path.join(choose_home(), '.planart_v' + get_version())
        return Path(os.path.join(planart_home, 'config.ini'))

    @classmethod
    def json_path(cls) -> Path:
        planart_home = os.path.join(choose_home(), '.planart_v' + get_version())
        return Path(os.path.join(planart_home, 'globalparameters.json'))

    @classmethod
    def etd_path(cls) -> Path:
        planart_home = os.path.join(choose_home(), '.planart_v' + get_version())
        return Path(os.path.join(planart_home, 'etd.json'))
    
    @classmethod
    def maeda_path(cls) -> Path:
        planart_home = os.path.join(choose_home(), '.planart_v' + get_version())
        return Path(os.path.join(planart_home, 'maeda.json'))

    @classmethod
    def acoustic_models_path(cls) -> Path:
        planart_home = os.path.join(choose_home(), '.planart_v' + get_version())
        return Path(os.path.join(planart_home, 'models'))

    @classmethod
    def planart_acoustic_models_path(cls) -> Path:
        return Path(os.path.join(get_planart_path(), 'models'))
 
    def load(self) -> None:
        """Load options from user configuration file, failing if it does not
        exist.
        """
        with self.file_path().open() as f:
            self.read_file(f)

def copy_models(modelpath: Path) -> None:
    """ Create model directory and copy default models """
    planart_home = os.path.join(choose_home(), '.planart_v' + get_version())
    new_path = os.path.join(planart_home, 'models')
    if os.path.isdir(new_path):
        shutil.rmtree(new_path)
    shutil.copytree(modelpath, new_path)    

def write_config_ini(fpath: Path) -> None:
    """ Writes config file """

    config = configparser.ConfigParser(strict=True)

    config['symbolic'] = {
        'method': 'dictionary'
    }
    config['dynamic'] = {
        'method': 'optimization'        
    }
    config['optimization'] = {
        'method': 'optimization',
        'model': 'english_alpha_0_layers_100_50_40_200_25_batch_size_1024.atp',
        'intensity': 'intensity_000'
        }
    config['motor-sensory'] = {
        'method': 'acoustics'
    }
    config['acoustics'] = {
        'method': 'acoustics'
        }
    config['scripts'] = {
        'optimization': os.path.join(get_planart_path(), 'scripts',
                                  'launch_optimization.py'),
        'etd': os.path.join(get_planart_path(), 'scripts',
                         'launch_etd.py'),
        'tau': os.path.join(get_planart_path(), 'scripts',
                         'launch_tau.py'),
        'dictionary': os.path.join(get_planart_path(), 'scripts',
                                'launch_symbolic.py'),
        'acoustics': os.path.join(get_planart_path(), 'scripts',
                               'launch_acoustics.py')
    }
    config_dir = fpath.parent
    if not config_dir.exists():
        config_dir.mkdir(parents=True)
    with fpath.open('w') as f:
        config.write(f)

def write_parameters(jsonpath: Path) -> None:
    """ Writes parameter file """
    config_json = {
        'timing': {
        "prominence_power": 1,
        "parsing_model": "arctan",
        "arctan_timing_coefficient": 500,
        "initial_duration": 0.25,
        "randomize_timing": 0
            },
        'optimization': {
        "repetitions": 1,
        "attempts": 3,
        "max_iterations": 200,
        "method": "nelder-mead",
        "early_stop": 0,
        "catch_solution": 1,
        "parallel": "full",
        "verbosity": 1,
        "convergence_threshold": 0,
	    "oversampling": 10,
        "debug": 0,
        "duration_model": "hyperbolic",
        "reward_discounting_rate": 1,
        "effort_model": "full_gradient"
            },
        'etd': {
        "sampling_rate": 100,
        "realization_threshold": 0.8,
        "closure_threshold": 0.02,
        "consonant_timing_factor": 500,
        "vowel_timing_factor": 250,
        "initial_stiffness": "50",
        "solver": "ode"
            },
        'tau':{
        "sampling_rate": 100,
        "realization_threshold": 0.8,
        "closure_threshold": 0.02,
        "consonant_timing_factor": 500,   
        "vowel_timing_factor": 250,
        "consonant_parsing_weight": 10,
        "initial_kappa": "0.453",
        "initial_position": "dictionary",
        "intelligibility_model": "max",
        "intelligibility_threshold_type": "relative",
        "intelligibility_threshold": 2/3,
        "kappa_opt": "global",
        "position_opt": "local",
        "duration_opt": "global",     
        "pressure_opt": False,
        "kappa_bounds": [0, 1]
            },
        'intensity': {
        "beta": 0.1873,
        "gamma": 9.0783,
        "distance": 0.01,
        "background_noise": 0,
        "intensity": True,
        "fundamental_frequency": 100,
        "subglottal_pressure": 500
            },
        'acoustics': {
        "simulation_rate": 20000,
        "output_sampling_rate": 12000,
        "fundamental_frequency": 100,
        "subglottal_pressure": 500
            }
        }
    with open(jsonpath.as_posix(), 'w') as f:
        json.dump(config_json, f, indent=4)

def initialize_global_config(fpath: Path, jsonpath: Path, etdpath: Path,
                             maedapath: Path, modelpath: Path) -> None:
    """ Writes all config files """
    write_config_ini(fpath)
    write_parameters(jsonpath)
    write_etd_dictionary(etdpath)
    write_maeda_dictionary(maedapath)
    copy_models(modelpath)

def read_config(fpath: str) -> configparser.ConfigParser:
    """Parse `fpath` as a .INI file and return a ConfigParser instance."""
    config = configparser.ConfigParser(strict=True)
    with open(fpath) as f:
        config.read_file(f)
    return config

def read_json_config(fpath: str) -> dict:
    """Parse `fpath` as a .json file and return a dict instance."""
    with Path(fpath).open() as f:
        return json.load(f)

def initialize_settings(path: str) -> None:
    fpath = Path(path).absolute()
    projectdir = fpath.parent
    ini_file = GlobalConfig().json_path()
    
    def filepath(name: str) -> str:
        return Path(os.path.join(projectdir, name)).as_posix()
    if os.path.isfile(ini_file):
        shutil.copyfile(ini_file, filepath('globalparameters.json'))    
    if os.path.isfile(GlobalConfig().file_path()):
        model = read_config(GlobalConfig().file_path())['optimization']['model']
        if os.path.splitext(model)[-1] == '.atp':
            model_path = os.path.join(GlobalConfig().acoustic_models_path(), 
                                      'probabilistic', 'atp', model)
        elif os.path.splitext(model)[-1] == '.ftp':
            model_path = os.path.join(GlobalConfig().acoustic_models_path(), 
                                      'probabilistic', 'ftp', model)
        else:
            raise ValueError('File format of the probabilistic model file (' +
                             os.path.splitext(model)[-1] + ') not recognized')
        if not os.path.isfile(model_path):
            raise ValueError('Probabilistic model file (' + model_path + 
                             ') does not exist')
        shutil.copyfile(model_path, filepath(model))   
        intensity_model = read_config(GlobalConfig().file_path())['optimization']['intensity']
        model_path = os.path.join(GlobalConfig().acoustic_models_path(), 
                                  'intensity', intensity_model)
        shutil.copytree(model_path, filepath(intensity_model))

    config = configparser.ConfigParser(strict=True)
    config['symbolic'] = {
        'utterance': filepath('utterance.json'),
        'parameters': filepath('globalparameters.json'),
    }
    config['dynamic'] = {
        'method': 'optimization'
    }
    config['optimization'] = {
        'method': 'xt/3c',
        'dictionary': filepath('gesture_dictionary.json'),
        'probabilistic_model': filepath('english_alpha_0_layers_100_50_40_200_25_batch_size_1024.atp'),
        'parameters': filepath('globalparameters.json'),
        'intensity_model': filepath('intensity_000'),
    }
    config['motor-sensory'] = {
        'method': 'acoustics'
    }
    config['acoustics'] = {
        'parameters': filepath('globalparameters.json'),
    }
    with fpath.open('w') as f:
        config.write(f)
        
    sentence, words, syllables, phonemes = default_constituents()
    
    create_utterance_file(filepath('utterance.json'), sentence, words,
                          syllables, phonemes,
                       0.5, None)
    """ Creates a dummy gesture_dictionary.json file. 
    Should be rmeoved in the future """    
    with open(filepath('gesture_dictionary.json'), 'w') as f:
        json.dump({}, f, indent=4)       

def default_constituents(vowel='aa'):
    return ([vowel + ':(prom=1,id=s1)'], 
            [vowel + ':(prom=1,par=s1,id=w1)'], 
            [vowel + ':(prom=2,par=w1,id=syl1)'],
            [vowel + ':(prom=1,par=syl1,id=p1)'])


def create_utterance_file(filename, sentence, words, syllables, phonemes,
                   speech_rate, speech_style):
    
    utterance_dictionary = {"inputs": {"constituents": [],
                                       "relative_speech_rate": float(speech_rate),
                                       "speech_style": get_speech_style(speech_style)}}    
    for keys, ctype in zip([sentence, words, syllables, phonemes],
                          ["sentence", "word", "syllable", "phoneme"]):
        utterance_dictionary["inputs"]["constituents"] += [get_parameters_from_input(key, ctype, n) for 
                        n, key in enumerate(keys)]
    
    with open(filename, 'w') as f:
        json.dump(utterance_dictionary, f, indent=4)        

def get_parameters_from_input(constituent, constituent_type, nb):

    label, settings = (constituent.split(":")[0], 
                       constituent.split("(")[-1].replace(')', '').split(','))
    constituent_dictionary = {'type': constituent_type,
                              'label': label,
                              'constituent_id': "_".join([constituent_type,
                                                          str(nb)]),
                              'prominence': 1,
                              }
    for s in settings:
        key, value = s.split('=')
        if key == "prom":
            constituent_dictionary['prominence'] = float(value)
        elif key == 'id':
            constituent_dictionary['constituent_id'] = value
        elif key == 'par':
            constituent_dictionary['parent'] = value
        elif key == 'sib':
            constituent_dictionary['previous_sibling'] = value
        else:
            raise ValueError('key ' + key + ' is not recognized')            
    
    return constituent_dictionary

def get_speech_style(style=None):
    speech_style = {
  	  "lexical contrast importance": 10.0,
  	  "relative speech rate importance": 10.0,
  	  "prosodic structure importance": 1.0,
      "relaxed speech importance": 0.1
         }   
    if style is None:
        return speech_style
    else:
        for s in style.split(','):            
            key, value = s.split('=')
            if key == "lex":
                speech_style["lexical contrast importance"] = float(value)
            elif key == 'sr':
                speech_style["relative speech rate importance"] = float(value)
            elif key == 'pros':
                speech_style["prosodic structure importance"] = float(value)
            elif key == 'rlx':
                speech_style["relaxed speech importance"] = float(value)
            else:
                raise ValueError('key ' + key + ' is not recognized')        
    
        return speech_style