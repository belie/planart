#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 22 10:03:02 2022

@author: benjamin
"""

from scipy.optimize import minimize, dual_annealing
import numpy as np
from .utils import gesture_timing
from ._io import export_solution
from .utils import workspace
import os
from .tautools import extract_parameters
from ._articulator import local_number_of_parameters
from .phonetics import phoneme_prediction
from .debugs import ( 
    save_current_state,
    get_time
    )

def check_global_optimization(local_min_cost, local_min_vt,
                              global_min_cost, global_min_vt):
    """ Check whether the local minimum in the local loop
    is temporary a global one """
    if local_min_cost < global_min_cost:
        global_min_vt = local_min_vt
        global_min_cost = local_min_cost

    return global_min_cost, global_min_vt

def check_local_optimization(new_vocal_tract, prms, local_min_cost,
                             stub_const, min_x, local_min_vt,
                             stub_count, early_stop=False, method="etd",
                             end_mov=True):
    """ Check whether the local minimum is temporary a global one """

    new_cost = new_vocal_tract.get_cost_function(prms, method=method)
    isContinue = True
    gain = np.abs((new_cost - local_min_cost) / local_min_cost)
    if new_cost < local_min_cost:
        local_min_cost = new_cost
        min_x = new_vocal_tract.get_solution(method=method,
                                             end_mov=end_mov)
        local_min_vt = new_vocal_tract.copy()
        if early_stop:
            isContinue = False
        if gain > prms.convergence_threshold:
            stub_count = stub_const
        else:
            stub_count -= 1
            if stub_count == 0:
                isContinue = False
    else:
        stub_count -= 1
        if stub_count <= 0:
            isContinue = False
    return min_x, stub_count, local_min_vt, local_min_cost, isContinue

def global_optimization(vocal_tract, prms, weights, method, early_stop=False,
                        end_mov=True):
    """ Performs global optimization """

    if "etd" in method and vocal_tract.initial_sequence is None:
        vocal_tract.initial_sequence = gesture_timing(vocal_tract)[0]
    prms.effort_weight, prms.parsing_weight, prms.duration_weight  = weights
    min_x = vocal_tract.get_solution(method=method, end_mov=end_mov)
    x0_start = vocal_tract.get_solution(method=method, end_mov=end_mov)
    global_min_cost = vocal_tract.get_cost_function(prms, method=method)
    if prms.verbosity >= 1 and method == "etd":
        print_solution(list(min_x), vocal_tract.start_index)
        print("Initial cost function: ", global_min_cost, flush=True)
    global_min_vt = vocal_tract.copy()
    col_len = None
    if prms.verbosity >= 1 and (prms.repetitions <= 1 or prms.parallel <= 1):
        col_len = print_first_line(prms, method)    
    if prms.catch_solution:
        output_score = os.path.join(workspace().as_posix(),
                                    "intial_solution.json")
        phoneme_prediction(global_min_vt, prms.probabilistic_model, 
                           len_win=1)
        export_solution(global_min_vt, output_score, prms.attempts)
    stub_const = prms.attempts
    initial_tract = vocal_tract.copy()

    if prms.parallel <= 1 or prms.repetitions <= 1:
        for k in range(prms.repetitions):
            local_min_cost, local_min_vt = repetition(k, vocal_tract,
                                                      initial_tract, prms,
                                                      x0_start, stub_const,
                                                      global_min_cost, method,
                                                      col_len, early_stop,
                                                      end_mov,
                                                      isparallel=False)

            global_checks = check_global_optimization(local_min_cost, local_min_vt,
                                          global_min_cost, global_min_vt)
            global_min_cost, global_min_vt = global_checks
    else:
        from joblib import Parallel, delayed
        if os.name == "nt":
            backend = "threading"
        else:
            backend = "loky"
        from tqdm import tqdm
        solutions = Parallel(n_jobs=prms.parallel, backend=backend,
                         prefer="processes")(delayed(repetition)(k,
                                                     vocal_tract.copy(),
                                                  initial_tract, prms,
                                                  x0_start, stub_const,
                                                  None,
                                                  method, col_len,
                                                  early_stop,
                                                  end_mov,
                                                  isparallel=True) for
                                                 k in tqdm(range(prms.repetitions), 
                                                           desc="repetitions", 
                                                           disable=not prms.verbosity))
        for solution in solutions:
            local_min_cost, local_min_vt = solution
            global_checks = check_global_optimization(local_min_cost, local_min_vt,
                                          global_min_cost, global_min_vt)
            global_min_cost, global_min_vt = global_checks

    min_x = global_min_vt.get_solution(method=method, end_mov=end_mov)
    if prms.verbosity >= 1:        
        print("Optimization completed", flush=True)
        print("Global minimum: ", global_min_cost, flush=True)
        if method == "etd":
            print_solution(list(min_x), global_min_vt.start_index)
    return min_x

def local_optimization(vocal_tract, min_x, prms, method="etd", end_mov=True):
    """ Run an optimization procedure to find local minimum """
    rand_nudge = 1 + (0.05 - 0.10*np.random.rand(len(min_x)))
    x = (rand_nudge * min_x).tolist()
    vocal_tract.solution_to_vt(x, method=method, end_mov=end_mov)
    x0 = vocal_tract.get_solution(method=method, end_mov=end_mov)
    args = (vocal_tract, prms, method, end_mov)
    if prms.debug:
        prms.optimization_state = {'evaluation': [],
                                   'start time': get_time(),
                                   'iteration times': [],
                                   'vocal tract': vocal_tract,
                                   'costs': []}
    return optimization(x0, args,
                        maxiter=prms.max_iterations,
                        method=prms.method, end_mov=end_mov)

def make_bounds_etd(nb_x):
    """ Creates bounds for ETD optimization """
    return ((0, None), )*int(nb_x/2-1) + ((50e-3, None), )*int(nb_x/2+1)

def make_bounds_tau(vocal_tract, prms, end_mov=True):
    """ Creates bounds for Tau optimization """
    nb_arts, nb_gests, nb_param = extract_parameters(vocal_tract, end_mov=end_mov)
    min_bounds = [m["minimal value"] for m in vocal_tract.articulatory_model]
    max_bounds = [m["maximal value"] for m in vocal_tract.articulatory_model]
    
    if hasattr(prms, "kappa_bounds"):
        tau_bounds = (prms.kappa_bounds[0], prms.kappa_bounds[1])
    else:
        tau_bounds = (0.1, 0.9)
    bounds = ()
    for ng in range(nb_gests):
        bounds += (tau_bounds,)*local_number_of_parameters(vocal_tract.optimization.kappa, nb_arts)
        if vocal_tract.optimization.position == 'local':
            for mn, mx in zip(min_bounds, max_bounds):
                bounds += ((mn, mx),)
        bounds += ((0.05, 0.75),)*local_number_of_parameters(vocal_tract.optimization.duration, nb_arts)    

    if vocal_tract.optimization.pressure:
        for n in range(vocal_tract.number_of_syllables):
            bounds += ((400, 2000),) + ((0.05, 2),)
    return bounds

def objective(x, *args):
    """ Objective function """
    vocal_tract, prms, method, end_mov = args
    if prms.debug:
        cost = vocal_tract.solution_to_cost(list(x), prms, method=method, 
                                        end_mov=end_mov)
        current_time = get_time()
        prms.optimization_state["evaluation"].append(list(x))
        prms.optimization_state["vocal tract"] = vocal_tract
        prms.optimization_state["iteration times"].append(current_time - 
                                                          prms.optimization_state["start time"])
        prms.optimization_state["costs"].append(cost)
        return cost        
    else:    
        return vocal_tract.solution_to_cost(list(x), prms, method=method, 
                                        end_mov=end_mov)

def optimization(x0, args, maxiter=200, method='nelder-mead', end_mov=True):
    """ Optimization procedure """

    if args[-2] == "etd":
        bounds = make_bounds_etd(len(x0))
    else:
        bounds = make_bounds_tau(args[0], args[1], end_mov)

    options={'maxiter': maxiter * len(x0),
             'maxfev': 4 * maxiter * len(x0), 
             'disp': args[1].verbosity >= 2}

    if method == "annealing":
        bounds = (((0, 5), )*int(len(x0)/2-1) +
                  ((50e-3, 1), )*int(len(x0)/2) + ((1, 100), ))
        result = dual_annealing(objective, bounds, x0=x0,
                                args=args, maxiter=maxiter)
    else:
        result = minimize(objective, x0, args=args, method=method,
                              options=options, bounds=bounds)

    return write_solution(result, args)

def print_first_line(prms, method="tau"):
    print('\n')    
    if method == "tau":
        first_line = ('rep. || att. || global min cost || local min cost ||' + 
              ' global cost || intell. (weighted) ||  effort (weighted)  || ' + 
              'PER (weighted) || prosody (weighted) || ' +
              'speech rate (weighted) || duration || nb. iter. (eval)')
    elif method == "etd":
        first_line = ('rep. || att. || global min cost || local min cost ||' + 
              ' global cost || intell. (weighted) ||  effort (weighted)  || ' + 
              ' prosody (weighted) || ' +
              'speech rate (weighted) || duration || nb. iter. (eval)')
    
    if prms.per_weight == 0:        
        first_line = first_line.replace('PER (weighted) || ', '')
    if prms.prosodic_weight == 0:        
        first_line = first_line.replace('prosody (weighted) || ', '')
    if prms.speech_rate_weight == 0:        
        first_line = first_line.replace('speech rate (weighted) || ', '')
    if (prms.duration_weight != 0 and 
        prms.prosodic_weight == 0 and prms.speech_rate_weight == 0):
        first_line = first_line.replace('duration', 
                                        'duration (weighted) || duration')
    
    print('-'*len(first_line), flush=True)
    print(first_line, flush=True)
    print('-'*len(first_line), flush=True)
    return [len(c) for c in first_line.split('||')]

def print_solution(x, idx):
    """ Prints temporary solution """

    x.insert(idx, 0)
    nb_gests = int((len(x)-1)/2)
    x_start = x[:nb_gests]
    x_duration = x[nb_gests:-1]
    general_stiffness = x[-1]
    x.pop(idx)
    ends = [s + d for s,d in zip(x_start, x_duration)]
    for s, e in zip(x_start, ends):
        print("Gesture starts at " + "%.2f" %s + "s and ends at " +
              "%.2f" %e + "s", flush=True)
    print("General stiffness is ", general_stiffness, flush=True)
    
    
def print_temp_solution(vocal_tract, k, prms, stub_count, global_min_cost, 
                        local_min_cost, col_len, nb_iter, method="tau"):
    """ Display the current state of the optimization process """
    
    weights = [getattr(prms, attrs) for attrs in ['parsing_weight',
                                                  'effort_weight',
                                                  'per_weight',
                                                  'duration_weight',
                                                  'prosodic_weight',
                                                  'speech_rate_weight']]
    lines = [str(k+1) + "/" + str(prms.repetitions), str(stub_count), 
             '%3.5f' %global_min_cost, '%3.5f' %local_min_cost,
             '%3.5f' %vocal_tract.cost_function, '%3.5f' %vocal_tract.parsing_cost + ' (' + 
             '%3.5f' %(vocal_tract.parsing_cost*weights[0]) + ')', 
             '%3.5f' %vocal_tract.effort_cost + ' (' + 
             '%3.5f' %(vocal_tract.effort_cost*weights[1]) + ')',
             '%3.5f' %vocal_tract.per_cost + ' (' + 
             '%3.3f' %(vocal_tract.per_cost*weights[2]) + ')',             
             '%3.5f' %vocal_tract.prosodic_cost + ' (' + 
             '%3.5f' %(vocal_tract.prosodic_cost*weights[4]) + ')',
             '%3.5f' %vocal_tract.speech_rate_cost + ' (' + 
             '%3.5f' %(vocal_tract.speech_rate_cost*weights[5]) + ')',
             str(int(vocal_tract.duration*1000)) + ' ms',
             str(nb_iter[0]) + ' (' + str(nb_iter[1]) + ')']
    
    if prms.per_weight == 0 and method == "tau":        
        lines.remove('%3.5f' %vocal_tract.per_cost + ' (' + 
        '%3.3f' %(vocal_tract.per_cost*weights[2]) + ')')
    if prms.prosodic_weight == 0:        
        lines.remove('%3.5f' %vocal_tract.prosodic_cost + ' (' + 
        '%3.5f' %(vocal_tract.prosodic_cost*weights[4]) + ')')
    if prms.speech_rate_weight == 0:        
        lines.remove('%3.5f' %vocal_tract.speech_rate_cost + ' (' + 
        '%3.5f' %(vocal_tract.speech_rate_cost*weights[5]) + ')')
    if (prms.duration_weight != 0 and 
        prms.prosodic_weight == 0 and prms.speech_rate_weight == 0):
        lines.insert(-2, '%3.5f' %vocal_tract.duration_cost + ' (' + 
        '%3.5f' %(vocal_tract.duration_cost*weights[3]) + ')')
    
    if method == "etd":
        weights.remove(prms.per_weight)
        lines.remove('%3.5f' %vocal_tract.per_cost + ' (' + 
        '%3.3f' %(vocal_tract.per_cost*weights[2]) + ')')
    second_columns = [write_column(line, c) for line, c in zip(lines, col_len)]
    second_line = '||'.join(second_columns)
    print(second_line, flush=True)

# def remove_column(line, weight, )

def repetition(k, vocal_tract, initial_tract, prms, x0_start, stub_const,
               global_min_cost, method, col_len, early_stop=False, 
               end_mov=True, isparallel=False):
    """ Perform one otpimization run """
    min_x = [xx for xx in x0_start]
    stub_count = stub_const
    local_min_cost = initial_tract.get_cost_function(prms, method=method)
    local_min_vt = initial_tract.copy()
    isContinue = True

    while isContinue:
        optim_vt, nb_iter = local_optimization(vocal_tract, min_x, prms, method=method,
                                      end_mov=end_mov)

        local_checks = check_local_optimization(optim_vt, prms,
                                     local_min_cost, stub_const,
                                     min_x, local_min_vt, stub_count,
                                     early_stop=early_stop, method=method,
                                     end_mov=end_mov)
        min_x, stub_count, local_min_vt, local_min_cost, isContinue = local_checks
        if stub_const == 0:
            isContinue = False
        if prms.catch_solution and method == "tau":
            output_score = os.path.join(workspace().as_posix(),
                        "tmp_solution_repetition_" + str(k+1) + ".json")
            phoneme_prediction(local_min_vt, prms.probabilistic_model, 
                               len_win=1)
            export_solution(local_min_vt, output_score, stub_count)
        if (prms.verbosity >= 1 and 
            global_min_cost is not None and 
            not isparallel):           
            print_temp_solution(local_min_vt, k, prms, stub_count, 
                                    global_min_cost, local_min_cost, 
                                    col_len, nb_iter, method)

    return local_min_cost, local_min_vt

def write_column(string, col_size):
    nb_space = int((col_size - len(string))/2)
    string = " "*nb_space + string + " "*nb_space
    while len(string) < col_size:
        string += " "
    return string

def write_solution(solution, args):
    """ Returns the optimal vocal tract instance """
    vocal_tract = args[0].copy()
    vocal_tract.solution_to_vt(list(solution["x"]), method=args[-2], 
                               end_mov=args[-1])
    return vocal_tract, (solution["nit"], solution["nfev"])
