#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  2 14:09:54 2022

@author: benjamin
"""

from ._gesture import Gesture
from ._articulator import Articulator
from ._activation import Activation
from .utils import (psinv)
import numpy as np
from scipy import integrate
import warnings


def assign_param_to_articulator(art, keys):
    """ Assigns parameters to articulator objects """
    new_art = Articulator()
    for key in keys:
        setattr(new_art, key, art[key])
    return new_art

def assign_param_to_gesture(gest, idx, keys, timings, weights):
    """ Assigns parameters to gesture objects """
    start, duration, end = timings
    new_gest = Gesture()
    for key in keys:
        if key in ["boundary", "target", "target_speech_ready"]:
            setattr(new_gest, key, gest[key])
        else:
            setattr(new_gest, key, gest[key])
    for key, value in zip(["start", "duration", "end", "weights"],
                       [start[idx], duration[idx], end[idx], weights[idx]]):
        setattr(new_gest, key, value)
    for key, value in zip(["effective_start", "effective_duration"],
                       [start[idx], duration[idx]]):
        setattr(new_gest, key, [0]*len(value))
    return new_gest

def group_articulators(articulators):
    """ Creates a vector of articulators to be assigned
    to the vocal tract object """
    return [assign_param_to_articulator(art, articulators[0].keys())
            for art in articulators]

def group_gestures(gestures, timings, weights):
    """ Creates a vector of gestures to be assigned
    to the vocal tract object """
    if weights is not None:
        effort, precision, duration_weight = weights
        dicts_weight = weights_to_dict(effort, precision, duration_weight)
    else:
        dicts_weight = (None,)*len(timings[0])
    return [assign_param_to_gesture(gest, idx, gestures[0].keys(),
                                    timings, dicts_weight)
            for idx, gest in enumerate(gestures)]

def one_step_etd(n, vocal_tract, n_pts, previous_values, delta_t, activs):
    """ Computes new positions at the next time step when using a PDE solver """
    zm1, zm2 = previous_values
    same_activation = vocal_tract.find_activation(n_pts, n)
    if not same_activation or n == 0:
        activs = Activation("parent", vocal_tract)
        activs.gest2activation()
        if len(activs.gestures) > 0:
            activs.get_activations()
    if len(activs.gestures) > 0:
        K_active = activs.articulatory_space.stiffness
        target_active = activs.articulatory_space.target
    else:
        K_active = np.zeros_like(vocal_tract.articulatory_space.stiffness)
        target_active = np.zeros((K_active.shape[0], 1))

    coll_dist = np.abs(vocal_tract.anatomy.boundaries -
                        vocal_tract.anatomy.general_matrix @ zm1).squeeze()

    Bbl = (psinv(vocal_tract.anatomy.general_matrix,
                    vocal_tract.anatomy.inverse_matrices[1],
                    inversion=False) @
           (vocal_tract.tract_space.damping + np.diag(1/(1e9*(coll_dist)**3))) @
           vocal_tract.anatomy.general_matrix)
    yn_coeff = (Bbl + vocal_tract.speech_ready_space.damping) / delta_t
    yn1_coeff = (vocal_tract.articulatory_space.stiffness * vocal_tract.general_stiffness +
                  K_active - yn_coeff)
    cst_coeff = -(vocal_tract.general_stiffness * vocal_tract.articulatory_space.stiffness @
                  vocal_tract.articulatory_space.initial_state +
                  K_active @ target_active)

    z_k = (-delta_t**2 * vocal_tract.articulatory_space.inverse_mass @
            (cst_coeff + yn1_coeff @ zm2 + yn_coeff @ zm1) + 2*zm1 - zm2)


    return z_k.squeeze(), (z_k, zm1), activs

def springs(y, t, vocal_tract, activs):
    """ 2nd-order coupled oscillators function for ODE solver """

    nb_arts = vocal_tract.articulatory_space.inverse_mass.shape[0]
    if len(activs.gestures) > 0:
        K_active = activs.articulatory_space.stiffness
        target_active = activs.articulatory_space.target
        Astar = psinv(activs.anatomy.general_matrix,
                    vocal_tract.anatomy.inverse_matrices[1],
                    inversion=False)
        By = Astar @ activs.task_space.damping @ activs.anatomy.general_matrix
    else:
        K_active = np.zeros_like(vocal_tract.articulatory_space.stiffness)
        target_active = np.zeros((K_active.shape[0], 1))
        Astar = np.zeros((nb_arts, 1))
        By = np.zeros((nb_arts, nb_arts))

    dydt = np.zeros((nb_arts*3,1))
    dydt[:nb_arts] = y[nb_arts:2*nb_arts].reshape(-1, 1)
    coll_dist = (vocal_tract.anatomy.boundaries -
                        (vocal_tract.anatomy.general_matrix @
                         y[:nb_arts].reshape(-1, 1))).squeeze()

    bound = np.diag(np.diag((1/(1e9*(coll_dist)**3).squeeze())))
    Abstar = psinv(vocal_tract.anatomy.general_matrix,
                    vocal_tract.anatomy.inverse_matrices[1],
                    inversion=False)
    dtract = (vocal_tract.anatomy.general_matrix @
              y[nb_arts:2*nb_arts].reshape(-1, 1))


    dy = y[:nb_arts].reshape(-1, 1) - vocal_tract.articulatory_space.initial_state
    dydt[nb_arts:2*nb_arts] = (-vocal_tract.articulatory_space.inverse_mass @
                               (vocal_tract.general_stiffness * vocal_tract.articulatory_space.stiffness @ dy
        + vocal_tract.speech_ready_space.damping @ y[nb_arts:2*nb_arts].reshape(-1, 1)
        + K_active @ (y[:nb_arts].reshape(-1, 1) - target_active)
        + By @ y[nb_arts:2*nb_arts].reshape(-1, 1)
        + (Abstar @ np.diag(np.sign(dtract).squeeze())) @ bound @
        vocal_tract.anatomy.general_matrix @ y[nb_arts:2*nb_arts].reshape(-1, 1)))

    dydt[2*nb_arts:] = (np.abs((vocal_tract.general_stiffness * vocal_tract.articulatory_space.stiffness) @
                               (y[:nb_arts].reshape(-1, 1) - vocal_tract.articulatory_space.initial_state) +
                               vocal_tract.speech_ready_space.damping @ y[nb_arts:2*nb_arts].reshape(-1, 1)) +
                 np.abs(K_active @ (y[:nb_arts].reshape(-1, 1) - target_active) +
                        By @ y[nb_arts:2*nb_arts].reshape(-1, 1)))

    return dydt.squeeze().tolist()

def trajectory_task_dynamics(vocal_tract, prms):
    """ compute articulators trajectories using task dynamics """

    n_pts, t_vec = vocal_tract.tp2activation(prms)
    nb_frame = len(t_vec)
    y0 = np.zeros(3*len(vocal_tract.articulators))
    y0[:len(vocal_tract.articulators)] = vocal_tract.articulatory_space.initial_state.squeeze()
    yvec = np.zeros((len(vocal_tract.articulators), nb_frame))
    vocal_tract.time_vector = t_vec

    if prms.solver == "ode":
        event_points = [0]
        for n in range(1, nb_frame):
            same_activation = vocal_tract.find_activation(n_pts, n)
            if not same_activation:
                event_points.append(n)
        event_points.append(nb_frame)
        event_points = np.unique(event_points)
        for n in range(len(event_points) - 1):
            curr_n = event_points[n]
            np1 = event_points[n+1]
            tspan = t_vec[curr_n:np1]
            same_activation = vocal_tract.find_activation(n_pts, curr_n)
            activs = Activation("parent", vocal_tract)
            activs.gest2activation()
            if len(activs.gestures) > 0:
                activs.get_activations()

            with warnings.catch_warnings():
                warnings.filterwarnings("ignore")
                y = integrate.odeint(springs, y0.squeeze(), tspan,
                                      args=(vocal_tract, activs))
                yvec[:, curr_n:np1] = (y[:, :5]).T
                y0 = y[-1, :].reshape(-1, 1)
    else:

        previous_values = [vocal_tract.articulatory_space.initial_state]*2
        delta_t = 1/prms.sampling_rate
        activs = None
        for n in range(nb_frame):
            yvec[:, n], previous_values, activs = one_step_etd(n, vocal_tract,
                                                        n_pts, previous_values,
                                                        delta_t, activs)

    return  yvec, vocal_tract.anatomy.general_matrix @ yvec

def update_vt_etd(vocal_tract, x):
    """ Updates the vocal tract object given solution x """

    x.insert(vocal_tract.start_index, 0)
    nb_gests = int((len(x)-1)/2)
    x_start = x[:nb_gests]
    x_duration = x[nb_gests:-1]
    vocal_tract.general_stiffness = x[-1]
    for g in vocal_tract.gestures:
        nb_activ_gest = len([d for d in g.duration if d > 0])
        g.start = [x_start[i] for i in range(len(g.start))
                   if g.duration[i] > 0]
        g.duration = [x_duration[i] for i in range(len(g.start)) if g.duration[i] > 0]
        g.end = [s + d for s, d in zip(g.start, g.duration)]
        [x_start.pop(0) for n in range(nb_activ_gest)]
        [x_duration.pop(0) for n in range(nb_activ_gest)]
        g.check_score()

    vocal_tract.solution = x

def vt_to_solution_etd(vocal_tract):
    """ Returns the solution vector from vocal tract object """

    x_start = []
    x_duration = []
    for g in vocal_tract.gestures:
        [x_start.append(s) for s, d in zip(g.start, g.duration) if
         d > 0]
        [x_duration.append(d) for d in g.duration if d > 0]

    vocal_tract.start_index = x_start.index(0)
    x_start.remove(0)
    x = x_start + x_duration
    x.append(vocal_tract.general_stiffness)
    vocal_tract.solution = x

    return x

def weights_to_dict(effort_vec, precision_vec, duration_vec):
    """ Creates a dictionary of weights """
    dicts_weight = []
    for e1, p1, d1 in zip(effort_vec, precision_vec, duration_vec):
        new_dict = [{"effort": e,
                         "parsing_precision": p,
                         "parsing_duration": d}
                 for e, p, d in zip(e1, p1, d1)]
        dicts_weight.append(new_dict)
    return dicts_weight


