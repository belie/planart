# -*- coding: utf-8 -*-
"""
Created on Wed Oct  5 11:41:37 2022

@author: belie
"""

import spychhiker as sph
import maedeep.parametric
import numpy as np
from .utils import interpolate, linspace
from .tautools import generate_tau


def glottal_control(vocal_tract, F0=100):
    """ Adds a Glottis object to the vocal tract """
    
    nb_frame = vocal_tract.waveguide[0].area_function.area.shape[1]
    Gl = sph.Glottis()
    Gl.fundamental_frequency = F0*np.ones(nb_frame)
    Gl.partial_abduction = 1e-11*np.ones(nb_frame)
    Gl.oscillation_factor = np.ones(nb_frame)
    vocal_tract.oscillators = [Gl]
   
def run_simulation(vocal_tract, output_file, sro=int(1.2e4)):
    """ Runs ESMF and saves in audio file """
    Const = sph.timestruct()
    Const.namp = 3e-6 # Sets the level of frication noise
    sri = vocal_tract.simulation_frequency
    vocal_tract.esmfsynthesis(Const)
    vocal_tract.outputresample(sro, sri)
    p_o = vocal_tract.pressure_radiated
    win = np.ones_like(p_o)
    rise = generate_tau(0.454, 0, 1, int(0.1*sro))[0][0]
    down = generate_tau(0.05, 1, 0, int(0.1*sro))[0][0]
    win[:len(rise)] = rise
    win[-len(down):] = down
    p_o = p_o * win
   
    # We use SpeechAudio object
    SP = sph.SpeechAudio('signal', p_o,
        'sampling_frequency', sro)
    # Save in audio file
    SP.savespeech(output_file, "normalized")    

def subglottal_pressure(vocal_tract, psubmax=500, bounds=(0.05, 0.05)):
    """ Adds the subglottal pressure to the vocal tract """
    nb_frame = vocal_tract.waveguide[0].area_function.area.shape[1]
    psub = psubmax*np.ones(nb_frame)
    win = np.ones_like(psub)
    sro = vocal_tract.simulation_frequency
    rise = generate_tau(0.454, 0, 1, int(bounds[0]*sro), np_lin=True)[0][0]
    down = generate_tau(0.454, 1, 0, int(bounds[0]*sro), np_lin=True)[0][0]
    win[:len(rise)] = rise
    win[-len(down):] = down
    vocal_tract.subglottal_control = psub * win    
    
def trajectory_to_vt(trajectory, sri, sro=int(2e4)):
    """ Returns a VT network object from MAEDA trajectories """

    area_function = maedeep.parametric.articulatory_to_area(trajectory)
    old_af = area_function.area
    old_lf = area_function.length
    nb_tubes, nb_frame = old_af.shape
    time_vector = np.arange(nb_frame) / sri
    new_nb_frame = int(time_vector[-1]*sro + 1)
    new_time_vector = linspace(0, time_vector[-1], new_nb_frame)

    new_af = np.zeros((nb_tubes, new_nb_frame))
    new_lf = np.zeros_like(new_af)

    for n in range(nb_tubes):
        new_af[n,:] = interpolate(time_vector, old_af[n,:], new_time_vector)
        new_lf[n,:] = interpolate(time_vector, old_lf[n,:], new_time_vector)
        

    MOT = sph.MainOralTract()
    MOT.area_function = sph.AreaFunction('area', new_af,
                                     'length', new_lf,
                                     'parent', MOT)

    VTobj = sph.VtNetwork()
    VTobj.waveguide = [MOT]
    VTobj.simulation_frequency = sro
    
    return VTobj

