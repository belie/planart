#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri May 27 09:46:46 2022

@author: benjamin
"""

from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.mixture import GaussianMixture
from sklearn.neural_network import MLPClassifier
import h5py
import numpy as np
import os
from ._config import GlobalConfig


def check_model(model, path):
    """ Check if model is a pre-lodaed model or a path """
    if isinstance(model, str):
        model = open_gmm_model(select_model(model, path))
    if not isinstance(model, GaussianMixture) and not isinstance(model, MLPClassifier) and not isinstance(model, QuadraticDiscriminantAnalysis):
        raise ValueError("Model is not a valid model")
    return model

def dediag(mtx):
    """ Squeeze diagonal matrix """
    nb_comp, nb_feat = mtx.shape[:2]
    new_mtx = np.zeros((nb_comp, nb_feat))
    for k in range(nb_comp):
        new_mtx[k, :] = np.diag(mtx[k, :,: ].squeeze())
    return new_mtx.squeeze()

def diagonalize(covars):
    """ Create diagonal covariance matrix """
    diagonal_covar = np.zeros((covars.shape[0], covars.shape[1], covars.shape[1]))
    for k in range(covars.shape[0]):
        diagonal_covar[k, :, :] = np.diag(covars[k, :].squeeze())
    return diagonal_covar

def get_proba(formants, model="english", path=None):
    """ Returns the vowel probability for a set of formant values
    and a given model. model can either be a file, or any
    pre-loaded gaussian mixture model """

    model = check_model(model, path)
    return model.predict_proba(reshape_formants(formants, model))

def get_vowel_index(formants, model="english", path=None):
    """ Returns the vowel index with the maximal likelihood """

    model = check_model(model, path)
    return [v for v in model.predict(reshape_formants(formants, model))]

def open_gmm_model(gmm_file):
    """ Open file containing the desired acoustic model and
    returns a GMM object """
    keys = ["mean", "covariance", "weights",
            "converged", "iters", "nb_var", "covariance_type"]
    with h5py.File(gmm_file, "r") as hf:
        data = [hf[key][()] for key in keys]

    means, covars, weights, convs, nb_iter, nb_var, covar_type = data
    if covar_type.squeeze() == 1:
        covar_type = "diag"
        covars = covars.squeeze()
        diagonal_covars = diagonalize(covars)
    else:
        covar_type = "full"
    nb_components = len(weights)
    gm = GaussianMixture(n_components=nb_components, 
                         covariance_type=covar_type)
    gm.means_ = means.T
    gm.weights_ = weights.squeeze()
    gm.covariances_ = covars
    if covar_type == "diag":
        gm.precisions_ = dediag(np.linalg.inv(diagonal_covars))
        gm.precisions_cholesky_ = dediag(np.linalg.cholesky(np.linalg.inv(diagonal_covars)))
    else:
        gm.precisions_ = np.linalg.inv(covars)
        gm.precisions_cholesky_ = np.linalg.cholesky(np.linalg.inv(covars))
    gm.converged_ = convs
    gm.n_features_in_ = nb_var
    gm.n_iter_ = nb_iter

    return gm

def reshape_formants(features, model):
    """ Reshapes feature patterns to fit model dimension """
    if isinstance(model, GaussianMixture):
        nb_f = model.means_.shape[1]
    if isinstance(model, MLPClassifier) or isinstance(model, QuadraticDiscriminantAnalysis):
        nb_f = model.n_features_in_
    if isinstance(features, list):
        features = np.array(features)
    if len(features.shape) == 1:
        features = features.reshape(1, -1)
    nb_frame, nb_features = features.shape
    if nb_features != nb_f and nb_frame != nb_f:
        raise ValueError("Feature matrix has not a valid size (" +
                         str(nb_frame) + "x" + str(nb_features))
    if nb_features != nb_f and nb_frame == nb_f:
        return features.T
    if nb_features == nb_f:
        return features

def select_model(language="english", path=None):
    """ Returns the file containing the selected GMM model """
    if path is None:
        path = os.path.join(GlobalConfig.acoustic_models_path().as_posix(),
                            'probabilistic', 'gmm')
    if not os.path.isdir(path):
        raise ValueError("Path containing acoustic models (" + path +
                         ") does not exist")
    model_file = os.path.join(path, language + ".h5")
    if not os.path.isfile:
        raise ValueError("Selected model (" + language + ") does not exist")

    return model_file
