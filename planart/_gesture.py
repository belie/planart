#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 30 10:02:52 2021

@author: benjamin
"""

import numpy as np
import copy


class Gesture:
    name = None
    target = None
    target_speech_ready = None
    boundary = None
    anatomy_vector = None
    stiffness = None
    tract_space_init = None
    tract_function = []
    art_space_init = None
    articulator_function = []
    parent = None
    start = None
    duration = None
    end = None
    occlusion = None
    index = None
    parsing_cost = 0
    articulatory_effort = 0
    activation = None
    start_index = None
    duration_index = None
    weights = None
    precision = None
    effective_duration = None
    effective_start = None

    # Constructor method
    def __init__(self, *args):
        nargin = len(args)
        for k in range(0, nargin, 2):
            key, value = args[k:k+2]
            setattr(self, key, value)

    def check_score(self):
        """ Check whether the gestural score is valid """
        self.start = [0 if i < 0 else i for i in self.start]
        self.duration = [50e-3 if i < 0 else i for i in self.duration]
        self.end = [s + d for s, d in zip(self.start, self.duration)]
        overlap = check_overlap(self)
        while overlap:
            overlap = check_overlap(self)

    def compute_parsing_cost(self, prms):
        self.parsing_cost = 0
        for n, (tf, weights) in enumerate(zip(self.tract_function, self.weights)):
            closure_duration, closure_mask, closure_start = closure_masking(self, 
                                                                            tf, 
                                                                            prms)
            
            if self.occlusion:
                self.effective_duration[n] = closure_duration
                eff_start = closure_start
                dg = 2 / np.pi * np.arctan(closure_duration * weights["parsing_duration"])
                pmat = np.sign(closure_duration)
                
            else:
                precision = self.target - tf[self.index, :]
                init = self.target - self.tract_space_init[self.index]
                pmat = (1 - abs(precision/init)) * (1 - closure_mask)
                t2 = len([x for i, x in enumerate(pmat)
                          if x >= prms.realization_threshold
                          and i >= int(len(pmat)/2)])
                eff_start, self.effective_duration[n] = find_precision(pmat, 
                                                                        prms)  
                c = weights["parsing_duration"]  * prms.vowel_timing_factor                                                                    
                dg = 2 / np.pi * np.arctan(c * t2 / prms.sampling_rate)
            self.parsing_cost += (1 - np.max(pmat) * dg) * weights["parsing_precision"]
            self.effective_start[n] = eff_start + self.start[n]
        

    def compute_force(self, prms):
        self.articulatory_effort = 0
        if len(self.articulator_function) > 0:
            M = self.parent.articulatory_space.mass
            dt = 1/prms.sampling_rate
            for articulator_function, weights in zip(self.articulator_function,
                                                     self.weights):
                displ = np.diff(articulator_function, axis=1)
                df = np.gradient(displ, dt, axis=1)
                df = np.gradient(df, dt, axis=1)
                self.articulatory_effort += (np.sum(np.sum(np.abs(M @ df*dt)**2,
                                                  axis=1), axis=0) * weights["effort"])

    def copy(self):
        return copy.deepcopy(self)

    def expand_gesture(self, nb_frame):
        return np.tile(np.array(self.target).reshape(-1,1), (1, nb_frame))

    def get_index(self):
        idx = [x for x in range(len(self.target)) if self.target[x] != 0]
        self.index = idx[0]


def closure_masking(gesture, tf, prms):
    """ Returns closure functions for computing the parsing cost """
    vocal_tract = gesture.parent
    nb_frame = tf.shape[1]
    ones =  np.ones((1, nb_frame))
    coll = tf[:2,:] - vocal_tract.anatomy.boundaries[:2].reshape(-1,1) @ ones
    bound_force = np.abs(1/(1e9*(coll**3)))
    closure_mask = (bound_force > prms.closure_threshold).astype(int)
    dc = np.diff(closure_mask, axis=0).squeeze()
    closure_mask[1,:] = np.max((np.zeros_like(dc), dc), axis=0)
    pure_closure_mask = np.sign(np.sum(closure_mask, axis=0))

    if gesture.occlusion:
        closure_points = np.diff(closure_mask, axis=1)
        onset_id, onset_pt = np.nonzero(closure_points == 1)
        if len(onset_pt) > 0:
            offset_id, offset_pt = np.nonzero(closure_points == -1)
            if len(offset_pt) < len(onset_pt):
                offset_pt = np.append(offset_pt, len(dc) -1)
            closure_start, closure_duration = find_start_and_duration(onset_pt,
                                                                      offset_pt,
                                                                      onset_id,
                                                                      gesture.index,
                                                                      prms)
            return closure_duration, pure_closure_mask, closure_start
        else:
            return 0, pure_closure_mask, 0
    else:
        return 0, pure_closure_mask, 0

def check_overlap(gesture):
    """ Check if same gestures overlap """
    overlap = False
    for i in range(len(gesture.start) - 1):
        if gesture.end[i] > gesture.start[i+1]:
            overlap = True
            gesture.start[i+1] = gesture.end[i]
            gesture.end[i+1] = gesture.start[i+1] + gesture.duration[i+1]

    return overlap

def find_precision(pmat, prms):
    """ finds starting  point of precise non-occlusive gestures """
    duration =  len([x for x in pmat if
                     x >= prms.realization_threshold]) / prms.sampling_rate
    start = [x/prms.sampling_rate for x in pmat if
             x >= prms.realization_threshold]
    if len(start) == 0:
        start = 0
    else:
        start = start[0]
    return start, duration

def find_start_and_duration(onset_pt, offset_pt, onset_id, index, prms):
    """ Finds start and duration of closure """
    duration = np.sum([offset_pt[i] - onset_pt[i] for i in range(len(onset_id))
                if onset_id[i] == index])
    start = np.min([onset_pt[i] for i in range(len(onset_id))
                    if onset_id[i] == index])
    return start/prms.sampling_rate, duration/prms.sampling_rate