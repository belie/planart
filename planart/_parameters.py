#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 30 10:02:34 2021

@author: benjamin
"""

import copy

class Parameters:
    sampling_rate = None
    effort_weight = 0
    parsing_weight = 0
    duration_weight = 0
    speech_rate_weight = 0
    prosodic_weight = 0
    per_weight = 0
    max_iterations = None
    realization_threshold = None
    closure_threshold = None
    vowel_timing_factor = None
    consonant_timing_factor = None
    probabilistic_model = None
    intensity_model = None
    parallel = None
    convergence_threshold = 0
    oversampling = 1
    prosodic_order = 2    
    debug = 0
    optimization_state = None
    pressure_opt = False
    subglottal_pressure = None
    fundamental_frequency = 0

    # Constructor method
    def __init__(self, *args):
        nargin = len(args)
        for k in range(0, nargin, 2):
            key, value = args[k:k+2]
            setattr(self, key, value)

    def copy(self):
        return copy.deepcopy(self)
    