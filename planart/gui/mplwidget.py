from PyQt5 import QtWidgets
from matplotlib.backends.backend_qt5agg \
    import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5 \
    import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
from matplotlib import gridspec

class MplCanvas(FigureCanvas):
    def __init__(self):
        self.fig = Figure(tight_layout=True)
        self.ax_waveform = self.fig.add_subplot(gridspec.GridSpec(2,1,height_ratios=[2, 1])[1])
        self.ax_spectro = self.fig.add_subplot(gridspec.GridSpec(2,1,height_ratios=[2, 1])[0],sharex=self.ax_waveform)
        self.ax_spectro.set_xticklabels([])
        self.ax_spectro.set_ylabel('Frequency (kHz)')
#        self.ax = self.fig.add_subplot(111)
        self.ax_waveform.set_xlabel('Time (s)')
        self.ax_waveform.set_ylim([-1, 1])

#        self.canvas.set_position([0.2,0.2, 1,1])
        FigureCanvas.__init__(self, self.fig)
        FigureCanvas.setSizePolicy(self,
                                   QtWidgets.QSizePolicy.Expanding,
                                   QtWidgets.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)

class MplWidget(QtWidgets.QWidget):
    def __init__(self, parent = None):
        QtWidgets.QWidget.__init__(self,parent)
        self.canvas = MplCanvas()
        self.navi_toolbar = NavigationToolbar(self.canvas, self)

        self.vbl = QtWidgets.QVBoxLayout()
        self.vbl.addWidget(self.canvas)
        self.vbl.addWidget(self.navi_toolbar)
        self.setLayout(self.vbl)
