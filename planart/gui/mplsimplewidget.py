#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 17 17:03:43 2018

@author: benjamin
"""

from PyQt5 import QtWidgets
from matplotlib.backends.backend_qt5agg \
    import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5 \
    import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure
from matplotlib import gridspec

class MplSimpleCanvas(FigureCanvas):
    def __init__(self):
        self.fig = Figure(tight_layout=True)
        self.ax_waveform = self.fig.add_subplot(111)
        self.ax_waveform.set_xlabel('Time (s)')
        FigureCanvas.__init__(self, self.fig)
        FigureCanvas.setSizePolicy(self,
                                   QtWidgets.QSizePolicy.Expanding,
                                   QtWidgets.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)        
        
class MplSimpleWidget(QtWidgets.QWidget):
    def __init__(self, parent = None):
        QtWidgets.QWidget.__init__(self,parent)
        self.canvas = MplSimpleCanvas()
        self.navi_toolbar = NavigationToolbar(self.canvas, self)
        
        self.vbl = QtWidgets.QVBoxLayout()
        self.vbl.addWidget(self.canvas)
#        self.vbl.addWidget(self.navi_toolbar)
        self.setLayout(self.vbl)
