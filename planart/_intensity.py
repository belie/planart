# -*- coding: utf-8 -*-
"""
Created on Thu Sep 21 15:50:51 2023

@author: belie
"""

import keras
import numpy as np
import pickle
from os import path
# from tensorflow_addons.metrics import RSquare

class IntensityModel:
    model = None
    input_features = None
    scalers = None
    distance = 0.01
    beta = None
    gamma = None
 
    # Constructor method
    def __init__(self, *args):
        nargin = len(args)
        for k in range(0, nargin, 2):
            key, value = args[k:k+2]
            setattr(self, key, value)            

def load_model(model_file, scaler_file):
    model = keras.models.load_model(model_file)    
    with open(scaler_file, "rb") as f:
        scaler_input, scaler_output = pickle.load(f)[:2]
    return IntensityModel("model", model,
                      "scalers", (scaler_input, scaler_output))

def predict_intensity(p, model):    
    p_norm = model.scalers[0].transform(p)
    linear_pressure = np.nan_to_num(model.scalers[1].inverse_transform(model.model.predict(p_norm, 
                                                                verbose=False)))
    linear_pressure[linear_pressure==0] = 1e-30
    L0 = 20*np.log10(linear_pressure)
    if model.distance > 0.01:
        L0 += 20*np.log10(0.01/model.distance)
    return L0.reshape(-1, 1).squeeze()

def intensity_score(p, model, background_noise=0):
    delta_L = predict_intensity(p, model) - background_noise
    return 1 / (1 + np.exp(-model.beta*(delta_L - model.gamma)))

def load_intensity_model(intensity_folder, prms):
    model_file = path.join(intensity_folder, 'intensity_model')
    intensity_model = load_model(model_file + '.h5', model_file + '.pkl')
    intensity_model.distance = prms.distance
    intensity_model.beta = prms.beta
    intensity_model.gamma = prms.gamma
    return intensity_model