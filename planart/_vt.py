#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 30 09:58:24 2021

@author: benjamin
"""

import copy
import numpy as np
from .utils import ( 
                    art_to_tract_mapping,
                    crit_damp,
                    gesture_effective_timing,
                    gesture_timing,
                    get_surface_duration,
                    get_time_vector,
                    get_vocal_effort,
                    max_list,
                    movement_padding,
                    write_json,
                    write_hdf5
                    )
from .phonetics import (
                        get_per,
                        phoneme_prediction
                        )
from ._subclasses import (Anatomy, Space)
from .etdtools import ( 
                        trajectory_task_dynamics,
                        update_vt_etd, 
                        vt_to_solution_etd
                        )
from .prosody._prosody import ( 
                                get_prosodic_structure, 
                                prosodic_difference
                                )
from .tautools import ( 
                        cost_parsing_precision,
                        update_vt_tau,
                        vt_to_solution_tau
                       )
from .optimtools import global_optimization
from .maedeeptools import (articulatory_to_task)
from ._proba import phoneme_probability

class VT:
    """ VT is a class modelling the vocal tract"""

    # pylint: disable=too-many-instance-attributes
    # pylint: disable=pointless-string-statement

    general_stiffness = 1
    articulators = []
    gestures = []
    time_vector = None
    parsing_cost = 0
    effort_cost = 0
    prosodic_cost = 0
    cost_function = 0
    duration_cost = 0
    per_cost = 0
    speech_rate_cost = 0
    duration = None
    stiffness_jacobian = []
    time_jacobian = []
    instant_stiffness_jacobian = 0
    instant_time_jacobian = 0
    solution = None
    start_index = None
    initial_duration = None
    articulatory_model = None
    surface_timing = None

    anatomy = None
    tract_space = None
    speech_ready_space = None
    articulatory_space = None
    activation_space = None
    initial_sequence = None
    prosodic_structure = None
    optimization = None
    reference_phonemes = None
    predicted_phonemes = None
    probability = None
    number_of_syllables = None
    intensity = None
    
    fundamental_frequency = None
    subglottal_pressure = None

    # Constructor method
    def __init__(self, *args):
        nargin = len(args)
        for k in range(0, nargin, 2):
            key, value = args[k:k+2]
            setattr(self, key, value)

    def compute_art_mk_matrix(self):
        """ Compute the mass and stiffness matrices in the articulatory space """
        if len(self.articulators) > 0:
            nb_arts = len(self.articulators)
            M = np.zeros((nb_arts, nb_arts))
            K = np.zeros((nb_arts, nb_arts))
            for idx in range(nb_arts):
                M[idx, idx] = self.articulators[idx].mass
                K[idx, idx] = self.articulators[idx].stiffness
            self.articulatory_space = Space("mass", M,
                                            "stiffness", K,
                                            "inverse_mass", np.diag(1/np.diag(M)),
                                            "parent", self)
            
    def compute_prosodic_structure(self, method, prms, normalization=True):
        if method == "etd":
            surface_durations = gesture_effective_timing(self)[-1] 
        elif method == "tau":
            if self.surface_timing is None:
                self.surface_timing = get_surface_duration(self, prms)
            surface_durations = np.diff(self.surface_timing, axis=1).squeeze()
            if len(surface_durations.shape) == 0:
                surface_durations = np.array([surface_durations])
        get_prosodic_structure(surface_durations, 
                               self.prosodic_structure,
                               normalization=normalization)

    def compute_tract_mk_matrix(self):
        """ Compute the mass and stiffness matrices in the tract space"""

        art_space = self.articulatory_space
        if (art_space.mass is not None and
            art_space.stiffness is not None):
            if self.tract_space is None:
                self.tract_space = Space("parent", self)
            art_to_tract_mapping(art_space, self.tract_space, "mass", 1)
            self.tract_space.get_stiffness(False)
            self.tract_space.damping = crit_damp(self.tract_space,
                                                      self.general_stiffness,
                                                      diagonal=True)

    def compute_trajectory(self, prms, method="etd"):
        """ Compute the articulatory gestures given targets in the tract space """

        if method == "etd":
            (self.articulatory_space.dynamics,
             self.tract_space.dynamics) = trajectory_task_dynamics(self, prms)
            self.get_gesture_function()
        elif method == "tau":
            if self.articulatory_space is None:
                self.articulatory_space = Space("parent", self)
            [a.generate_trajectory(oversampling=prms.oversampling, 
                                   model="tau") for a in self.articulators]
            movement_padding(self)
            self.articulatory_space.dynamics = np.array([a.dynamics for
                                                         a in self.articulators])
            self.time_vector = np.arange(self.articulatory_space.dynamics.shape[1])/prms.sampling_rate

    def compute_tract_dynamics(self, method="tau"):
        """ Compute tract variables function """
        if self.tract_space is None:
            self.tract_space = Space("parent", self)
        self.tract_space.dynamics = articulatory_to_task(self.articulatory_space.dynamics)

    def copy(self):
        """ Returns a copy of the VT instance """
        return copy.deepcopy(self)

    def export_dynamics(self, output_name, prms):
        """ exports articulatory and tract functions in a hdf5 file """
        if not hasattr(self.subglottal_pressure, 'dynamics'):
            psub = prms.subglottal_pressure
        else:
            psub = self.subglottal_pressure.dynamics
        write_hdf5(output_name,
                   "articulators", self.articulatory_space.dynamics,
                   "tract", self.tract_space.dynamics,
                   "time", self.time_vector,
                   "general_stiffness", self.general_stiffness,
                   "global_minimum", self.get_cost_function(prms),
                   "subglottal pressure", psub,
                   "fundamental frequency", prms.fundamental_frequency)

    def export_score(self, output_name, method="etd"):
        """ export gesture score into JSON file """

        data = dict({"phoneme" : []})
        if method == "etd":
            names, starts, durations = gesture_timing(self)
        else:
            dum_art = self.articulators[0]
            names = [n.name for n in dum_art.movements[:-1]]
            starts = [n.time_points[0] for n in dum_art.movements[:-1]]
            durations = [np.diff(n.time_points) for n in dum_art.movements[:-1]]
        for name, start, duration in zip(names, starts, durations):
            data["phoneme"].append(dict({"name": name,
                                         "start": float(start),
                                         "duration": float(duration)}))
        write_json(data, output_name)
        return data

    def find_activation(self, n_pts, n):
        """ Find active gestures. Returns True if same configuration """

        same_activation = True
        for idx, gest in enumerate(self.gestures):
            activation = n_pts[idx, n]
            if gest.activation != activation:
                gest.activation = activation
                same_activation = False
        return same_activation

    def get_articulatory_effort(self, prms, method="etd"):
        """ Compute the total articulatory effort """
        if method == "etd":
            for gest in self.gestures:
                gest.compute_force(prms)
            self.effort_cost = np.mean([g.articulatory_effort for
                                               g in self.gestures])
        else:
            self.effort_cost = np.mean([a.effort(oversampling=prms.oversampling,
                                                 model="tau", 
                                                 acceleration_model=prms.effort_model) for
                                               a in self.articulators])
        return self.effort_cost * prms.effort_weight

    def get_cost_function(self, prms, method="etd"):
        """ Compute the total cost function """
        self.cost_function = 0
        if self.number_of_syllables is None:
            self.number_of_syllables = len([s for s in self.prosodic_structure if 
                                        s['type'] == 'syllable'])
        if gesture_timing(self)[0] != self.initial_sequence and "etd" in method:
            self.cost_function = 1e12
        else:
            if "etd" in method:
                parsing_method = "etd"
            else:
                parsing_method = "tau"
                
            if ((prms.speech_rate_weight != 0 and method == "tau") or 
            (prms.prosodic_weight != 0 and self.number_of_syllables > 1) or
            prms.duration_weight != 0 and method == "tau"):
                self.surface_timing = get_surface_duration(self, prms) 
            if prms.effort_weight != 0:
                self.cost_function += self.get_articulatory_effort(prms, method)
            if prms.parsing_weight != 0:
                self.cost_function += self.get_parsing_cost(prms, 
                                                            method=parsing_method)
            if prms.speech_rate_weight != 0:
                self.cost_function += self.get_speech_rate_cost(prms, method)
            if method == "tau" and prms.per_weight != 0:
                self.cost_function += ( 
                    self.get_per_cost(prms)
                    )
            if prms.prosodic_weight != 0 and self.number_of_syllables > 1:
                self.cost_function += self.get_prosodic_cost(prms,
                                                             method=method)
            if prms.duration_weight != 0 and prms.speech_rate_weight == 0:
                self.cost_function += self.get_duration_cost(prms, 
                                                             model=method)
            if prms.pressure_opt:
                self.cost_function += get_vocal_effort(self, prms.sampling_rate)
                
        if prms.verbosity >= 2:
            print("Cost function is ", self.cost_function, flush=True)
        return self.cost_function

    def get_duration(self, prms, model="tau"):
        """ Compute the total duration of the utterance """
        if model == "tau":
            if self.surface_timing is None:
                self.surface_timing = get_surface_duration(self, prms) 
            self.duration = np.max(self.surface_timing) - np.min(self.surface_timing)
        elif model == "etd":
            self.duration = (self.articulatory_space.dynamics.shape[1] / 
                             prms.sampling_rate)
        return self.duration

    def get_duration_cost(self, prms, model="tau"):
        duration = np.array([[np.diff(m.time_points)[0] for 
                                    m in a.movements] for
                                  a in self.articulators])        
        self.duration = np.sum(duration, axis=1)[0]
        if prms.duration_model == "linear":           
            self.duration_cost = self.duration            
        elif prms.duration_model == "hyperbolic":
            beta = prms.reward_discounting_rate
            self.duration_cost = 1 - 1 / (1 + beta*self.duration)
        self.duration = np.sum(duration, axis=1)[0]
        return self.duration_cost * prms.duration_weight

    def get_gesture_function(self):
        """ Separate the articulatory gestures into individual gesture functions """
        for idx_gest, gest in enumerate(self.gestures):
            gest.articulator_function = []
            gest.tract_function = []
            for n in range(len(gest.start)):
                if gest.duration[n] > 0:
                    idx = [i for i in range(len(self.time_vector))
                           if self.time_vector[i] >= gest.start[n]
                           and self.time_vector[i] <= gest.end[n]]
                    gest.articulator_function.append(self.articulatory_space.dynamics[:, idx])
                    gest.tract_function.append(self.tract_space.dynamics[:, idx])

    def get_parsing_cost(self, prms, method="etd"):
        """ Compute the total parsing cost """
        
        if method == "etd":
            for gest in self.gestures:
                gest.compute_parsing_cost(prms)
            self.parsing_cost = np.sum([g.parsing_cost for g in self.gestures])
        else:
            idx = [g.index for g in self.articulators[0].movements]     
            weights = [g.weights for g in self.articulators[0].movements]
            if len(self.articulators[0].movements) > 1:
               idx = idx[:-1]
               weights = weights[:-1]

            self.parsing_cost = cost_parsing_precision(self, prms, idx,
                                                       weights)            
            
        return self.parsing_cost * prms.parsing_weight
    
    def get_per_cost(self, prms):
        self.per_cost = get_per(self, phoneme_prediction(self, 
                                    prms.probabilistic_model), 
                                    prms.probabilistic_model.neutral_phoneme)
        return prms.per_weight * self.per_cost

    def get_prosodic_cost(self, prms, method="tau"):
        """ Computes the prosodic cost """
        self.compute_prosodic_structure(method, prms, normalization=True)
        self.prosodic_cost = prosodic_difference(self.prosodic_structure,
                                                 order=prms.prosodic_order)       
        return prms.prosodic_weight * self.prosodic_cost

    def get_solution(self, method="etd", end_mov=True):
        """ Returns the solution vector """
        if method == "etd":
            x = vt_to_solution_etd(self)
        else:
            x = vt_to_solution_tau(self, end_mov=end_mov)
        return x
    
    def get_speech_rate_cost(self, prms, model="tau"):
        self.speech_rate_cost = ((self.initial_duration -
                                  self.get_duration(prms, model))/self.initial_duration)**2
        movement_duration = (self.articulatory_space.dynamics.shape[1] / 
                         prms.sampling_rate) - 0.05
        self.speech_rate_cost += (movement_duration/self.duration)**2
        
        return prms.speech_rate_weight * self.speech_rate_cost

    def get_time_points(self):
        """ Compute the time points for each gestures """
        t_p = []
        for idx, gest in enumerate(self.gestures):
            t_p_tmp = []
            for start, end in zip(gest.start, gest.end):
                t_p_tmp.append(start)
                t_p_tmp.append(end)
            t_p.append(t_p_tmp)

        return t_p

    def init_gesture(self):
        """ Initialization of the gestures """
        y_0 = [x.speech_ready_state for x in self.articulators]
        self.articulatory_space.initial_state = np.array(y_0).reshape(-1,1)
        art_to_tract_mapping(self.articulatory_space, self.tract_space,
                             "initial_state")
        for gest in self.gestures:
            gest.tract_space_init = self.tract_space.initial_state
            gest.art_space_init = self.articulatory_space.initial_state

    def make_anatomy(self):
        """ Create the anatomy matrix """
        self.anatomy = Anatomy("parent", self)
        self.anatomy.make_matrices(False)
        
    def resampling(self, prms, sr_new, method="tau"):
        """ regenerate trajectory at a new sampling rate """
        [[setattr(m, "sampling_frequency", sr_new) for m in a.movements] for 
         a in self.articulators]
        prms.sampling_rate = sr_new
        self.compute_trajectory(prms, method=method)
        

    def optimize(self, prms, method="etd", early_stop=False, end_mov=True):
        """ Launch the optimization process """
        weights = (prms.effort_weight,
                   prms.parsing_weight,
                   prms.duration_weight)
        solution = global_optimization(self, prms, weights, method=method,
                                        early_stop=early_stop, 
                                        end_mov=end_mov)
        self.solution_to_vt(solution, method=method, end_mov=end_mov)
        self.compute_trajectory(prms, method=method)

    def score_to_trajectory(self, prms, method="etd"):
        """ Compute the trajectory for given gestures """

        self.compute_tract_mk_matrix()
        self.init_gesture()
        self.speech_ready_space = Space("parent", self)
        self.speech_ready_space.damping = crit_damp(self.articulatory_space,
                                                           self.general_stiffness)
        self.compute_trajectory(prms, method=method)

    def solution_to_cost(self, x, prms, method="etd", end_mov=True):
        """ Return the cost function for a solution vector x """
        self.solution_to_vt(x, method=method, end_mov=end_mov)
        if method == "etd":
            self.score_to_trajectory(prms, method=method)
        else:
            self.compute_trajectory(prms, "tau")
            self.probability = phoneme_probability(self.articulatory_space.dynamics,
                                                   prms.probabilistic_model)
            
        return self.get_cost_function(prms, method=method)

    def solution_to_vt(self, x, method="etd", end_mov=True):
        """ Update the gestures and the general stiffness
        during the optimization"""

        if method == "etd":
            update_vt_etd(self, x)
        else:
            update_vt_tau(self, x, end_mov)

    def tp2activation(self, params):
        """ converts time points into activation indices """
        s_r = params.sampling_rate
        t_pp = self.get_time_points()
        t_max = max_list(t_pp)
        t_vec = get_time_vector(t_max, s_r)
        nb_gestures = len(t_pp)
        n_pts = np.zeros((nb_gestures, len(t_vec)))
        for k_gest, t_tmp in enumerate(t_pp):
            for k_pts in range(int(len(t_tmp)/2)):
                (start, end) = (np.array(t_tmp[2*k_pts:2*(k_pts+1)]) * s_r).astype(int)
                end = min(end, len(t_vec))
                if t_tmp[2*k_pts+1] == t_max:
                    end = len(t_vec)
                if end > start:
                    n_pts[k_gest, start:end] = np.ones_like(n_pts[k_gest, start:end])

        return n_pts.astype(bool), t_vec

    def update_parent(self):
        """ Assigns parent properties to the articulators and gestures """
        for arti in self.articulators:
            arti.parent = self
        for gest in self.gestures:
            gest.parent = self
