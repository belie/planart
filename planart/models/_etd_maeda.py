#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  2 14:09:54 2022

@author: benjamin
"""

from pathlib import Path
import json

def make_articulators():
    """ Default values for articulators """
    jaw = ["jaw", 0.55, 1.5, 0]
    tongue_body_position = ["tongue_body_position", 0.25, 1.5, 0]
    tongue_body_height = ["tongue_body_height", 0.25, 1.5, 0]
    tongue_tip = ["tongue_tip", 0.05, 1.5, 0]
    lip_aperture = ["lip_aperture", 0.03, 1, 0]
    lip_protrusion = ["lip_protrusion", 0.03, 1, 0]
    larynx = ["larynx", 0.03, 1, 0]

    return (jaw, tongue_body_position, tongue_body_height,tongue_tip,
            lip_aperture, lip_protrusion, larynx)

def make_gestures():
    """ Default values for gestures """
    """ keys: name,
             target,
             target_speech_ready,
             stiffness,
             occlusion,
             index """

    b = ["b", 0, 0, -0.004,
         [2, 0, 0, -1, 1],
         15, True, 2]
    d = ["d", 0.003, 0.003, 0.005,
         [-1, 0.25, 1, 0, 0],
         17.276, True, 1]
    a = ["a", 0.015, -0.009, -0.0116,
         [-2/3, 1, 0, 0, 0],
         5, False, 0]
    i = ["i", 0.015, 0.009, 0.0116,
         [-2/3, 1, 0, 0, 0],
         5, False, 0]

    return (b, d, a, i)

GESTURES = make_gestures()
ARTICULATORS = make_articulators()

def make_dict(keys, values):
    """ Returns a dictionary for gestures and articulators data"""
    output = dict()
    for key, value in zip(keys, values):
        output[key] = value

    return output

def write_etd_dictionary(jsonpath: Path) -> None:
    """ Writes gesture file for ETD """
    config_json = dict({"gestures": [],
                        "articulators": []})

    gest_keys = ["name", "boundary", "target", "target_speech_ready",
            "anatomy_vector", "stiffness", "occlusion", "index"]

    for gest_values in GESTURES:
        config_json["gestures"].append(make_dict(gest_keys, gest_values))

    art_keys = ["name", "mass", "stiffness", "speech_ready_state"]

    for art_values in ARTICULATORS:
        config_json["articulators"].append(make_dict(art_keys, art_values))

    with open(jsonpath.as_posix(), 'w') as f:
        json.dump(config_json, f, indent=4)