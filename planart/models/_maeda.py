#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  2 14:09:54 2022

@author: benjamin
"""

from pathlib import Path
import json

def make_articulators():
    """ Default values for articulators """
    jaw = ["jaw", 0.55, 1.5, 0, -3, 3]
    td_pos = ["TD position", 0.25, 1.5, 0, -3, 3]
    td_height = ["TD height", 0.25, 1.5, 0, -3, 3]
    tt_pos = ["TT position", 0.05, 1.5, 0, -3, 3]
    lip_aperture = ["lip aperture", 0.06, 1, 0, -3, 3]    
    lip_protrusion = ["lip protrusion", 0.06, 1, 0, -3, 3]
    larynx = ["larynx height", 0.1, 1, 0, -3, 3]

    return (jaw, td_pos, td_height, tt_pos, 
            lip_aperture, lip_protrusion, larynx)

ARTICULATORS = make_articulators()

def make_dict(keys, values):
    """ Returns a dictionary for gestures and articulators data"""
    output = dict()
    for key, value in zip(keys, values):
        output[key] = value

    return output

def write_maeda_dictionary(jsonpath: Path) -> None:
    """ Writes parameters of the Maeda model """
    config_json = dict({"articulators": []})
    art_keys = ["name", "mass", "stiffness", "speech_ready_state", 
                "minimal value", "maximal value"]

    for art_values in ARTICULATORS:
        config_json["articulators"].append(make_dict(art_keys, art_values))
    with open(jsonpath.as_posix(), 'w') as f:
        json.dump(config_json, f, indent=4)
