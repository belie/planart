#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  2 14:09:54 2022

@author: benjamin
"""

from pathlib import Path
import json

def make_articulators():
    """ Default values for articulators """
    jaw = ["jaw", 0.55, 1.5, 0.009]
    tongue_body = ["tongue_body", 0.25, 1.5, 0.006]
    tongue_tip = ["tongue_tip", 0.05, 1.5, -0.002]
    upper_lip = ["upper_lip", 0.03, 1, -0.004]
    lower_lip = ["lower_lip", 0.03, 1, 0.002]

    return (jaw, tongue_body, tongue_tip, upper_lip, lower_lip)

def make_gestures():
    """ Default values for gestures """
    b = ["b", 0, 0, -0.002,
         [2, 0, 0, 1, -1],
         7, True, 0]
    d = ["d", 0.001, 0, 0.003,
         [-1, 0.25, 1, 0, 0],
         3.95, True, 1]
    a = ["aa", 0.015, -0.009, -0.0116,
         [-2/3, 1, 0, 0, 0],
         5, False, 2]
    i = ["iy", 0.015, 0.009, 0.0116,
         [-2/3, 1, 0, 0, 0],
         5, False, 3]

    return (b, d, a, i)

GESTURES = make_gestures()
ARTICULATORS = make_articulators()

def make_dict(keys, values):
    """ Returns a dictionary for gestures and articulators data"""
    output = dict()
    for key, value in zip(keys, values):
        output[key] = value

    return output

def write_etd_dictionary(jsonpath: Path) -> None:
    """ Writes gesture file for ETD """
    config_json = dict({"gestures": [],
                        "articulators": []})

    gest_keys = ["name", "boundary", "target", "target_speech_ready",
            "anatomy_vector", "stiffness", "occlusion", "index"]

    for gest_values in GESTURES:
        config_json["gestures"].append(make_dict(gest_keys, gest_values))

    art_keys = ["name", "mass", "stiffness", "speech_ready_state"]

    for art_values in ARTICULATORS:
        config_json["articulators"].append(make_dict(art_keys, art_values))

    with open(jsonpath.as_posix(), 'w') as f:
        json.dump(config_json, f, indent=4)