#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr 12 18:48:21 2022

@author: benjamin
"""

import copy
import numpy as np

class Space:
    """ For storing quantities related to a specific space """

    # pylint: disable=too-many-instance-attributes
    # pylint: disable=pointless-string-statement

    name = None
    anatomy_matrix = None
    boundaries = None
    parent = None
    target_vector = None
    mass_matrix = None
    stiffness_matrix = None
    damping_matrix = None

    # Constructor method
    def __init__(self, *args):
        nargin = len(args)
        for k in range(0, nargin, 2):
            key, value = args[k:k+2]
            setattr(self, key, value)

    def copy(self):
        """ Returns a copy of the instance """
        return copy.deepcopy(self)

    def critical_damping(self):
        if (self.mass_matrix is not None and
            self.stiffness_matrix is not None):
            self.damping_matrix = 2*np.sqrt(self.mass_matrix @
                                            self.stiffness_matrix)

    def vec_reshape(self):
        if self.target_vector is not None:
            self.target_vector = self.target_vector.reshape(-1, 1)





