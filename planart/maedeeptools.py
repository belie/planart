#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug 29 11:20:34 2022

@author: benjamin
"""

import maedeep
import maedeep.dnn
import maedeep.parametric
import numpy as np
import h5py
# import tensorflow as tf
# tf.compat.v1.disable_eager_execution()


def acoustic_to_articulatory(formants):
    """ Acoustic to Articulatory mapping """
    return maedeep.dnn.acoustic_to_articulatory(reshape_input(formants, 4))
    
def acoustic_to_task(formants):
    """ Acoustic to task mapping """
    return maedeep.dnn.acoustic_to_task(reshape_input(formants, 4))

def articulatory_to_acoustic(articulators, method="dnn", dnn_model=None):
    """ Articulatory to acoustic mapping """
    check_mapping_method(method)
    
    if method == "dnn":
        if dnn_model is not None:
            model, norms = dnn_model
        else:
            model, norms = (None, None)        
        return maedeep.dnn.articulatory_to_formant(reshape_input(articulators, 7),
                                                   model=model, norms=norms)
    if method == "parametric":
        return maedeep.parametric.articulatory_to_formant(articulators)

def articulatory_to_task(articulators, method="dnn", verbosity=False):
    """ Articulatory to task mapping """
    check_mapping_method(method)
    if method == "dnn":
        return maedeep.dnn.articulatory_to_task(reshape_input(articulators, 7))
    if method == "parametric":
        return maedeep.parametric.articulatory_to_task(articulators, 
                                                       verbosity=verbosity)

def check_model(prms):
    """ Checks if the DNN model has already been loaded into prms and 
    loads DNN model if not """
    if prms.dnn_model is None:
        model, model_file = maedeep.dnn.load_mapping_model(None, 
                                               input_space="articulator",
                                               output_space="acoustic")
        with h5py.File(model_file, "r") as hf:
            norms_input = hf["normalization_factors_input"][()]
            norms_output = hf["normalization_factors_output"][()]
        norms = (norms_input, norms_output)
        prms.dnn_model = (model, norms)

def check_mapping_method(method):
    """ Check if the chosen mapping method is valid """
    if method not in ["linear", "parametric", "dnn"]:
        raise ValueError(method + """ is not a valid method. Please choose either 
                         'linear', 'parametric', or 'dnn' """)

def reshape_input(x, nb_feat):
    """ Reshapes input matrix to fit model dimension """
    if isinstance(x, list):
        x = np.array(x)
    if len(x.shape) == 1:
        x = x.reshape(1, -1)
    nb_frame, nb_feat_input = x.shape
    if nb_feat_input != nb_feat and nb_frame != nb_feat:
        raise ValueError("input matrix has not a valid size (" +
                         str(nb_frame) + "x" + str(nb_feat_input))
    if nb_feat_input != nb_feat and nb_frame == nb_feat:
        return x.T
    if nb_feat_input == nb_feat:
        return x

def task_to_acoustic(tasks, method="dnn"):
    """ Task to acoustic mapping """
    check_mapping_method(method)
    if method == "dnn":
        return maedeep.dnn.task_to_formant(reshape_input(tasks, 7))
    if method == "parametric":
        return maedeep.parametric.task_to_formant(tasks)
    
def task_to_articulatory(tasks):
    """ Task to Articulatory mapping """
    return maedeep.dnn.task_to_articulatory(reshape_input(tasks, 7))