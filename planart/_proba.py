# -*- coding: utf-8 -*-
"""
Created on Fri Oct 28 14:21:03 2022

@author: belie
"""

from .gmmtools import get_proba, open_gmm_model
from .maedeeptools import articulatory_to_acoustic
import os
import pickle
import numpy as np


class ProbaModel:
    model = None
    input_features = None
    scaler = None
    labels = None
    neutral_phoneme = None
    dictionary = None

    # Constructor method
    def __init__(self, *args):
        nargin = len(args)
        for k in range(0, nargin, 2):
            key, value = args[k:k+2]
            setattr(self, key, value)
            
    def get_neutral_phoneme(self, neutral_vector=np.zeros((7,1))):
        idx = int(np.argmax(phoneme_probability(neutral_vector, self), axis=1)[0])
        self.neutral_phoneme = self.labels[idx]
            
def create_model(model, input_features, labels=None, scaler=None, 
                 dictionary=None):
    """ Returns a ProbaModel object """
    return ProbaModel("model", model, "input_features", input_features, 
                      "scaler", scaler, "labels", labels,
                      "dictionary", dictionary)

def open_model(model_file):
    """ Opens the probabilistic model """        
    if os.path.splitext(model_file)[-1] in [".atp", ".pbm", ".ftp"]:        
        model_data = pickle.load(open(model_file, "rb"))
        if len(model_data) == 3:
            model, scaler, labels = model_data
            dictionary = None
        if len(model_data) == 4:
            model, scaler, labels, dictionary = model_data
        if os.path.splitext(model_file)[-1] in [".atp", ".pbm"]:
            return create_model(model, "articulatory", labels, scaler, 
                                dictionary)          
        else:
            return create_model(model, "acoustics", labels, scaler, 
                                dictionary)      
    elif os.path.splitext(model_file)[-1] in [".h5"] :
        return create_model(open_gmm_model(model_file), 
                            "acoustics")

def phoneme_probability(dynamics, probabilistic_model): # , phoneme_prediction=False):
    """ Generates the phoneme probability from the articulatory trajectory """
    if probabilistic_model.input_features == "acoustics":
        inputs = articulatory_to_acoustic(dynamics,
                                          dnn_model=probabilistic_model)
    elif probabilistic_model.input_features == "articulatory":
        inputs = probabilistic_model.scaler.transform(dynamics.T)
    return get_proba(inputs, model=probabilistic_model.model)
    
def scaling(dynamics, scaler):
    return (dynamics - scaler.mean_)/np.sqrt(scaler.var_)
           