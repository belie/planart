#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue May 24 09:09:11 2022

@author: benjamin
"""

from planart._inputs import (find_children, find_constituent)
from .optimtools import optimization
import numpy as np
from planart.symbolic import define_phoneme_type
from planart.utils import (normalize_duration)
import json


def get_global_weights(inputs):
    """ Returns global weights of the prosodic structure """
    """ At this stage, the weight on effort is hard-coded. We
    should allow to change it in the future """
    ae = inputs["speech_style"]["relaxed speech importance"]*0 # 3
    ap = inputs["speech_style"]["lexical contrast importance"]
    ad = inputs["relative_speech_rate"]
    return (ae, ap, ad)

def get_constituents(constituents):
    """ Returns words and syllables """
    return ([c for c  in constituents if c["type"] == "word"],
            [c for c  in constituents if c["type"] == "syllable"])

def get_indices(uptier, subtier, constituents):
    """ Assign subtier indices to a given tier  """
    up_indices = []
    for w in uptier:
        children = find_children(w, constituents)[0]
        up_indices.append([find_constituent(x, subtier) for x in children])
    return up_indices

def get_key_constituent(constituents, key):
    """ returns the key constituents """
    return [c for c in constituents if c["type"] == key]

def get_prosodic_structure(durations, constituents, normalization=True):
    """ returns the normalized syllable duration of the vocal tract """
    if isinstance(constituents, dict):
        if ('syllable' in constituents.keys() and
            'phoneme' in constituents.keys()):
            [sylls, phonemes] = [constituents[key] for
                                 key in ["syllable", "phoneme"]]
    else:
        sylls, phonemes = [get_key_constituent(constituents, key) for
                           key in ["syllable", "phoneme"]]

    ph_indices = get_indices(sylls, phonemes, constituents)
    syllable_duration = [np.sum([durations[i] for i in p]) for p in ph_indices]
    if normalization:
        syllable_duration = normalize_duration(syllable_duration)
    for s, dur in zip(sylls, syllable_duration):
        s["actual_duration"] = dur

def get_syllable_weight(syllables, min_prom=1, max_prom=3, parsing_model="arctan",
                        prosodic_importance=1, prominence_power=1):
    """ Returns weights applied to syllable constituents """
    if parsing_model == "exp":
        prominence = transform_prominence([s["prominence"] for s in syllables], 
                                          min_prom=min_prom, 
                                          max_prom=max_prom)
        return ([p for p in prominence],
                [1/s["prominence"] for s in syllables])
    elif parsing_model == "arctan":
        prominence = [s["prominence"] for s in syllables]
        # return ([p**prominence_power*prosodic_importance for p in prominence],
        #         [1/(p**prominence_power*prosodic_importance) for
        #           p in prominence])
        return ([p**prominence_power*prosodic_importance for p in prominence],
                [1 for
                  p in prominence])

def get_word_weights(words):
    """ Returns weights applied to word constituents """
    return ([w["prominence"] for w in words],
            [1*(w["prominence"]>1) for w in words])

def individual_difference(syllable):
    """ returns the individual prosodic difference of one syllable """
    return np.abs(syllable["target_duration"] - syllable["actual_duration"])

def prosodic_difference(constituents, order=2):
    """ Returns the mean prosodic difference """
    if order is None:
        order = 2
    syllables = get_key_constituent(constituents, "syllable")
    return np.mean([individual_difference(s)**order for s in syllables])**(1/order)

def prosodic_timing(constituents):
    syllables = get_key_constituent(constituents, "syllable")
    durations, weights = [[s[key] for s in syllables] for 
                          key in ["actual_duration", "weights"]]
    return (np.array(durations), 
            np.array([w["parsing duration"] for w in weights]))

def run_optimization(inputs, min_prom=1, max_prom=3, parsing_model="arctan",
                     prominence_power=1, c=500, randomize=False, 
                     init_duration=0.25):
    """ Launches optimization from inputs """
    
    global_weights = get_global_weights(inputs)
    words, syllables = get_constituents(inputs["constituents"])
    word_indices = get_indices(words, syllables, inputs["constituents"])
    prominences = [s["prominence"] for s in syllables]
    prominence_importance = inputs["speech_style"]["prosodic structure importance"]
    (syll_parsing_weight, 
     syll_effort_weight) = get_syllable_weight(syllables, 
                                               np.min(prominences), 
                                               np.max(prominences),
                                               parsing_model,
                                               prominence_importance,
                                               prominence_power)
    words_parsing_weight, words_parsing_strength = get_word_weights(words)
    return optimization(syll_parsing_weight, syll_effort_weight,
                           words_parsing_weight, words_parsing_strength,
                 global_weights, word_indices, init_duration=init_duration,
                 randomize=randomize, parsing_model=parsing_model, 
                 c=c)

def transform_prominence(prominence, min_prom=1, max_prom=3):
    """ Transforms prominence for normalization between min_prom 
    and max_prom """
    delta_prominence = max_prom - min_prom
    if delta_prominence == 0:
        return np.array([4 for p in prominence])
    else:
        return np.array([4 - 2*(p-min_prom)/delta_prominence for p in prominence])

def write_initial_score(solution, constituents, output_score, method="etd"):
    """ Creates the initial gestural score """
    json_dict = dict()
    json_dict["phoneme"] = []
    json_dict["syllable"] = []
    syllables, phonemes = [get_key_constituent(constituents, key) for
                          key in ["syllable", "phoneme"]]
    children = get_indices(syllables, phonemes, constituents)
    curr_syllable_start = 0
    for n in range(len(solution)):
        curr_ph = [phonemes[i] for i in children[n]]
        curr_phoneme_start = 0
        curr_syll_dict = {"name": syllables[n]["label"],
                      "start": curr_syllable_start,
                      "duration": solution[n], 
                      "weights": syllables[n]["weights"]}
        for ph in curr_ph:
            if method == "tau":                
                duration = solution[n]/len(curr_ph)       
                start = curr_syllable_start + curr_phoneme_start
            else: 
                if define_phoneme_type(ph["label"]) == "vowel":
                    start = curr_syllable_start
                    duration = solution[n]
                else:
                    start = curr_syllable_start + curr_phoneme_start
                    duration = solution[n] / 2 / (len(curr_ph) - 1)
            
            curr_phoneme_start += duration
            curr_dict = {"name": ph["label"],
                         "start": start,
                         "duration": duration, 
                         "weights": ph["weights"]}
            json_dict["phoneme"].append(curr_dict)
        curr_syllable_start += solution[n]
        json_dict["syllable"].append(curr_syll_dict)

    with open(output_score, "w") as f:
        json.dump(json_dict, f, indent=4)