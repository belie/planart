#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  2 15:46:48 2022

@author: benjamin
"""

import numpy as np
from .utils import psinv

class Anatomy:
    """ Anatomy is a class to store with anatomy matrices"""

    # pylint: disable=too-many-instance-attributes
    # pylint: disable=pointless-string-statement

    general_matrix = None
    boundaries = None
    pseudo_matrices = None
    inverse_matrices = None
    pseudo_inverse_matrix = None
    parent = None

    # Constructor method
    def __init__(self, *args):
        nargin = len(args)
        for k in range(0, nargin, 2):
            key, value = args[k:k+2]
            setattr(self, key, value)

    def make_anatomy_matrix(self, inversion=True):
        """ Make the anatomy matrix from gestures """

        nb_gestures = len(self.parent.gestures)
        nb_articulators = len(self.parent.gestures[0].anatomy_vector)
        self.general_matrix = np.zeros((nb_gestures, nb_articulators))
        self.boundaries = np.zeros(nb_gestures)
        for n, g in enumerate(self.parent.gestures):
            self.general_matrix[n, :] = g.anatomy_vector
            self.boundaries[n] = g.boundary
        if inversion:
            self.general_matrix = self.general_matrix[2::-1,:]
            self.boundaries = self.boundaries[2::-1].reshape(-1, 1)

    def make_matrices(self, inversion=True):

        self.make_anatomy_matrix(inversion)
        self.pseudo_matrices = (np.diag([3, 1, 1, 3, 3]) @
                                self.parent.articulatory_space.mass,
                                np.diag([3, 1, 1, 3, 3]))
        self.inverse_matrices = [np.linalg.inv(x) for x in self.pseudo_matrices]
        self.pseudo_inverse_matrix = psinv(self.general_matrix,
                                               self.inverse_matrices[1],
                                               inversion=False)

class Space:
    """ Space is a class to store mass, stiffness and damping matrices
    in each space """

    # pylint: disable=too-many-instance-attributes
    # pylint: disable=pointless-string-statement

    stiffness = None
    mass = None
    inverse_mass = None
    damping = None
    dynamics = None
    initial_state = None
    parent = None
    target = None

    # Constructor method
    def __init__(self, *args):
        nargin = len(args)
        for k in range(0, nargin, 2):
            key, value = args[k:k+2]
            setattr(self, key, value)

    def apply_general_stiffness(self, k):
        """ Applies general stiffness to the active task stiffness matrix """
        self.stiffness *= k

    def get_stiffness(self, inversion=True):

        self.stiffness = np.diag(np.zeros(len(self.parent.gestures)))
        for n in range(len(self.parent.gestures)):
            self.stiffness[n, n] = self.parent.gestures[n].stiffness
        if inversion:
            self.stiffness = self.stiffness[2::-1, 2::-1]

    def get_target(self, inversion=True, speech_ready=True):

        self.target = np.zeros(len(self.parent.gestures))
        for n in range(len(self.parent.gestures)):
            if speech_ready:
                self.target[n] = self.parent.gestures[n].target_speech_ready
            else:
                self.target[n] = self.parent.gestures[n].target
        if inversion:
            self.target =self.target[2::-1]
