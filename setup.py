#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 30 07:53:09 2021

@author: benjamin
"""

from setuptools import setup, find_packages

setup(name='planart',
      version='0.0.5',
      description='Computational platforms for models of speech articulatory planning',
      url=' https://gitlab.com/benjamin.elie/planart',
      author='Benjamin Elie',
      author_email='benjamin.elie@ed.ac.uk',
      license='Creative Commons Attribution 4.0 International License',
      packages=find_packages(),
      install_requires=[
        "numpy",
        "click",
        "h5py", 
        "pytest",
        "scipy",
        "scikit-learn",
#        "pyqt5",
#        "pyqtgraph",
       "maedeep>=0.1.2",
       "spychhiker",
	"tensorflow",
# 	 "tauspeech"
    ],
      entry_points={
          'console_scripts': [
              'planart=planart.cli:cli_cmd',
              'planart_analysis=planart.analysis.cli:cli_analysis_cmd'
          ],
      },
      zip_safe=False)