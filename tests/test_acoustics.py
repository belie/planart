import os
from conftest import create_ini_for_test
import shutil
from click.testing import CliRunner
from planart import cli
from planart import utils

def test_acoustics(tmpdir, datapath, initfile_tmp):

    tmp_files = ["solution.json", "speech_audio.wav"]   
    runner = CliRunner()
    with runner.isolated_filesystem():   
        
        runner.invoke(cli.init_settings, ["test_tmp.ini"])
        create_ini_for_test("test_tmp.ini", method="xt/3c")        
        shutil.copyfile(os.path.join(datapath, "solution_a.json"), 
                        "solution.json")    
        
        result = runner.invoke(cli.run, ["-macoustics", "test_tmp.ini"])    
        assert result.exit_code == 0
        assert os.path.isfile(tmp_files[1])
    
        for tmp_file in tmp_files:
            if os.path.isfile(tmp_file):
                os.remove(tmp_file)
            
        if os.path.isfile(initfile_tmp.as_posix()):
            os.remove(initfile_tmp.as_posix())