from planart import _gesture
from planart import etdtools
import numpy as np

def test_init(etdfile, score):

    gestures = etdfile["gestures"]
    start, duration, end = score

    gests = etdtools.group_gestures(gestures, (start, duration, end), None)
    assert len(gests) == len(gestures)
    b, d, a, i = gests
    assert d.duration == [-1]
    assert a.duration == [0.5]

def test_expand():
    nb_frame = 10
    a = _gesture.Gesture("target", np.array([0, 1, 0]))
    target = a.expand_gesture(nb_frame)
    assert target.shape == (3,10)
    assert np.sum(np.diff(target)) == 0

def test_get_index():
    a = _gesture.Gesture("target", np.array([0, 1, 0]))
    a.get_index()
    assert a.index == 1

def test_parsing(parameters, vocaltract):
    nb_frame = 10
    a = _gesture.Gesture("target", 1,
                         "parent", vocaltract,
                         "occlusion", False,
                         "index", 1,
                         "weights", 
                         [{"parsing_precision": 1, 
                           "parsing_duration": 1,
                           "effort": 1}],
                         "effective_start", [0],
                         "effective_duration", [0.1],
                         "start", [0])

    a.tract_function = [np.random.randn(4, nb_frame)]
    a.tract_space_init =  [0, a.target + 1, 0, 0]
    a.compute_parsing_cost(parameters)
    assert a.parsing_cost >= 0 and a.parsing_cost <= 1

def test_effort(parameters):
    nb_frame = 10
    a = _gesture.Gesture("target", np.array([0, 1, 0]),
                         "weights",
                         [{"parsing_precision": 1, 
                           "parsing_duration": 1,
                           "effort": 1}])
    a.get_index()
    target = a.expand_gesture(nb_frame)
    a.tract_function = target
    a.tract_space_init = a.target + 1
    a.compute_force(parameters)
    assert a.articulatory_effort == 0
