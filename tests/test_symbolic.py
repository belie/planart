import os
from conftest import create_ini_for_test
import json
import random
import planart as pa
import shutil
from click.testing import CliRunner
from planart import cli
from planart import utils

def test_input(datapath):
    tmp_file = os.path.join(datapath, "utterance.json")
    
    with open(tmp_file, "r") as f:
        data = json.load(f)["inputs"]

    constituents = data["constituents"]
    assert isinstance(data["relative_speech_rate"], float)

    style_keys = ['lexical contrast importance',
      'relative speech rate importance',
      'prosodic structure importance']
    for key in style_keys:
        assert key in list(data["speech_style"].keys())

    random.shuffle(constituents)
    constituent_sorted, types = pa.sort_all_constituents(constituents)
    labels = " ".join([l["label"] for l in constituent_sorted[-1]])
    assert labels == "h @ l oU w 3 r l d"

def test_symbolic_weights(tmpdir, datapath, initfile_tmp):
    tmp_files = ["symbolic_weights.json", "symbolic_cues.json"]
    runner = CliRunner()
    with runner.isolated_filesystem():   
        
        runner.invoke(cli.init_settings, ["test_tmp.ini"])
        with open('globalparameters.json', 'r') as f:
            t = json.load(f)
        print(t['timing'].keys(), flush=True)
        shutil.copyfile(os.path.join(datapath, "utterance.json"), 
                        "utterance.json")    
        result = runner.invoke(cli.run, ["-msymbolic", "test_tmp.ini"])    
        assert result.exit_code == 0
        assert os.path.isfile(tmp_files[1])
        with open(tmp_files[0], "r") as file_id:
            constituents = json.load(file_id)["inputs"]["constituents"]
    
        assert len(constituents) == 15
        assert constituents[0]["label"] == "Hello, world!"
        assert "weights" in constituents[0].keys()
        assert constituents[0]["weights"]["duration"] != 0
        assert constituents[-1]["label"] == "d"

        for tmp_file in tmp_files:
            if os.path.isfile(tmp_file):
                os.remove(tmp_file)
                
        if os.path.isfile(initfile_tmp.as_posix()):
            os.remove(initfile_tmp.as_posix())

def test_symbolic_cues(tmpdir, datapath, initfile_tmp):
    
    tmp_files = ["symbolic_weights.json", "symbolic_cues.json"]
    runner = CliRunner()
    with runner.isolated_filesystem():   
        
        runner.invoke(cli.init_settings, ["test_tmp.ini"])    
        shutil.copyfile(os.path.join(datapath, "utterance.json"), 
                        "utterance.json")    
        result = runner.invoke(cli.run, ["-msymbolic", "test_tmp.ini"])    
        assert result.exit_code == 0
        assert os.path.isfile(tmp_files[0])        
        with open(tmp_files[1], "r") as file_id:
            constituents = json.load(file_id)["constituents"]
    
        assert len(constituents) == 9
        assert constituents[0]["label"] == "h"
        assert "landmarks" in constituents[0].keys()
        assert constituents[-1]["type"] == "voiced_stop"
        assert len(constituents[-1]["landmarks"]) == 3
    
        for tmp_file in tmp_files:
            if os.path.isfile(tmp_file):
                os.remove(tmp_file)
        if os.path.isfile(initfile_tmp.as_posix()):
            os.remove(initfile_tmp.as_posix())


