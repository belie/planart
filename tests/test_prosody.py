import json
import planart.prosody

def test_input(tmpdir):
    tmp_file = "data/utterance.json"

    with open(tmp_file, "r") as f:
        inputs = json.load(f)["inputs"]

    nb_syll = len([c for c in inputs["constituents"] if c["type"] == "syllable"])
    solution = planart.prosody.run_optimization(inputs)
    assert len(solution) == nb_syll
    assert min(solution) >= 0