from planart import maedeeptools
import numpy as np


def check_size(x, nb_feat, nb_frame, xmin, xmax):
    assert x.shape == (nb_feat, nb_frame)
    assert np.min(x) >= xmin
    assert np.max(x) <= xmax
    
# def test_articulatory_to_acoustic_dnn(maeda):
#     n = maeda.shape[0]
#     formants = maedeeptools.articulatory_to_acoustic(maeda).T
#     check_size(formants, 4, n, 0, 5000)
    
def test_articulatory_to_acoustic_parametric(maeda):
    n = maeda.shape[0]
    formants = maedeeptools.articulatory_to_acoustic(maeda.T, method="parametric")
    check_size(formants, 4, n, 0, 5000)
    
# def test_articulatory_to_task_dnn(maeda):
#     n = maeda.shape[0]
#     tasks = maedeeptools.articulatory_to_task(maeda).T
#     check_size(tasks, 7, n, 0, 180)
    
def test_articulatory_to_task_parametric(maeda):
    n = maeda.shape[0]
    tasks = maedeeptools.articulatory_to_task(maeda.T,  method="parametric")
    check_size(tasks, 7, n, 0, 180)
    
# def test_acoustic_to_articulatory(formants):
#     n = len(formants)
#     articulators = maedeeptools.acoustic_to_articulatory(formants).T
#     check_size(articulators, 7, n, -3, 3)