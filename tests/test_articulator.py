# from planart import _articulator
from planart import etdtools

def test_init(etdfile):
    articulators = etdfile["articulators"]
    arts = etdtools.group_articulators(etdfile["articulators"])

    assert len(arts) == len(articulators)
    jaw = arts[0]
    for key in ["mass", "stiffness", "speech_ready_state", "name"]:
        assert getattr(jaw, key) == articulators[0][key]
