import os
from pathlib import Path
import tempfile
import pytest
from planart import (cli,
                     _parameters,
                     utils,
                     _config,
                     etdtools,
                     _vt)

import configparser
from click.testing import CliRunner
import numpy as np
import shutil

def create_symbolic_output(datapath):
    """ Creates symbolic output for testing optimization procedures """
    runner = CliRunner()
    with runner.isolated_filesystem():        
        runner.invoke(cli.init_settings, ["test_tmp.ini"])  
        runner.invoke(cli.run, ["-msymbolic", "test_tmp.ini"])   
        [shutil.copyfile(f, os.path.join(datapath, f)) for 
         f in ["symbolic_weights.json", "symbolic_cues.json"]]
        

def create_ini_for_test(initfile_tmp, method):
    """ Create a test_optimi.ini for testing optimization procedures (makes it faster) """
    fpath = Path(initfile_tmp).absolute()
    projectdir = fpath.parent

    def filepath(name: str) -> str:
        return (projectdir / name).as_posix()
    
    config = configparser.ConfigParser(strict=True)
    config['symbolic'] = {
        'utterance': filepath('utterance.json'),
    }
    config['dynamic'] = {
        'method': 'optimization'
    }
    config['optimization'] = {
        'method': method,
        'dictionary': filepath('gesture_dictionary.json'),
        'probabilistic_model': filepath('english_alpha_0_layers_100_50_40_200_25_batch_size_1024.atp'),
        'parameters': filepath('globalparameters.json'),
        'intensity_model': filepath('intensity_000'),
    }
    config['motor-sensory'] = {
        'method': 'acoustics'
    }
    config['acoustics'] = {
        'parameters': filepath('globalparameters.json'),
    }
    with fpath.open('w') as f:
        config.write(f)

@pytest.fixture
def home():
    """Set and return $HOME to a temporary directory."""
    with tempfile.TemporaryDirectory() as temphome:
        if os.name == "nt":
            home_name = "USERPROFILE"
        else:
            home_name = "HOME"
        old_home = os.environ.get(home_name)
        os.environ[home_name] = temphome
        
        try:
            yield temphome
        finally:
            if old_home:
                os.environ[home_name] = old_home

@pytest.fixture
def datapath():
    """Return the path to data directory alongside tests."""
    out_dir = _config.get_planart_path() / 'tests' / 'data'
    if not os.path.isdir(out_dir):
        os.mkdir(out_dir)
    return out_dir

@pytest.fixture
def initfile():
    """Return the path to ini file."""
    return _config.get_planart_path() / 'tests' / 'data' / 'test.ini'

@pytest.fixture
def initfile_tmp():
    """Return the path to ini file."""
    return _config.get_planart_path() / 'tests' / 'data' / 'test_tmp.ini'

@pytest.fixture
def acousticmodel():
    """Return the path to ini file."""
    return _config.get_planart_path() / 'models' / 'probabilistic' / 'gmm'

@pytest.fixture
def articulators(etdfile):
    return etdtools.group_articulators(etdfile["articulators"])

@pytest.fixture
def gestures(etdfile, score):
    gestures = etdfile["gestures"]
    start, duration, end = score
    return etdtools.group_gestures(gestures, (start, duration, end), None)

@pytest.fixture
def parameters(jsonparameters):
    prms = _parameters.Parameters()
    for key in jsonparameters.keys():
        setattr(prms, key, jsonparameters[key])
    return prms

@pytest.fixture
def vocaltract(gestures, articulators):
    vocal_tract = _vt.VT("articulators", articulators,
                         "general_stiffness", 10,
                     "gestures", gestures)

    vocal_tract.compute_art_mk_matrix()
    vocal_tract.make_anatomy()
    return vocal_tract

@pytest.fixture
def jsonparameters():
    config_file = Path(__file__).parent / "data" / "globalparameters.json"
    return utils.read_json(config_file)["etd"]

@pytest.fixture
def etdfile():
    config_file = Path(__file__).parent / "data" / "etd.json"
    return utils.read_json(config_file)

@pytest.fixture
def formants():
    n = np.random.randint(2, 10)
    return [[500, 1500, 2500, 3500]]*n

@pytest.fixture
def maeda():
    n = np.random.randint(2, 10)
    return np.array([[0]*7]*n)

@pytest.fixture
def score(etdfile):
    score_file = Path(__file__).parent / "data" / "score.json"
    gestures = etdfile["gestures"]
    return utils.read_score(score_file, gestures)
