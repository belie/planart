from planart import _config


def test_modules(tmpdir, datapath, initfile):
    settings =_config.read_config(initfile)
    assert "symbolic" in settings.sections()
    assert "dynamic" in settings.sections()
    assert "optimization" in settings.sections()

def test_symbolic(tmpdir, datapath, initfile):
    settings =_config.read_config(initfile)
    for key in settings["symbolic"]:
        assert key in ["utterance", "parameters"]

def test_dynamic(tmpdir, datapath, initfile):
    settings =_config.read_config(initfile)
    for key in settings["dynamic"]:
        assert key in ["method"]

def test_optimization(tmpdir, datapath, initfile):
    settings =_config.read_config(initfile)
    for key in settings["optimization"]:
        assert key in ["parameters", "method",
                       "dictionary", "probabilistic_model",
                       "intensity_model"]
        
def test_motor_sensory(tmpdir, datapath, initfile):
    settings =_config.read_config(initfile)
    for key in settings["motor-sensory"]:
        assert key in ["method"]

def test_acoustics(tmpdir, datapath, initfile):
    settings =_config.read_config(initfile)
    for key in settings["acoustics"]:
        assert key in ["parameters"]