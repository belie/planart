import configparser
import pathlib
import json

from click.testing import CliRunner

from planart import _config
from planart import cli
import os
# from conftest import create_ini_for_test

def test_init(home):    
    runner = CliRunner()
    result = runner.invoke(cli.init_config)
    assert result.exit_code == 0
    config = _config.GlobalConfig()
    assert config.file_path().exists()
    assert config.json_path().exists()
    config.load()
    assert 'symbolic' in config.sections()
    assert 'optimization' in config.sections()
    assert 'scripts' in config.sections()

    with open(config.json_path(), "r") as file_id:
        data = json.load(file_id)
    assert 'etd' in data.keys()
    assert 'sampling_rate' in data['etd'].keys()
    
def _only_final_path_component(options):
    for key, value in options.items():
        name = pathlib.Path(value).name
        options[key] = name

def test_new(home, tmpdir, datapath, initfile):
    runner = CliRunner()
    settings_file = tmpdir.join('test_tmp.ini')
    result = runner.invoke(cli.init_settings, [str(settings_file)])
    assert result.exit_code == 0
    assert settings_file.exists()
    settings = _config.read_config(str(settings_file))
    expected = configparser.ConfigParser(strict=True)
    with (settings_file).open() as f:
        expected.read_file(f)
    assert settings.sections() == expected.sections()
    for section, expected_values in expected.items():
        options = dict(settings[section].items())
        expected_options = dict(expected_values.items())
        _only_final_path_component(options)
        _only_final_path_component(expected_options)
        assert options == expected_options
        
    if os.path.isfile(settings_file):
        os.remove(settings_file)
        
def test_init_utt(tmpdir):
    runner = CliRunner()
    settings_file = tmpdir.join('test_utterance.json')
    result = runner.invoke(cli.init_utterance, [str(settings_file), 
                                                "-utt=adi:(prom=200)",
                                                "-wadi:(prom=3,par=sentence_1)",
                                                "-syl=a:(prom=5,par=word_1)",
                                                "-syl=di:(prom=10,par=word_1,sib=word_1)",
                                                "-paa:(prom=1,par=syllable_1)",
                                                "-pd:(prom=5,par=syllable_2,sib=phoneme_1)",
                                                "-piy:(prom=1,par=syllable_2,sib=phoneme_2)",
                                                "-sty=lex=2,pros=10",
                                                "-sr=10"
                                                ])
    assert result.exit_code == 0
    assert settings_file.exists()
    with open(settings_file, 'r') as f:
        settings = json.load(f)
    assert 'inputs' in settings.keys()
    for key in ['constituents', 'speech_style', 'relative_speech_rate']:
        assert key in settings['inputs'].keys()
    assert isinstance(settings['inputs']['relative_speech_rate'], float)
    assert settings['inputs']['relative_speech_rate'] == 10
    assert isinstance(settings['inputs']['speech_style']["relative speech rate importance"], float)
    assert isinstance(settings['inputs']['constituents'][0]['type'], str)
    assert isinstance(settings['inputs']['constituents'][0]['label'], str)
    assert settings['inputs']['constituents'][0]['label'] == 'adi'
    
def test_init_utt_vowel(tmpdir):
    runner = CliRunner()
    settings_file = tmpdir.join('test_utterance.json')
    result = runner.invoke(cli.init_utterance, [str(settings_file), "-viy"])
    assert result.exit_code == 0
    assert settings_file.exists()
    with open(settings_file, 'r') as f:
        settings = json.load(f)['inputs']['constituents']
    assert settings[0]['label'] == 'iy'
    
def test_modif_param(tmpdir):
    runner = CliRunner()
    settings_file = tmpdir.join('test_tmp.ini')
    param_file = tmpdir.join('globalparameters.json')
    runner.invoke(cli.init_settings, [str(settings_file)])
    result = runner.invoke(cli.param_modif, [str(settings_file), 
                                                "-poptimization:(attempts=1000)"])
    assert result.exit_code == 0
    with open(param_file, 'r') as f:
        settings = json.load(f)['optimization']['attempts']    
    assert settings == 1000
            
config_fpath = _config.GlobalConfig.file_path()
if not config_fpath.exists():
    os.system("planart init-config")
    
