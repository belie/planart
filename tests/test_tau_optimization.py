import os
import json
from conftest import create_ini_for_test
import shutil
from click.testing import CliRunner
from planart import cli
from planart import utils

def test_tau(tmpdir, datapath, initfile_tmp):
    
    tmp_files = ["solution.json", "dynamics.h5",
                  "symbolic_weights.json", "symbolic_cues.json"]    
    
    # create_symbolic_output(datapath)
    runner = CliRunner()
    with runner.isolated_filesystem():   
        
        runner.invoke(cli.init_settings, ["test_tmp.ini"])
        create_ini_for_test("test_tmp.ini", method="xt/3c")
        utils.change_parameters('globalparameters.json', ["early_stop",
                                                    "max_iterations"], 
                                                  [1, 2], level='optimization')  
        shutil.copyfile(os.path.join(datapath, "symbolic_cues.json"), 
                        "symbolic_cues.json")
        shutil.copyfile(os.path.join(datapath, "symbolic_weights.json"), 
                        "symbolic_weights.json")    
        
        result = runner.invoke(cli.run, ["-moptimization", "test_tmp.ini"])    
        assert result.exit_code == 0    
        for tmp_file in tmp_files[:2]:
            assert os.path.isfile(tmp_file)

        with open(tmp_files[0], "r") as file_id:
            res = json.load(file_id)
        for key in ["static model", "dynamic model", "solution"]:
            assert key in res.keys()
        assert "name" in res["solution"][0].keys()
        assert res["dynamic model"] == "tau"
        
        for tmp_file in tmp_files:
            if os.path.isfile(tmp_file):
                os.remove(tmp_file)
            
        if os.path.isfile(initfile_tmp.as_posix()):
            os.remove(initfile_tmp.as_posix())