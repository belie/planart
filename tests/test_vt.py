from planart import _vt
import numpy as np

def check_diagonal(matrix):
    return np.count_nonzero(matrix - np.diag(np.diagonal(matrix))) == 0

def test_init():
    nb_articulators = 3
    nb_gestures = 10
    articulators = "a" * nb_articulators
    gestures = "g" * nb_gestures
    vocal_tract = _vt.VT("articulators", articulators,
                         "gestures", gestures)
    assert len(vocal_tract.articulators) == nb_articulators
    assert len(vocal_tract.gestures) == nb_gestures

def test_art_matrix(articulators):
    matrix_size = (len(articulators), len(articulators))
    vocal_tract = _vt.VT("articulators", articulators, "general_stiffness", 1)
    vocal_tract.compute_art_mk_matrix()
    assert vocal_tract.articulatory_space.stiffness.shape == matrix_size
    assert vocal_tract.articulatory_space.mass.shape == matrix_size

def test_update_parent(articulators, gestures):
    vocal_tract = _vt.VT("articulators", articulators,
                         "gestures", gestures,
                         "general_stiffness", 1)
    vocal_tract.update_parent()
    assert vocal_tract.articulators[0].parent is vocal_tract
    assert vocal_tract.gestures[0].parent is vocal_tract

def test_init_gesture(articulators, gestures):
    vocal_tract = _vt.VT("articulators", articulators,
                         "gestures", gestures,
                         "general_stiffness", 1)
    vocal_tract.compute_art_mk_matrix()
    vocal_tract.make_anatomy()
    tract_dim = vocal_tract.anatomy.general_matrix.shape[0]
    vocal_tract.update_parent()
    vocal_tract.compute_tract_mk_matrix()
    vocal_tract.init_gesture()
    assert vocal_tract.tract_space.initial_state.shape[0] == tract_dim
    for gest in vocal_tract.gestures:
        assert (gest.tract_space_init == vocal_tract.tract_space.initial_state).all()
        assert (gest.art_space_init == vocal_tract.articulatory_space.initial_state).all()

def test_tract_matrix(articulators, gestures):
    matrix_size = (4, 4)
    vocal_tract = _vt.VT("articulators", articulators,
                         "gestures", gestures,
                         "general_stiffness", 1)
    vocal_tract.compute_art_mk_matrix()
    vocal_tract.make_anatomy()
    vocal_tract.update_parent()
    vocal_tract.compute_tract_mk_matrix()
    vocal_tract.init_gesture()
    assert vocal_tract.tract_space.stiffness.shape == matrix_size
    assert vocal_tract.tract_space.mass.shape == matrix_size
    assert check_diagonal(vocal_tract.tract_space.stiffness)