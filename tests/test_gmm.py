from sklearn.mixture import GaussianMixture
import os

from planart import gmmtools
from planart import _config

expected_nb_components = 68

def test_select_model_default(acousticmodel):
    model_file = gmmtools.select_model()
    assert model_file ==  os.path.join(_config.GlobalConfig.acoustic_models_path().as_posix(), 
                                       "probabilistic", "gmm", "english.h5")
    
def test_select_model(acousticmodel):    
    model_file = gmmtools.select_model("english", acousticmodel)
    assert model_file ==  os.path.join(acousticmodel, "english.h5")

def test_open_gmm():
    gmm = gmmtools.open_gmm_model(gmmtools.select_model())
    assert isinstance(gmm, GaussianMixture)
    assert gmm.n_components == expected_nb_components
    
def test_proba(formants):
    n = len(formants)
    proba = gmmtools.get_proba(formants)
    assert proba.shape == (n, expected_nb_components)
    assert max(proba.reshape(-1)) <= 1 and min(proba.reshape(-1)) >= 0   