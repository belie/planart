import os
import json
from conftest import create_ini_for_test, create_symbolic_output
import shutil
from click.testing import CliRunner
from planart import cli
from planart import utils

       
def test_etd(tmpdir, datapath, initfile_tmp):
    
    tmp_files = ["final_score.json", "dynamics.h5",
                  "symbolic_weights.json", "symbolic_cues.json"]    
    
    # create_symbolic_output(datapath)
    runner = CliRunner()
    with runner.isolated_filesystem():   
        
        runner.invoke(cli.init_settings, ["test_tmp.ini"])
        create_ini_for_test("test_tmp.ini", method="etd")
        utils.change_parameters('globalparameters.json', ["early_stop",
                                                    "max_iterations"], 
                                                  [1, 2], level='optimization')  
        shutil.copyfile(os.path.join(datapath, "symbolic_cues.json"), 
                        "symbolic_cues.json")
        shutil.copyfile(os.path.join(datapath, "symbolic_weights.json"), 
                        "symbolic_weights.json")    
        
        result = runner.invoke(cli.run, ["-moptimization", "test_tmp.ini"])    
        assert result.exit_code == 0    
        for tmp_file in tmp_files[:2]:
            assert os.path.isfile(tmp_file)
    
        with open(tmp_files[0], "r") as file_id:
            res = json.load(file_id)
        assert "phoneme" in res.keys()
        assert "name" in res["phoneme"][0].keys()
        assert "start" in res["phoneme"][0].keys()
        assert "duration" in res["phoneme"][0].keys()
        
        for tmp_file in tmp_files:
            if os.path.isfile(tmp_file):
                os.remove(tmp_file)
            
        if os.path.isfile(initfile_tmp.as_posix()):
            os.remove(initfile_tmp.as_posix())