#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 30 07:53:09 2021

@author: benjamin
"""

import argparse
import planart
import planart.symbolic
import os

parser = argparse.ArgumentParser(description="Symbolic planning")
parser.add_argument("utterance_file", metavar="utterance file", 
                    type=str, help="path to the utterance file")
parser.add_argument("config_file", metavar="config file", 
                    type=str, help="path to the config file")

utterance_file = parser.parse_args().utterance_file
config_file = parser.parse_args().config_file

json_dict = planart.read_json(utterance_file)
prms = planart.Parameters()
params = planart.read_json(config_file)

[planart.set_configs(params, key, prms) for key in ["timing"]]
inputs = json_dict["inputs"]
constituents = inputs["constituents"]

output_weights = planart.symbolic.create_output_weight_dict(json_dict, 
                                                            prms.prominence_power)
weights_file = os.path.join(planart.workspace().as_posix(), 
                            "symbolic_weights.json")
planart.write_json(output_weights, weights_file)

output_cues = planart.symbolic.create_output_cues_dict(constituents)
cues_file = os.path.join(planart.workspace().as_posix(), "symbolic_cues.json")
planart.write_json(output_cues, cues_file)
