#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 30 07:53:09 2021

@author: benjamin
"""

import planart as pa
import planart.prosody
import argparse
import os

##### Read input arguments #####

parser = argparse.ArgumentParser(description="""Launch optimization for 
                                 articulatory planning""")
parser.add_argument("method", metavar="method", type=str,
                    help="optimization method (etd or tau)")
parser.add_argument("config_file", metavar="config", type=str,
                    help="path to the configuration file (JSON format)")
parser.add_argument("gesture_dictionary", metavar="gesture_dictionary", 
                    type=str,
                    help="path to the gesture dicationary (JSON format)")
parser.add_argument("proba_model", metavar="proba_model", type=str,
                    help="path to the probabilistic model (HDF5 format)")
parser.add_argument("intensity_model", metavar="intensity_model", type=str,
                    help="path to the folder containing the intensity model")
parser.add_argument("cues", metavar="cues", type=str,
                    help="path to the acoustic cues (JSON format)")
parser.add_argument("weights", metavar="weights", type=str,
                    help="path to the cost function weights (JSON format)")

method = parser.parse_args().method
config_file = parser.parse_args().config_file
gesture_file = parser.parse_args().gesture_dictionary
proba_model = parser.parse_args().proba_model
cues_file = parser.parse_args().cues
weights_file = parser.parse_args().weights
intensity_model = parser.parse_args().intensity_model


if method == "etd":
    files = " ".join([config_file, gesture_file, cues_file, weights_file])
    script_file = pa.get_planart_path() / 'scripts' / 'launch_etd.py'
elif method == "xt/3c":
    files = " ".join([config_file, gesture_file, proba_model, intensity_model,
                      cues_file, weights_file])
    script_file = pa.get_planart_path() / 'scripts' / 'launch_tau.py'
else:
    raise ValueError(method + " is not a valid method. Choose either " + 
                     "'xt/3c' or 'etd'")
os.system("python " + script_file.as_posix() + " " + files)