#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 30 07:53:09 2021

@author: benjamin
"""

import planart as pa
import planart.prosody
import argparse
import os
import numpy as np
import time

start_time = time.time()

##### Read input arguments #####

parser = argparse.ArgumentParser(description="Speech articulatory optimization " +
                                  "using general Tau theory")
parser.add_argument("config_file", metavar="config", type=str,
                    help="path to the configuration file (JSON format)")
parser.add_argument("gesture_dictionary", metavar="gesture_dictionary", type=str,
                    help="path to the gesture dicationary (JSON format)")
parser.add_argument("proba_model", metavar="proba_model", type=str,
                    help="path to the probabilistic model (HDF5 format)")
parser.add_argument("intensity_model", metavar="intensity_model", type=str,
                    help="path to the folder containing the intensity model")
parser.add_argument("cues", metavar="cues", type=str,
                    help="path to the acoustic cues (JSON format)")
parser.add_argument("weights", metavar="weights", type=str,
                    help="path to the cost function weights (JSON format)")

config_file = parser.parse_args().config_file
gesture_dictionary = parser.parse_args().gesture_dictionary
proba_model = parser.parse_args().proba_model
intensity_model = parser.parse_args().intensity_model
cues_file = parser.parse_args().cues
weights_file = parser.parse_args().weights
maeda_file = pa.GlobalConfig.maeda_path()

non_vocalic = ['p', 'S', 't', 'k', 's', 'f', "#"]

##### Check if input files exist #####
exist = [pa.check_file_exist(file) for file in [config_file,
                                        gesture_dictionary,
                                        cues_file,
                                        weights_file,
                                        proba_model]]
if exist.count(1) > 0:
    raise ValueError("At least one of the input files does not exist")

prms = pa.Parameters()
params = pa.read_json(config_file)

[pa.set_configs(params, key, prms) for key in ["tau",
                                               "optimization",
                                               "timing",
                                               "intensity"]]
if prms.parallel == "full":
    prms.parallel = prms.repetitions

if prms.verbosity:
    print('Tau optimization starts...', flush=True)
    print('Config file:', config_file, flush=True)
    print('Weights stored in:', weights_file, flush=True)
    print('Probabilistic model:', proba_model, flush=True)
    print('Intensity model:', intensity_model, flush=True)

##### Read the constituents #####
json_dict = pa.read_json(weights_file)
inputs = json_dict["inputs"]
prms.prosodic_weight = inputs["speech_style"]["prosodic structure importance"]*0
prms.per_weight = inputs["speech_style"]["lexical contrast importance"]*0
prms.parsing_weight = inputs["speech_style"]["lexical contrast importance"]
prms.duration_weight = inputs["relative_speech_rate"]
prms.speech_rate_weight = 0 #  inputs["speech_style"]["relative speech rate importance"]
prms.effort_weight = inputs["speech_style"]["relaxed speech importance"]

if prms.verbosity:
    print('Weights:', flush=True)
    [print("Weight assigned to " + key1 + ": " + str(getattr(prms, key2)),
           flush=True) for key1, key2 in zip(['effort', 'intelligibility',
                                              'duration', 'prosody',
                                              'speech rate', 'PER'],
                                              ['effort_weight',
                                               'parsing_weight',
                                               'duration_weight',
                                               'prosodic_weight',
                                               'speech_rate_weight',
                                               'per_weight'])]
    print('Optimization of the prosodic timings starts...', flush=True)
constituents = inputs["constituents"]
sylls = [c for c  in constituents if c["type"] == "syllable"]

solution = planart.prosody.run_optimization(inputs,
                                        prominence_power=prms.prominence_power,
                                        randomize=prms.randomize_timing,
                                        init_duration=prms.initial_duration,
                                        parsing_model=prms.parsing_model,
                                        c=prms.arctan_timing_coefficient)
if prms.verbosity:
    print('Optimized duration for utterance:', np.sum(solution), flush=True)

for s, dur in zip(sylls, pa.normalize_duration(solution)):
    s["target_duration"] = dur
    if prms.verbosity:
        print('Optimized relative duration for syllable ' +  s['label'] +
              ': ' + '%.2f' %(dur), flush=True)

##### Read score and weights of the cost function #####
score_file = "initial_score.json"
planart.prosody.write_initial_score(solution, constituents,
                                    score_file, method="tau")
# ##### Read score and weights of the cost function #####
import json
with open(score_file, "r") as f:
    score = json.load(f)["phoneme"]

def rewrite_phonemes(l):
    if l == 'd':
        return 't'
    if l == 'g':
        return 'k'
    if l == 'b':
        return 'p'
    if l == 'z':
        return 's'
    if l == 'Z':
        return 'S'
    if l == 'v':
        return 'f'
    return l

lbls = [s["name"] for s in score]
lbls = [rewrite_phonemes(l) for l in lbls]
starts = [s["start"] for s in score]
durations = [s["duration"] for s in score]
ends = [s + d for s, d in zip(starts, durations)]
weights = [s["weights"] for s in score]
starts.append(ends[-1])
durations.append(0.05)
ends = [s + d for s, d in zip(starts, durations)]
os.remove(score_file)
if prms.verbosity:
    print('Initial durations assigned', flush=True)

prms.probabilistic_model = pa.open_model(proba_model)
prms.probabilistic_model.get_neutral_phoneme()
if prms.verbosity:
    print('Probabilistic model loaded', flush=True)
    print('Initializing the solution...', flush=True)

if prms.intensity:
    prms.intensity_model = pa.load_intensity_model(intensity_model, prms)

model_labels = [l for l in prms.probabilistic_model.labels]
keys = ["jaw", "TD position", "TD height", "TT position",
        "lip aperture", "lip protrusion", "larynx height"]

maeda_articulators = pa.read_json(maeda_file)["articulators"]
masses = {m["name"]: m["mass"] for m in maeda_articulators}
min_pos = {m["name"]: m["minimal value"] for m in maeda_articulators}
max_pos = {m["name"]: m["maximal value"] for m in maeda_articulators}
rest_pos = {m["name"]: m["speech_ready_state"] for m in maeda_articulators}

targets = []
for l in lbls:
    # if l not in [tg['label'] for tg in targets]:
    if prms.initial_position == "dictionary":
        if prms.probabilistic_model.dictionary is not None:
            min_x = prms.probabilistic_model.dictionary[l]
        else:
            print('WARNING: Model file does not contain a dictionary. ' +
                  'Running optimal initialization instead', flush=True)
            prms.initial_position = 'optimal'
    if prms.initial_position == "optimal":

        idx_phoneme = prms.probabilistic_model.labels.index(l)
        min_x = pa.optimal_initialization(prms.probabilistic_model,
                                          idx_phoneme, prms, x0=None,
                                          verbosity=False)
        while pa.get_init_cost_function(min_x, prms.probabilistic_model,
                                        idx_phoneme) > 0.1:
            min_x = pa.optimal_initialization(prms.probabilistic_model,
                                              idx_phoneme, prms, x0=None,
                                              verbosity=False)
    elif prms.initial_position == "random":
        min_x = [np.random.uniform(min_pos[key], max_pos[key]) for
                 key in keys]
    elif prms.initial_position == "rest":
        min_x = [rest_pos[key] for key in keys]

    targets.append({"label": l, "class": 'dummy',
                        "articulatory parameters": {key: value for key, value in
                                             zip(keys, min_x)}})

if prms.verbosity:
    print('Solution initialized', flush=True)
arts = pa.init_tau_articulators(targets, lbls, prms, starts, ends, weights,
                                masses=masses)

vocal_tract = pa.VT("articulators", arts,
                    "articulatory_model", maeda_articulators,
                    "prosodic_structure", constituents,
                    "reference_phonemes", lbls)

vocal_tract.compute_trajectory(prms, method="tau")
vocal_tract.initial_duration = np.sum(solution)
# if prms.pressure_opt:
pa.create_subglottal_pressure(vocal_tract, solution, prms)

vocal_tract.get_cost_function(prms, method="tau")
if prms.verbosity:
    print('Initial trajectories and cost function computed', flush=True)
    print('Global cost function:', vocal_tract.cost_function, flush=True)
    [print("Cost function of " + key1 + ": " + str(getattr(vocal_tract, key2)),
           flush=True) for key1, key2 in zip(['effort', 'intelligibility',
                                              'duration', 'prosody',
                                              'speech rate', 'PER'],
                                              ['effort_cost',
                                               'parsing_cost',
                                               'duration_cost',
                                               'prosodic_cost',
                                               'speech_rate_cost',
                                               'per_cost'])]

# prms.pressure_opt = True
vocal_tract.optimization = pa.OptimParameter("parent", vocal_tract,
                                             "kappa", prms.kappa_opt,
                                             "position", prms.position_opt,
                                             "duration", prms.duration_opt,
                                             "pressure", prms.pressure_opt)

vocal_tract.optimize(prms, method="tau", early_stop=prms.early_stop)
if prms.verbosity:
    print('Optimization has finished', flush=True)
    print('Global cost function:', vocal_tract.cost_function, flush=True)
    [print("Cost function of " + key1 + ": " + str(getattr(vocal_tract, key2)),
           flush=True) for key1, key2 in zip(['effort', 'intelligibility',
                                              'duration', 'prosody',
                                              'speech rate', 'PER'],
                                              ['effort_cost',
                                               'parsing_cost',
                                               'duration_cost',
                                               'prosodic_cost',
                                               'speech_rate_cost',
                                               'per_cost'])]

vocal_tract.time_vector = np.arange(vocal_tract.articulatory_space.dynamics.shape[1])/prms.sampling_rate
vocal_tract.tract_space = pa.Space("parent", vocal_tract)
vocal_tract.tract_space.dynamics = pa.articulatory_to_task(vocal_tract.articulatory_space.dynamics,
                                                           method="parametric")
phonemes = pa.phoneme_prediction(vocal_tract, prms.probabilistic_model)
vocal_tract.surface_timing = pa.get_surface_duration(vocal_tract, prms)

##### Export results
if prms.verbosity:
    print("Predicted phonemes:", " ".join(phonemes))
    print("Surface timing:", vocal_tract.surface_timing)
    print("Surface duration:", np.diff(vocal_tract.surface_timing, axis=1).squeeze())
    print('Exporting result in',
          os.path.join(pa.workspace().as_posix(), "solution.json"),
          flush=True)
output_score = os.path.join(pa.workspace().as_posix(), "solution.json")
data = pa.export_solution(vocal_tract, output_score)
output_dynamics = os.path.join(pa.workspace().as_posix(), "dynamics.h5")
vocal_tract.export_dynamics(output_dynamics, prms)

if prms.debug:
    pa.save_current_state(vocal_tract, prms, None,
                          file=os.path.join(pa.workspace().as_posix(),
                                            "debug.pickle"))

if prms.verbosity:
    print('Optimization successfully done in ' +
          str(int(time.time() - start_time)) + ' seconds', flush=True)