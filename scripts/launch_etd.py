#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 30 07:53:09 2021

@author: benjamin
"""

import planart as pa
import planart.prosody
import argparse
import os

##### Read input arguments #####

parser = argparse.ArgumentParser(description="Speech articulatory optimization " +
                                  "using Embodied Task Dynamics (ETD) method")
parser.add_argument("config_file", metavar="config", type=str,
                    help="path to the configuration file (JSON format)")
parser.add_argument("gesture_dictionary", metavar="gesture_dictionary", type=str,
                    help="path to the gesture dicationary (JSON format)")
parser.add_argument("cues", metavar="cues", type=str,
                    help="path to the acoustic cues (JSON format)")
parser.add_argument("weights", metavar="weights", type=str,
                    help="path to the cost function weights (JSON format)")

config_file = parser.parse_args().config_file
gesture_file = parser.parse_args().gesture_dictionary
cues_file = parser.parse_args().cues
weights_file = parser.parse_args().weights
etd_file = pa.GlobalConfig.etd_path()

##### Check if input files exist #####
exist = [pa.check_file_exist(file) for file in [config_file,
                                        gesture_file,
                                        cues_file,
                                        weights_file,
                                        etd_file]]
if exist.count(1) > 0:
    raise ValueError("At least one of the input files does not exist")

##### Read the global parameters and ETD parameters #####

prms = pa.Parameters()
params = pa.read_json(config_file)
etd_params = pa.read_json(etd_file)
gestures = etd_params["gestures"]
# prms.duration_weight = sentence[0]["weights"]["duration"]

[pa.set_configs(params, key, prms) for key in ["etd", 
                                               "optimization",
                                               "timing"]]
if prms.parallel == "full":
    prms.parallel = prms.repetitions
    
##### Read the constituents #####
json_dict = pa.read_json(weights_file)
inputs = json_dict["inputs"]

prms.prosodic_weight = inputs["speech_style"]["prosodic structure importance"]
prms.per_weight = inputs["speech_style"]["lexical contrast importance"]*0
prms.parsing_weight = inputs["speech_style"]["lexical contrast importance"]
prms.duration_weight = inputs["relative_speech_rate"]
prms.speech_rate_weight = inputs["speech_style"]["relative speech rate importance"]
prms.effort_weight = inputs["speech_style"]["relaxed speech importance"]

if prms.verbosity:
    print('Weights:', flush=True)
    [print("Weight assigned to " + key1 + ": " + str(getattr(prms, key2)),
           flush=True) for key1, key2 in zip(['effort', 'intelligibility',
                                              'duration', 'prosody',
                                              'speech rate', 'PER'],
                                              ['effort_weight',
                                               'parsing_weight',
                                               'duration_weight',
                                               'prosodic_weight',
                                               'speech_rate_weight',
                                               'per_weight'])]
    print('Optimization of the prosodic timings starts...', flush=True)

constituents = inputs["constituents"]
sylls = [c for c  in constituents if c["type"] == "syllable"]

solution = planart.prosody.run_optimization(inputs, 
                                            prominence_power=prms.prominence_power,
                                            randomize=prms.randomize_timing,
                                            init_duration=prms.initial_duration,
                                            parsing_model=prms.parsing_model,
                                            c=prms.arctan_timing_coefficient)

for s, dur in zip(sylls, pa.normalize_duration(solution)):
    s["target_duration"] = dur

constituent_sorted, types = pa.sort_all_constituents(constituents)
sentence = [c for c in constituents if c["type"] == "sentence"]

##### Read score and weights of the cost function #####
score_file = "initial_score.json"
planart.prosody.write_initial_score(solution, constituents, score_file)
start, duration, end = pa.read_score(score_file, gestures)

os.remove(score_file)
effort_vec, duration_vec, precision_vec = pa.read_weights(constituents, gestures)
Kgen = params["etd"]["initial_stiffness"]
if "random" in Kgen:
    import numpy as np
    min_bound = 0
    max_bound = 100
    bounds = Kgen.split(":")[1].split(",")
    for b in bounds:
        if "max" in b:
            max_bound = float(b.split("=")[1])
        if "min" in b:
            min_bound = float(b.split("=")[1])
    Kgen = np.random.uniform(min_bound, max_bound)
else:
    Kgen = float(Kgen)
articulators = etd_params["articulators"]

##### Create the Vocal Tract object #####

arts = pa.group_articulators(etd_params["articulators"])
gests = pa.group_gestures(gestures, (start, duration, end),
                          (effort_vec, duration_vec, precision_vec))

vocal_tract = pa.VT("articulators", arts, "general_stiffness", Kgen,
                 "gestures", gests)

##### Initialization of the Vocal tract characteristics (anatomy matrices, targets, ...) #####

vocal_tract.compute_art_mk_matrix()
vocal_tract.make_anatomy()
vocal_tract.update_parent()
vocal_tract.compute_tract_mk_matrix()
vocal_tract.init_gesture()
vocal_tract.speech_ready_space = pa.Space("parent", vocal_tract)
vocal_tract.speech_ready_space.damping = pa.crit_damp(vocal_tract.articulatory_space,
                                                   vocal_tract.general_stiffness)
vocal_tract.prosodic_structure = constituents
prms.prosodic_weight = inputs["speech_style"]["prosodic structure importance"]
prms.parsing_weight = inputs["speech_style"]["lexical contrast importance"]
prms.duration_weight = inputs["speech_style"]["relative speech rate importance"]
prms.effort_weight = inputs["speech_style"]["relaxed speech importance"]
vocal_tract.compute_trajectory(prms)

vocal_tract.initial_sequence = pa.gesture_timing(vocal_tract)[0]
vocal_tract.initial_duration = vocal_tract.get_duration(prms, model="etd")
vocal_tract.get_cost_function(prms, method="etd")
if prms.verbosity:
    print('Initial trajectories and cost function computed', flush=True)
    print('Global cost function:', vocal_tract.cost_function, flush=True)
    [print("Cost function of " + key1 + ": " + str(getattr(vocal_tract, key2)),
           flush=True) for key1, key2 in zip(['effort', 'intelligibility',
                                              'duration', 'prosody', 
                                              'speech rate', 'PER'],
                                              ['effort_cost',
                                               'parsing_cost',
                                               'duration_cost',
                                               'prosodic_cost',
                                               'speech_rate_cost',
                                               'per_cost'])]
prms.catch_solution = False
##### Launch optimization
vocal_tract.optimize(prms, method="etd", early_stop=prms.early_stop)

##### Export results
output_score = os.path.join(pa.workspace().as_posix(), "final_score.json")
data = vocal_tract.export_score(output_score, method="etd")
output_dynamics = os.path.join(pa.workspace().as_posix(), "dynamics.h5")
vocal_tract.export_dynamics(output_dynamics, prms)