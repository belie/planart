#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Oct 30 07:53:09 2021

@author: benjamin
"""

import planart as pa
import argparse
import os

##### Read input arguments #####

parser = argparse.ArgumentParser(description="Speech synthesis")
parser.add_argument("config_file", metavar="config", type=str,
                    help="path to the configuration file (JSON format)")
parser.add_argument("solution_file", metavar="gesture_dictionary", type=str,
                    help="path to the gesture dicationary (JSON format)")

config_file = parser.parse_args().config_file
solutions = parser.parse_args().solution_file
##### Check if input files exist #####

exist = [pa.check_file_exist(file) for file in [config_file, solutions]]
if exist.count(1) > 0:
    raise ValueError("At least one of the input files does not exist")

prms = pa.Parameters()
prms_solutions = pa.Parameters()

params = pa.read_json(config_file)
pa.set_configs(params, "acoustics", prms)
[pa.set_configs(params, key, prms_solutions) for key in ["tau", "optimization"]]
 
arts = pa.read_solution(solutions)
vocal_tract = pa.VT("articulators", arts)
vocal_tract.compute_trajectory(prms_solutions, method="tau")
trajectory = vocal_tract.articulatory_space.dynamics

VTobj = pa.trajectory_to_vt(trajectory, sri=prms_solutions.sampling_rate, 
                            sro=prms.simulation_rate)
pa.glottal_control(VTobj, F0=prms.fundamental_frequency)
pa.subglottal_pressure(VTobj, psubmax=prms.subglottal_pressure)
output_file = os.path.join(pa.workspace().as_posix(), "speech_audio.wav")
pa.run_simulation(VTobj, output_file=output_file,
                  sro=prms.output_sampling_rate)
