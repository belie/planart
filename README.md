# PlanArt

## Presentation

PlanArt is a modular software for testing and comparing computational models of human speech articulation planning. The software includes three main modules corresponding to the different stages of articulatory planning:
* the `symbolic` module takes a symbolic representation of an utterance as input and assigns weights to each constituent
* the `dynamic` module takes the output of the `symbolic` module and returns the planned articulatory movements as output. 
* the `motor-sensory` module takes the planned articulatory sequence and returns the final articulatory movmeents (and the simulated acoustic output if available). 

Notes:
* At this stage, the dynamic planning only accepts the submodule `optimization` to estimate the planned articulatory movements via optimization procedure. The `optimization` submodule can use either Embodied Task Dynamics (ETD) [1] or the Extrinsic Phonology 3-component (XT/3C) [2] optimization via general Tau theory [3]. Other kind (i.e. non-optimization-based) of dynamic planning may be implemented in the future.
* At this stage, the motor-sensory implementation only accepts the submodule `acoustics`. The submodule `acoustics` generates the acoustic output of the simulated utterance using the Extended Single Matrix Formulation (ESMF) [4], a physics-based paradigm for articulatory synthesis.
 of running speech. Other models may be implemented in the future.

## OS and Python requirements

* PlanArt is implemented with Python 3. Successful tests have been performed with Python 3.9, so consider having this specific version of Python. We noticed bugs with Python<=3.9 with importlib and with Python 3.10.

* PlanArt currently uses TensorFlow, which is not supported on 32 bit systems. If you have a 32 bit system, you can possibly install TensorFlow manually from source codes, but not tested with PlanArt. We are currently working on that issue. Check for later updates

* joblib does not work on machines which do not support parallel computing => Set "parallel" to 1 in globalparameters.json or it may raise errors

## Installation

Please also consider using a Python 3 virtual environment during the development stage:
```bash
# create a python 3 virtual environment and activate it
$ python3 -m venv planart_env
$ source planart_env/bin/activate
```


**Windows users**: activate the environment in an Anaconda PowerShell window (recommended) with:  
```bash
# create a python 3 virtual environment and activate it
$ python -m venv planart_env
$ ./planart_env/Scripts/activate
```

You can get the source codes by cloning this repo with git
```
git clone https://git.ecdf.ed.ac.uk/belie/planart.git

```

PlanArt is not yet on PyPI. You can install it locally in editable mode:
```bash
# install planart locally in editable mode
$ python3 -m pip install -e path_to_planart
```

* WARNING: Please, do not modify anything in the planart folder, unless you want to modify the repository.

## Checking the software behavior
To check if everything runs as expected, run the following commands:
```bash
# test if everything works as expected
$ cd path_to_planart/tests
$ pytest .
```
It should not return any error message

* WARNING: test create a .planart folder in the `$HOME` directory (See below). It may make the tests fail if it has been badly created. You may remove it (or just change the name, in order to keep a backup version) if the tests fail, and relaunch pytest.

<!---
### Dependencies
PlanArt requires dependencies which are also in early developments. For these dependencies, it is recommended to clone their repos and install them in editable mode. These dependencies are:
* maedeep (https://git.ecdf.ed.ac.uk/belie/maedeep)
* spychhiker (https://gitlab.com/benjamin.elie/spychhiker)
-->

Check regularly for updates

## Configuration

### Global configuration
You have to create a global configuration folder located in your `$HOME` directory. You can create one automatically with the following command:
```bash
$ planart init-config
```

This will create the folder `$HOME/.planart_vX.Y.Z`, where X.Y.Z is the current version of PlanArt that is installed in your environment. The folder contains 4 files:
* `config.ini` stores the method you want to apply for each module (at this stage only ETD and XT/3C are available for the optimization module), and the location of the python scripts to launch the modules.
* `globalparameters.json` stores the global parameters used for each module and method. At this stage, only ETD and XT/3C are considered: the file contains all the hyperparameters and default settings for running them.
* `etd.json` stores the parameters of the articulatory model used in ETD
* `maeda.json` stores parameters related to the Maeda's articulatory model used during the XT/3C optimization

It also creates a folder `models` which contains probabilistic models used during the `optimization` module.

**If a new config folder has been created by a new version of PlanArt, you can safely remove all older folders (recommended)**

### .ini files
The software modules needs configuration files as input. The files used as input for each module are specified in a .ini file. To create a new one, run the following command:
```bash
$ planart new config.ini
```

This generates the file `config.ini` which contains 5 sections corresponding to each module and submodule:
* module `symbolic`: paths to the file which contains the utterance to be analyzed (see next section for creating such files)
* module `dynamic`: choice of the dynamic planning method. At this stage, it only accepts 'optimization'
* module `motor-sensory`: choice of the method for the motor-sensory implementation. At this stage, it only accepts 'acoustics'
* submodule `optimization`: choice of the articulatory planning model (etd or xt/3C) in the field 'method', path to a dictionary of predefined movement targets (not used at this stage), path to the probabilistic model and the file that contains the global parameters.
* submodule `acoustics`: path to the file that contains the global parameters used to run the aocustic simulation.

It also copies all the default files stored in the config directory (located in your `$HOME`) needed to run all modules. In addition, it creates a default utterance file `utterance.json`, corresponding to the requirement of producing a single Americam-English vowel 'aa'. 

### Creating an utterance file
To create an utterance file, run the following command
```bash
$  planart init-utterance utterance.json -utt 'sentence:(key1=value1,key2=value2,..)' -w 'word1:(key1=value1,key2=value2,..)' -w 'word2:(key1=value1,key2=value2,..)' -syl 'syl1:(key1=value1,key2=value2,..)' -syl 'syl2:(key1=value1,key2=value2,..)' -p 'phoneme1:(key1=value1,key2=value2,..)' -p 'phoneme2:(key1=value1,key2=value2,..)' -p 'phoneme3:(key1=value1,key2=value2,..)' -sty 'key1=value1,key2=value2' -sr speech_rate
```

where -sr accepts a float that defines the desired relative speech rate, and `sentence` is a string that contains the sentence (e.g. 'hello world'), `word1` and `word2` are the words that compose the sentence (e.g. 'hello' and 'world') and so forth. -syl and -p are used to initialize the syllable and phoneme constituents respectively, and -sty is used to define the speech style requirements. key1, key2, ... are strings used to assign different parameters to the constituents. The following keys are accepted:
* 'prom': assigns prominence to the constituent (e.g. -w 'hello=(prom=4)' to assign a prominence equal to 4 to the word 'hello')
* 'id': assigns a unique ID to the constituent (default is the type of constituent followed by its order, e.g. the default of the word 'world' in 'hello world' would be word_2)
* 'par': assigns a constituent parent to the constituent through the parent ID (e.g. the parents of the words 'hello' and 'world' in 'hello world' should be the ID of the sentence 'hello world')
* 'sib': assigns the previous sibling constituent to the current constituent through the sibling ID (e.g. the previous sibling of the word 'world' in 'hello world' should be the ID of the word 'hello')

For the speech style (-sty) the different keys are used to assign importance to various aspects of speech. It accepts the following strings:
* 'lex': defines the importance given to the respect of the lexical contrast (default=1)
* 'sr': defines the importance given to the respect of the desired relative speech rate (default=1)
* 'pros': defines the importance given to the respect of the desired prosodic structure (default=1)
* 'rlx': defines the importance given to the respect of a relaxed, effortless, speech (default=1)

If you want to simulate a single phoneme,you can use a simplified version of init-utterance with the -v option, such as (e.g. for the vowel /i:/):
```bash
$  planart init-utterance utterance.json -v 'iy'
```
You can also specify the speech style and speech rate arguments.

### Changing default parameters
To modify default parameters written in the globalparameters.json file, you may run
```bash
$  planart param-modif your_ini.ini -p 'level1:(key1=value1,key2=value2,...)' -p 'level2:(key1=value1,key2=value2,...)'
```

where 'your_ini.ini' is the .ini file which calls the parameter file you want to modify. The 'level' argument is the field to which belong the parameters you want to modify, and 'key' and 'value' pairs are respectively the field name and the new value you want to assign to the parameter. For instance, if you want to switch from a global kappa optimization to a local kappa optimization, and want to set the number of optimization repetitions to 50, you will have to run:
```bash
$  planart param-modif your_ini.ini -p 'tau:(kappa_opt=local)' -p 'optimization:(repetitions=50)'
```

## Running PlanArt
To run all main modules at once, use the following command:
```bash
$ planart run config.ini
```
This will run the `symbolic`, `dynamic`, and `motor-sensory` modules in that specific order.

To run only one specific module (or submodule), use the following command:
```bash
$ planart run -m module config.ini
```
where module is one of the following: `symbolic`, `dynamic`, `optimization`, `motor-sensory`, or `acoustics`

To run all main modules (`symbolic`, `dynamic`, and `motor-sensory`) at once from a specific main module, use the following command:
```bash
$ planart run -f module config.ini
```

## Quick test
To perform a quick test, run the following commands in a new empty folder:
```bash
$ planart new test.ini
$ planart run test.ini
```
It will simulate and synthesize a default single Americam-English vowel 'aa'. 

## Viewing the planned articulatory gestures
The outputs of the `optimization` module are stored in two files. `solution.json` contains the estimated gesture score stored in a JSON format. `dynamics.h5` contains the time evolution of the planned articulatory movements, both in the articulatory and tract variable domains, in a HDF5 format.

To display these functions, simply run the following command:
```bash
$ planart_analysis plot-gestures final_score.json gestures.h5
```

## References
[1] J. Simko and F. Cummins, “Embodied task dynamics,” Psychological review, vol. 117, no. 4, p. 1229, 2010

[2] A. Turk and S. Shattuck-Hufnagel, “Timing evidence for symbolic phonological representations and phonology-extrinsic timing in speech production,” Frontiers in Psychology, p. 2952, 2020.

[3] D. N. Lee, “Guiding movement by coupling taus,” Ecological psychology, vol. 10, no. 3-4, pp. 221–250, 1998.

[4] Elie B., and Laprie Y. "Extension of the single-matrix formulation of the vocal tract: consideration of bilateral channels and connection of self-oscillating models of the vocal folds with a glottal chink".  Speech Comm. 82, pp. 85-96 (2016)




